/* author   :   Serhat Istin
<mailto:istin@cern.ch>
*/

#include <NTAnalysis/BSLLAlg.h>
#include <EventLoop/Worker.h>
#include <SampleHandler/MetaObject.h>
#include "TH1F.h"
#include "TTree.h"
#include <TFile.h>

#include <NTAnalysis/LQTree_v26.h>


BSLLAlg :: BSLLAlg (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : Skeleton (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
    
    //In the new AnalAlgorithm those methods seem to be disabled by default
    //put the below lines to re-enable them. but remember to override the methods here
}

BSLLAlg::~BSLLAlg(){

}




StatusCode BSLLAlg::BookHistogram(std::string name,int nbinsx,float xmin,float xmax,std::string title){
    
    if(title=="None" ) title=name;
    
    ANA_CHECK (book (TH1F (name.c_str(), title.c_str(), nbinsx, xmin, xmax))); 
    
    auto newname=name+"_0bEx";
    ANA_CHECK (book (TH1F (newname.c_str(), title.c_str(), nbinsx, xmin, xmax))); 
    
    newname=name+"_1bEx";
    ANA_CHECK (book (TH1F (newname.c_str(), title.c_str(), nbinsx, xmin, xmax))); 

    newname=name+"_1bInc";
    ANA_CHECK (book (TH1F (newname.c_str(), title.c_str(), nbinsx, xmin, xmax))); 
    
    newname=name+"_2bInc";
    ANA_CHECK (book (TH1F (newname.c_str(), title.c_str(), nbinsx, xmin, xmax))); 

    return StatusCode::SUCCESS;
}



void BSLLAlg::FillHistogram(const std::string& name,float val,float weight){

    int NBJETS=m_data->BJets().size();
    bool b0Ex= true ? NBJETS==0 : false;
    bool b1Ex=true ? NBJETS==1 : false;
    bool b1Inc=true ? NBJETS >=1 : false;
    bool b2Inc=true ? NBJETS>=2 : false;
    
    hist(name)->Fill (val,weight);
    auto newname=name+"_0bEx";
    if(b0Ex) hist(newname)->Fill (val,weight);
    newname=name+"_1bEx";
    if(b1Ex) hist(newname)->Fill (val,weight);
    newname=name+"_1bInc";
    if(b1Inc) hist(newname)->Fill (val,weight);
    newname=name+"_2bInc";
    if(b2Inc) hist(newname)->Fill (val,weight);
    return;
}



StatusCode BSLLAlg :: BookHistograms ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
    //ANA_CHECK (book (TH1F ("h_jetPt", "h_jetPt", 100, 0, 500))); // jet pt [GeV]


    //BOOK YOUR HISTOGRAMS HERE

    ANA_CHECK(BookHistogram("h_SR_mll_400", 31, 400, 3500, "m_{ll} [GeV]"));
    ANA_CHECK(BookHistogram("h_SR_mll_500", 30, 500, 3500, "m_{ll} [GeV]"));
    ANA_CHECK(BookHistogram("h_SR_mll_600", 29, 600, 3500, "m_{ll} [GeV]"));
    ANA_CHECK(BookHistogram("h_SR_mll_700", 28, 700, 3500, "m_{ll} [GeV]"));
    ANA_CHECK(BookHistogram("h_SR_mll_800", 27, 800, 3500, "m_{ll} [GeV]"));
    ANA_CHECK(BookHistogram("h_SR_mll_900", 26, 900, 3500, "m_{ll} [GeV]"));
    ANA_CHECK(BookHistogram("h_SR_mll_1000", 25, 1000, 3500, "m_{ll} [GeV]"));
    ANA_CHECK(BookHistogram("h_SR_mll_1100", 24, 1100, 3500, "m_{ll} [GeV]"));
    ANA_CHECK(BookHistogram("h_SR_mll_1200", 23, 1200, 3500, "m_{ll} [GeV]"));
    ANA_CHECK(BookHistogram("h_SR_mll_1300", 22, 1300, 3500, "m_{ll} [GeV]"));
    ANA_CHECK(BookHistogram("h_SR_mll_1400", 21, 1400, 3500, "m_{ll} [GeV]"));
    ANA_CHECK(BookHistogram("h_SR_mll_1500", 20, 1500, 3500, "m_{ll} [GeV]"));
    ANA_CHECK(BookHistogram("h_SR_mll_1600", 19, 1600, 3500, "m_{ll} [GeV]"));
    ANA_CHECK(BookHistogram("h_SR_mll_1700", 18, 1700, 3500, "m_{ll} [GeV]"));
    ANA_CHECK(BookHistogram("h_SR_mll_1800", 17, 1800, 3500, "m_{ll} [GeV]"));
    ANA_CHECK(BookHistogram("h_SR_mll_1900", 16, 1900, 3500, "m_{ll} [GeV]"));
    ANA_CHECK(BookHistogram("h_SR_mll_2000", 15, 2000, 3500, "m_{ll} [GeV]"));
    ANA_CHECK(BookHistogram("h_SR_mll_2100", 14, 2100, 3500, "m_{ll} [GeV]"));
    ANA_CHECK(BookHistogram("h_SR_mll_2200", 13, 2200, 3500, "m_{ll} [GeV]"));
    ANA_CHECK(BookHistogram("h_SR_mll_2300", 12, 2300, 3500, "m_{ll} [GeV]"));
    ANA_CHECK(BookHistogram("h_SR_mll_2400", 11, 2400, 3500, "m_{ll} [GeV]"));
    ANA_CHECK(BookHistogram("h_SR_mll_2500", 10, 2500, 3500, "m_{ll} [GeV]"));

//    ANA_CHECK(BookHistogram("h_SR1_mll", 18, 1700, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_SR1_N_1_mll", 25, 1000, 3500, "m_{ll} [GeV]"));
//
//    ANA_CHECK(BookHistogram("h_SR1_mllb", 60, 0, 3000, "m_{llb} [GeV]"));
//
//    ANA_CHECK(BookHistogram("h_SR1_met", 20, 0, 1000, "E_{T}^{miss} [GeV]"));
//    ANA_CHECK(BookHistogram("h_SR1_metphi", 20, -4, 4));
//    ANA_CHECK(BookHistogram("h_SR1_HT_leptons", 60, 0, 3000));
//    ANA_CHECK(BookHistogram("h_SR1_HT_jets", 60, 0, 3000));
//    ANA_CHECK(BookHistogram("h_SR1_HT_total", 60, 0, 3000, "H_{T} [GeV]"));
//    ANA_CHECK(BookHistogram("h_SR1_N_1_HT_total", 60, 0, 3000, "H_{T} [GeV]"));
//
//    ANA_CHECK(BookHistogram("h_SR1_ptll", 50, 0, 1000));
//    ANA_CHECK(BookHistogram("h_SR1_etall", 20, -4, 4));
//    ANA_CHECK(BookHistogram("h_SR1_phill", 20, -4, 4));
//    ANA_CHECK(BookHistogram("h_SR1_dphill", 20, 0, 4, "#Delta#phi (#mu_{1},#mu_{2})"));
//    ANA_CHECK(BookHistogram("h_SR1_detall", 20, 0, 10));
//    ANA_CHECK(BookHistogram("h_SR1_dRll", 20, 0, 10, "#DeltaR (#mu_{1},#mu_{2})"));
//    ANA_CHECK(BookHistogram("h_SR1_dphill_b", 20, 0, 4));
//    ANA_CHECK(BookHistogram("h_SR1_detall_b", 20, 0, 10));
//    ANA_CHECK(BookHistogram("h_SR1_dRll_b", 20, 0, 10, "#DeltaR (#mu_{1}+#mu_{2},b_{1})"));
//
//
//    ANA_CHECK(BookHistogram("h_SR1_leading_lep_pt", 50, 0, 2500, "p_{T}(lep_{1}) [GeV]"));
//    ANA_CHECK(BookHistogram("h_SR1_sub_leading_lep_pt", 50, 0, 2500, "p_{T}(lep_{2}) [GeV]"));
//    ANA_CHECK(BookHistogram("h_SR1_leading_lep_eta", 20, -4, 4));
//    ANA_CHECK(BookHistogram("h_SR1_sub_leading_lep_eta", 20, -4, 4));
//    ANA_CHECK(BookHistogram("h_SR1_leading_lep_phi", 20, -4, 4));
//    ANA_CHECK(BookHistogram("h_SR1_sub_leading_lep_phi", 20, -4, 4));
//
//    ANA_CHECK(BookHistogram("h_SR1_leading_jet_pt", 50, 0, 2500, "p_{T}(jet_{1}) [GeV]"));
//    ANA_CHECK(BookHistogram("h_SR1_sub_leading_jet_pt", 50, 0, 2500, "p_{T}(jet_{2}) [GeV]"));
//    ANA_CHECK(BookHistogram("h_SR1_leading_jet_eta", 20, -4, 4));
//    ANA_CHECK(BookHistogram("h_SR1_sub_leading_jet_eta", 20, -4, 4));
//    ANA_CHECK(BookHistogram("h_SR1_leading_jet_phi", 20, -4, 4));
//    ANA_CHECK(BookHistogram("h_SR1_sub_leading_jet_phi", 20, -4, 4));
//
//    ANA_CHECK(BookHistogram("h_SR1_leading_bjet_pt", 50, 0, 2500, "p_{T}(b-tagged jet_{1}) [GeV]"));
//    ANA_CHECK(BookHistogram("h_SR1_sub_leading_bjet_pt", 50, 0, 2500, "p_{T}(b-tagged jet_{2}) [GeV]"));
//    ANA_CHECK(BookHistogram("h_SR1_leading_bjet_eta", 20, -4, 4));
//    ANA_CHECK(BookHistogram("h_SR1_sub_leading_bjet_eta", 20, -4, 4));
//    ANA_CHECK(BookHistogram("h_SR1_leading_bjet_phi", 20, -4, 4));
//    ANA_CHECK(BookHistogram("h_SR1_sub_leading_bjet_phi", 20, -4, 4));
//
//
//    ANA_CHECK(BookHistogram("h_SR1_nj", 10, 0, 10, "Number of Jets"));
//    ANA_CHECK(BookHistogram("h_SR1_nbj", 10, 0, 10, "Number of b-tagged Jets"));
//    ANA_CHECK(BookHistogram("h_SR1_nel", 5, 0, 5));
//    ANA_CHECK(BookHistogram("h_SR1_nmu", 5, 0, 5));
//
//
//
//    ANA_CHECK(BookHistogram("h_SR2_mll", 11, 2400, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_SR2_mllb", 60, 0, 3000, "m_{llb} [GeV]"));
//    ANA_CHECK(BookHistogram("h_SR2_N_1_mll", 25, 1000, 3500, "m_{ll} [GeV]"));
//
//
//    ANA_CHECK(BookHistogram("h_SR2_met", 20, 0, 1000, "E_{T}^{miss} [GeV]"));
//    ANA_CHECK(BookHistogram("h_SR2_metphi", 20, -4, 4));
//    ANA_CHECK(BookHistogram("h_SR2_HT_leptons", 60, 0, 3000));
//    ANA_CHECK(BookHistogram("h_SR2_HT_jets", 60, 0, 3000));
//    ANA_CHECK(BookHistogram("h_SR2_HT_total", 60, 0, 3000, "H_{T} [GeV]"));
//    ANA_CHECK(BookHistogram("h_SR2_N_1_HT_total", 60, 0, 3000, "H_{T} [GeV]"));
//
//    ANA_CHECK(BookHistogram("h_SR2_ptll", 50, 0, 1000));
//    ANA_CHECK(BookHistogram("h_SR2_etall", 20, -4, 4));
//    ANA_CHECK(BookHistogram("h_SR2_phill", 20, -4, 4));
//    ANA_CHECK(BookHistogram("h_SR2_dphill", 20, 0, 4, "#Delta#phi (#mu_{1},#mu_{2})"));
//    ANA_CHECK(BookHistogram("h_SR2_detall", 20, 0, 10));
//    ANA_CHECK(BookHistogram("h_SR2_dRll", 20, 0, 10, "#DeltaR (#mu_{1},#mu_{2})"));
//    ANA_CHECK(BookHistogram("h_SR2_dphill_b", 20, 0, 4));
//    ANA_CHECK(BookHistogram("h_SR2_detall_b", 20, 0, 10));
//    ANA_CHECK(BookHistogram("h_SR2_dRll_b", 20, 0, 10, "#DeltaR (#mu_{1}+#mu_{2},b_{1})"));
//
//
//    ANA_CHECK(BookHistogram("h_SR2_leading_lep_pt", 50, 0, 2500, "p_{T}(lep_{1}) [GeV]"));
//    ANA_CHECK(BookHistogram("h_SR2_sub_leading_lep_pt", 50, 0, 2500, "p_{T}(lep_{2}) [GeV]"));
//    ANA_CHECK(BookHistogram("h_SR2_leading_lep_eta", 20, -4, 4));
//    ANA_CHECK(BookHistogram("h_SR2_sub_leading_lep_eta", 20, -4, 4));
//    ANA_CHECK(BookHistogram("h_SR2_leading_lep_phi", 20, -4, 4));
//    ANA_CHECK(BookHistogram("h_SR2_sub_leading_lep_phi", 20, -4, 4));
//
//    ANA_CHECK(BookHistogram("h_SR2_leading_jet_pt", 50, 0, 2500, "p_{T}(jet_{1}) [GeV]"));
//    ANA_CHECK(BookHistogram("h_SR2_sub_leading_jet_pt", 50, 0, 2500, "p_{T}(jet_{2}) [GeV]"));
//    ANA_CHECK(BookHistogram("h_SR2_leading_jet_eta", 20, -4, 4));
//    ANA_CHECK(BookHistogram("h_SR2_sub_leading_jet_eta", 20, -4, 4));
//    ANA_CHECK(BookHistogram("h_SR2_leading_jet_phi", 20, -4, 4));
//    ANA_CHECK(BookHistogram("h_SR2_sub_leading_jet_phi", 20, -4, 4));
//
//    ANA_CHECK(BookHistogram("h_SR2_leading_bjet_pt", 50, 0, 2500, "p_{T}(b-tagged jet_{1}) [GeV]"));
//    ANA_CHECK(BookHistogram("h_SR2_sub_leading_bjet_pt", 50, 0, 2500, "p_{T}(b-tagged jet_{2}) [GeV]"));
//    ANA_CHECK(BookHistogram("h_SR2_leading_bjet_eta", 20, -4, 4));
//    ANA_CHECK(BookHistogram("h_SR2_sub_leading_bjet_eta", 20, -4, 4));
//    ANA_CHECK(BookHistogram("h_SR2_leading_bjet_phi", 20, -4, 4));
//    ANA_CHECK(BookHistogram("h_SR2_sub_leading_bjet_phi", 20, -4, 4));
//
//
//    ANA_CHECK(BookHistogram("h_SR2_nj", 10, 0, 10, "Number of Jets"));
//    ANA_CHECK(BookHistogram("h_SR2_nbj", 10, 0, 10, "Number of b-tagged Jets"));
//    ANA_CHECK(BookHistogram("h_SR2_nel", 5, 0, 5));
//    ANA_CHECK(BookHistogram("h_SR2_nmu", 5, 0, 5));







    ANA_CHECK(BookHistogram("h_CRZ_mll", 8, 130, 250, "m_{ll} [GeV]"));
    ANA_CHECK(BookHistogram("h_VRZ_mll", 15, 250, 400, "m_{ll} [GeV]"));
    ANA_CHECK(BookHistogram("h_CRtt_mll", 35, 0, 3500, "m_{ll} [GeV]"));





//    ANA_CHECK(BookHistogram("h_opt_HT0_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//
//    ANA_CHECK(BookHistogram("h_opt_HT350_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT400_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT450_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT500_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT550_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT600_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT650_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT700_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT750_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT800_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT850_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT900_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT950_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT1000_mll", 35, 0, 3500, "m_{ll} [GeV]"));


//    ANA_CHECK(BookHistogram("h_opt_HT350_mllb1000_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT350_mllb1100_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT350_mllb1200_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT350_mllb1300_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT350_mllb1400_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT350_mllb1500_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT350_mllb1600_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT350_mllb1700_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT350_mllb1800_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT350_mllb1900_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT350_mllb2000_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT350_mllb2100_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT350_mllb2200_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT350_mllb2300_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_HT350_mllb2400_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//
//
//
//
//    ANA_CHECK(BookHistogram("h_opt_MET80_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_MET100_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_MET120_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_MET140_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_MET160_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_MET180_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_MET200_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//
//
//    ANA_CHECK(BookHistogram("h_opt_MET80_mllb", 60, 0, 3000, "m_{llb} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_MET100_mllb", 60, 0, 3000, "m_{llb} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_MET120_mllb", 60, 0, 3000, "m_{llb} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_MET140_mllb", 60, 0, 3000, "m_{llb} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_MET160_mllb", 60, 0, 3000, "m_{llb} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_MET180_mllb", 60, 0, 3000, "m_{llb} [GeV]"));
//    ANA_CHECK(BookHistogram("h_opt_MET200_mllb", 60, 0, 3000, "m_{llb} [GeV]"));




//     ANA_CHECK(BookHistogram("h_presel_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//     ANA_CHECK(BookHistogram("h_presel_mllb", 60, 0, 3000, "m_{llb} [GeV]"));
//     ANA_CHECK(BookHistogram("h_presel_met", 20, 0, 1000));
//     ANA_CHECK(BookHistogram("h_presel_metphi", 20, -4, 4));
//     ANA_CHECK(BookHistogram("h_presel_HT_leptons", 60, 0, 3000));
//     ANA_CHECK(BookHistogram("h_presel_HT_jets", 60, 0, 3000));
//     ANA_CHECK(BookHistogram("h_presel_HT_total", 60, 0, 3000, "H_{T} [GeV]"));
//
//     ANA_CHECK(BookHistogram("h_presel_ptll", 50, 0, 1000));
//     ANA_CHECK(BookHistogram("h_presel_etall", 20, -4, 4));
//     ANA_CHECK(BookHistogram("h_presel_phill", 20, -4, 4));
//     ANA_CHECK(BookHistogram("h_presel_dphill", 20, 0, 4));
//     ANA_CHECK(BookHistogram("h_presel_detall", 20, 0, 10));
//     ANA_CHECK(BookHistogram("h_presel_dRll", 20, 0, 10, "#DeltaR (#mu_{1},#mu_{2})"));
//     ANA_CHECK(BookHistogram("h_presel_dphill_b", 20, 0, 4));
//     ANA_CHECK(BookHistogram("h_presel_detall_b", 20, 0, 10));
//     ANA_CHECK(BookHistogram("h_presel_dRll_b", 20, 0, 10, "#DeltaR (#mu_{1}+#mu_{2},b_{1})"));
//
//
//     ANA_CHECK(BookHistogram("h_presel_leading_lep_pt", 50, 0, 2500));
//     ANA_CHECK(BookHistogram("h_presel_sub_leading_lep_pt", 50, 0, 2500));
//     ANA_CHECK(BookHistogram("h_presel_leading_lep_eta", 20, -4, 4));
//     ANA_CHECK(BookHistogram("h_presel_sub_leading_lep_eta", 20, -4, 4));
//     ANA_CHECK(BookHistogram("h_presel_leading_lep_phi", 20, -4, 4));
//     ANA_CHECK(BookHistogram("h_presel_sub_leading_lep_phi", 20, -4, 4));
//
//     ANA_CHECK(BookHistogram("h_presel_leading_jet_pt", 50, 0, 2500, "p_{T}(jet_{1}) [GeV]"));
//     ANA_CHECK(BookHistogram("h_presel_sub_leading_jet_pt", 50, 0, 2500, "p_{T}(jet_{2}) [GeV]"));
//     ANA_CHECK(BookHistogram("h_presel_leading_jet_eta", 20, -4, 4));
//     ANA_CHECK(BookHistogram("h_presel_sub_leading_jet_eta", 20, -4, 4));
//     ANA_CHECK(BookHistogram("h_presel_leading_jet_phi", 20, -4, 4));
//     ANA_CHECK(BookHistogram("h_presel_sub_leading_jet_phi", 20, -4, 4));
//
//     ANA_CHECK(BookHistogram("h_presel_leading_bjet_pt", 50, 0, 2500, "p_{T}(b-tagged jet_{1}) [GeV]"));
//     ANA_CHECK(BookHistogram("h_presel_sub_leading_bjet_pt", 50, 0, 2500, "p_{T}(b-tagged jet_{2}) [GeV]"));
//     ANA_CHECK(BookHistogram("h_presel_leading_bjet_eta", 20, -4, 4));
//     ANA_CHECK(BookHistogram("h_presel_sub_leading_bjet_eta", 20, -4, 4));
//     ANA_CHECK(BookHistogram("h_presel_leading_bjet_phi", 20, -4, 4));
//     ANA_CHECK(BookHistogram("h_presel_sub_leading_bjet_phi", 20, -4, 4));
//
//
//     ANA_CHECK(BookHistogram("h_presel_nj", 10, 0, 10, "Number of Jets"));
//     ANA_CHECK(BookHistogram("h_presel_nbj", 10, 0, 10, "Number of b-tagged Jets"));
//     ANA_CHECK(BookHistogram("h_presel_nel", 5, 0, 5));
//     ANA_CHECK(BookHistogram("h_presel_nmu", 5, 0, 5));




//    //after mll>120GeV
//     ANA_CHECK(BookHistogram("h_mll120_mll", 35, 0, 3500, "m_{ll} [GeV]"));
//     ANA_CHECK(BookHistogram("h_mll120_mllb", 60, 0, 3000, "m_{llb} [GeV]"));
//     ANA_CHECK(BookHistogram("h_mll120_met", 20, 0, 1000));
//     ANA_CHECK(BookHistogram("h_mll120_metphi", 20, -4, 4));
//     ANA_CHECK(BookHistogram("h_mll120_HT_leptons", 60, 0, 3000));
//     ANA_CHECK(BookHistogram("h_mll120_HT_jets", 60, 0, 3000));
//     ANA_CHECK(BookHistogram("h_mll120_HT_total", 60, 0, 3000, "H_{T} [GeV]"));
//
//     ANA_CHECK(BookHistogram("h_mll120_ptll", 50, 0, 1000));
//     ANA_CHECK(BookHistogram("h_mll120_etall", 20, -4, 4));
//     ANA_CHECK(BookHistogram("h_mll120_phill", 20, -4, 4));
//     ANA_CHECK(BookHistogram("h_mll120_dphill", 20, 0, 4));
//     ANA_CHECK(BookHistogram("h_mll120_detall", 20, 0, 10));
//     ANA_CHECK(BookHistogram("h_mll120_dRll", 20, 0, 10, "#DeltaR (#mu_{1},#mu_{2})"));
//     ANA_CHECK(BookHistogram("h_mll120_dphill_b", 20, 0, 4));
//     ANA_CHECK(BookHistogram("h_mll120_detall_b", 20, 0, 10));
//     ANA_CHECK(BookHistogram("h_mll120_dRll_b", 20, 0, 10, "##DeltaR (#mu_{1}+#mu_{2},b_{1})"));
//
//
//     ANA_CHECK(BookHistogram("h_mll120_leading_lep_pt", 50, 0, 2500, "p_{T}(#mu_{1}) [GeV]"));
//     ANA_CHECK(BookHistogram("h_mll120_sub_leading_lep_pt", 50, 0, 2500, "p_{T}(#mu_{2}) [GeV]"));
//     ANA_CHECK(BookHistogram("h_mll120_leading_lep_eta", 20, -4, 4));
//     ANA_CHECK(BookHistogram("h_mll120_sub_leading_lep_eta", 20, -4, 4));
//     ANA_CHECK(BookHistogram("h_mll120_leading_lep_phi", 20, -4, 4));
//     ANA_CHECK(BookHistogram("h_mll120_sub_leading_lep_phi", 20, -4, 4));
//
//     ANA_CHECK(BookHistogram("h_mll120_leading_jet_pt", 50, 0, 2500, "p_{T}(jet_{1}) [GeV]"));
//     ANA_CHECK(BookHistogram("h_mll120_sub_leading_jet_pt", 50, 0, 2500, "p_{T}(jet_{2}) [GeV]"));
//     ANA_CHECK(BookHistogram("h_mll120_leading_jet_eta", 20, -4, 4));
//     ANA_CHECK(BookHistogram("h_mll120_sub_leading_jet_eta", 20, -4, 4));
//     ANA_CHECK(BookHistogram("h_mll120_leading_jet_phi", 20, -4, 4));
//     ANA_CHECK(BookHistogram("h_mll120_sub_leading_jet_phi", 20, -4, 4));
//
//     ANA_CHECK(BookHistogram("h_mll120_leading_bjet_pt", 50, 0, 2500, "p_{T}(b-tagged jet_{1}) [GeV]"));
//     ANA_CHECK(BookHistogram("h_mll120_sub_leading_bjet_pt", 50, 0, 2500, "p_{T}(b-tagged jet_{2}) [GeV]"));
//     ANA_CHECK(BookHistogram("h_mll120_leading_bjet_eta", 20, -4, 4));
//     ANA_CHECK(BookHistogram("h_mll120_sub_leading_bjet_eta", 20, -4, 4));
//     ANA_CHECK(BookHistogram("h_mll120_leading_bjet_phi", 20, -4, 4));
//     ANA_CHECK(BookHistogram("h_mll120_sub_leading_bjet_phi", 20, -4, 4));
//
//
//     ANA_CHECK(BookHistogram("h_mll120_nj", 10, 0, 10, "Number of Jets"));
//     ANA_CHECK(BookHistogram("h_mll120_nbj", 10, 0, 10, "Number of b-tagged Jets"));
//     ANA_CHECK(BookHistogram("h_mll120_nel", 5, 0, 5));
//     ANA_CHECK(BookHistogram("h_mll120_nmu", 5, 0, 5));
    

    return StatusCode::SUCCESS;
}

StatusCode BSLLAlg :: doAnalysis (){ 

    
  auto electrons=m_data->Electrons();
  auto muons=m_data->Muons();
  auto jets=m_data->Jets();
  auto bjets=m_data->BJets();
  float MET=m_data->CaloMet();
  
  Float_t mll_truth=-999;
  int dsid=-5;
  if(m_datasource=="LQTree_v26"){
    dsid=m_data->dsid();
    mll_truth=m_data->PickLeafValue("truth_mll");
  }
  std::cout<<dsid<<"   "<<mll_truth<<std::endl;

  float w=m_data->EventWeight();
  ANA_MSG_DEBUG("Weight used in making distributions: "<<w);
  float HT_leptons = 0, HT_jets = 0, HT_total = 0 ;
  float leading_lep_pt = 0, leading_lep_eta = 0, leading_lep_phi = 0 ;
  float sub_leading_lep_pt = 0, sub_leading_lep_eta = 0, sub_leading_lep_phi = 0 ;
  float leading_jet_pt = 0, leading_jet_eta = 0, leading_jet_phi = 0 ;
  float sub_leading_jet_pt = 0, sub_leading_jet_eta = 0, sub_leading_jet_phi = 0 ;
  float leading_bjet_pt = 0, leading_bjet_eta = 0, leading_bjet_phi = 0 ;
  float sub_leading_bjet_pt = 0, sub_leading_bjet_eta = 0, sub_leading_bjet_phi = 0 ;

  float dphi_ll = 0, deta_ll = 0, dR_ll = 0, dphi_ll_b, deta_ll_b, dR_ll_b = 0 ;

  auto channel=wk()->metaData()->castString("channel");
  NTAna::Particle Zll;
  NTAna::Particle Zllb;

  int NEL=electrons.size();
  int NMU=muons.size();
  if(channel=="EE"){
      if( NMU != 0 || NEL != 2){
          return StatusCode::SUCCESS;
    }
    Zll=*electrons.at(0)+*electrons.at(1);
    HT_leptons = electrons.at(0)->Pt() + electrons.at(1)->Pt() ;
    leading_lep_pt = electrons.at(0)->Pt() ; sub_leading_lep_pt = electrons.at(1)->Pt() ;
    leading_lep_eta = electrons.at(0)->Eta() ; sub_leading_lep_eta = electrons.at(1)->Eta() ;
    leading_lep_phi = electrons.at(0)->Phi() ; sub_leading_lep_phi = electrons.at(1)->Phi() ;
    dphi_ll = NTAna::Particle::DeltaPhi(electrons.at(0),electrons.at(1));
    deta_ll = electrons.at(0)->Eta() - electrons.at(1)->Eta();
    dR_ll = NTAna::Particle::DeltaR(electrons.at(0),electrons.at(1));

    if(bjets.size() > 0){
  	  dphi_ll_b = Zll.Phi() - bjets.at(0)->Phi() ;
  	  deta_ll_b = Zll.Eta() - bjets.at(0)->Eta() ;

  	  Zllb=*electrons.at(0)+*electrons.at(1)+*bjets.at(0);

  	  dphi_ll_b = fabs(dphi_ll_b) ;
  	  deta_ll_b = fabs(deta_ll_b) ;
  	  if (dphi_ll_b > 3.14159265359) {dphi_ll_b = 2 * 3.14159265359 - dphi_ll_b ; }

  	  dR_ll_b = sqrt(dphi_ll_b * dphi_ll_b + deta_ll_b * deta_ll_b) ;
  	}
  }
  if(channel=="MM"){
      if(NEL != 0 || NMU != 2){
          return StatusCode::SUCCESS;
    }
    Zll=*muons.at(0)+*muons.at(1);
    HT_leptons = muons.at(0)->Pt() + muons.at(1)->Pt() ;
    leading_lep_pt = muons.at(0)->Pt() ; sub_leading_lep_pt = muons.at(1)->Pt() ;
    leading_lep_eta = muons.at(0)->Eta() ; sub_leading_lep_eta = muons.at(1)->Eta() ;
    leading_lep_phi = muons.at(0)->Phi() ; sub_leading_lep_phi = muons.at(1)->Phi() ;
    dphi_ll = NTAna::Particle::DeltaPhi(muons.at(0),muons.at(1));
    deta_ll = muons.at(0)->Eta() - muons.at(1)->Eta();
    dR_ll = NTAna::Particle::DeltaR(muons.at(0),muons.at(1));

    if(bjets.size() > 0){
  	  dphi_ll_b = Zll.Phi() - bjets.at(0)->Phi() ;
  	  deta_ll_b = Zll.Eta() - bjets.at(0)->Eta() ;

  	  Zllb=*muons.at(0)+*muons.at(1)+*bjets.at(0);

  	  dphi_ll_b = fabs(dphi_ll_b) ;
  	  deta_ll_b = fabs(deta_ll_b) ;
  	  if (dphi_ll_b > 3.14159265359) {dphi_ll_b = 2 * 3.14159265359 - dphi_ll_b ; }

  	  dR_ll_b = sqrt(dphi_ll_b * dphi_ll_b + deta_ll_b * deta_ll_b) ;
  	}
  }
  

  dphi_ll = fabs(dphi_ll) ;
  deta_ll = fabs(deta_ll) ;
  if (dphi_ll > 3.14159265359) {dphi_ll = 2 * 3.14159265359 - dphi_ll ; }


  for(uint i_jet = 0 ; i_jet < jets.size() ; i_jet++)
  {
	  HT_jets = HT_jets + jets.at(i_jet)->Pt() ;
	  if (i_jet == 0) {leading_jet_pt = jets.at(i_jet)->Pt() ; leading_jet_eta = jets.at(i_jet)->Eta() ; leading_jet_phi = jets.at(i_jet)->Phi() ; }
	  if (i_jet == 1) {sub_leading_jet_pt = jets.at(i_jet)->Pt() ; sub_leading_jet_eta = jets.at(i_jet)->Eta() ; sub_leading_jet_phi = jets.at(i_jet)->Phi() ; }
  }


  for(uint i_bjet = 0 ; i_bjet < bjets.size() ; i_bjet++)
  {
	  if (i_bjet == 0) {leading_bjet_pt = bjets.at(i_bjet)->Pt() ; leading_bjet_eta = bjets.at(i_bjet)->Eta() ; leading_bjet_phi = bjets.at(i_bjet)->Phi() ; }
	  if (i_bjet == 1) {sub_leading_bjet_pt = bjets.at(i_bjet)->Pt() ; sub_leading_bjet_eta = bjets.at(i_bjet)->Eta() ; sub_leading_bjet_phi = bjets.at(i_bjet)->Phi() ; }
  }


  HT_total = HT_leptons + HT_jets ;

  float MLL=Zll.M();
  float PTLL=Zll.Pt();
  float ETALL=Zll.Eta();
  float PHILL=Zll.Phi();
  int NJ=jets.size();
  int NBJ=bjets.size();

  float MLLB=Zllb.M();

  //if(NJ == 0){return StatusCode::SUCCESS; }

//  if(NBJ != 1){return StatusCode::SUCCESS; }
//  if(bjets.at(0)->Pt() < jets.at(0)->Pt()){return StatusCode::SUCCESS; }



    if (MLL > 400.) FillHistogram("h_SR_mll_400",HT_total,w);
    if (MLL > 500.) FillHistogram("h_SR_mll_500",HT_total,w);
    if (MLL > 600.) FillHistogram("h_SR_mll_600",HT_total,w);
    if (MLL > 700.) FillHistogram("h_SR_mll_700",HT_total,w);
    if (MLL > 800.) FillHistogram("h_SR_mll_800",HT_total,w);
    if (MLL > 900.) FillHistogram("h_SR_mll_900",HT_total,w);
    if (MLL > 1000.) FillHistogram("h_SR_mll_1000",HT_total,w);
    if (MLL > 1100.) FillHistogram("h_SR_mll_1100",HT_total,w);
    if (MLL > 1200.) FillHistogram("h_SR_mll_1200",HT_total,w);
    if (MLL > 1300.) FillHistogram("h_SR_mll_1300",HT_total,w);
    if (MLL > 1400.) FillHistogram("h_SR_mll_1400",HT_total,w);
    if (MLL > 1500.) FillHistogram("h_SR_mll_1500",HT_total,w);
    if (MLL > 1600.) FillHistogram("h_SR_mll_1600",HT_total,w);
    if (MLL > 1700.) FillHistogram("h_SR_mll_1700",HT_total,w);
    if (MLL > 1800.) FillHistogram("h_SR_mll_1800",HT_total,w);
    if (MLL > 1900.) FillHistogram("h_SR_mll_1900",HT_total,w);
    if (MLL > 2000.) FillHistogram("h_SR_mll_2000",HT_total,w);
    if (MLL > 2100.) FillHistogram("h_SR_mll_2100",HT_total,w);
    if (MLL > 2200.) FillHistogram("h_SR_mll_2200",HT_total,w);
    if (MLL > 2300.) FillHistogram("h_SR_mll_2300",HT_total,w);
    if (MLL > 2400.) FillHistogram("h_SR_mll_2400",HT_total,w);
    if (MLL > 2500.) FillHistogram("h_SR_mll_2500",HT_total,w);

//  if (HT_total > 350.) FillHistogram("h_SR1_N_1_mll",MLL,w);
//  if (MLL > 1700.) FillHistogram("h_SR1_N_1_HT_total",HT_total,w);
//
//  if (MLL > 1700. && HT_total > 350.)
//  {
//
//  FillHistogram("h_SR1_mll",MLL,w);
//  FillHistogram("h_SR1_mllb",MLLB,w);
//  FillHistogram("h_SR1_met",MET,w);
//  //FillHistogram("h_SR1_metphi",MET,w);
//  FillHistogram("h_SR1_HT_jets",HT_jets,w);
//  FillHistogram("h_SR1_HT_leptons",HT_leptons,w);
//  FillHistogram("h_SR1_HT_total",HT_total,w);
//
//  FillHistogram("h_SR1_ptll",PTLL,w);
//  FillHistogram("h_SR1_etall",ETALL,w);
//  FillHistogram("h_SR1_phill",PHILL,w);
//  FillHistogram("h_SR1_dphill",dphi_ll,w);
//  FillHistogram("h_SR1_detall",deta_ll,w);
//  FillHistogram("h_SR1_dRll",dR_ll,w);
//  FillHistogram("h_SR1_dphill_b",dphi_ll_b,w);
//  FillHistogram("h_SR1_detall_b",deta_ll_b,w);
//  FillHistogram("h_SR1_dRll_b",dR_ll_b,w);
//
//
//  FillHistogram("h_SR1_leading_lep_pt",leading_lep_pt,w);
//  FillHistogram("h_SR1_sub_leading_lep_pt",sub_leading_lep_pt,w);
//  FillHistogram("h_SR1_leading_lep_eta",leading_lep_eta,w);
//  FillHistogram("h_SR1_sub_leading_lep_eta",sub_leading_lep_eta,w);
//  FillHistogram("h_SR1_leading_lep_phi",leading_lep_phi,w);
//  FillHistogram("h_SR1_sub_leading_lep_phi",sub_leading_lep_phi,w);
//
//  FillHistogram("h_SR1_leading_jet_pt",leading_jet_pt,w);
//  FillHistogram("h_SR1_sub_leading_jet_pt",sub_leading_jet_pt,w);
//  FillHistogram("h_SR1_leading_jet_eta",leading_jet_eta,w);
//  FillHistogram("h_SR1_sub_leading_jet_eta",sub_leading_jet_eta,w);
//  FillHistogram("h_SR1_leading_jet_phi",leading_jet_phi,w);
//  FillHistogram("h_SR1_sub_leading_jet_phi",sub_leading_jet_phi,w);
//
//  FillHistogram("h_SR1_leading_bjet_pt",leading_bjet_pt,w);
//  FillHistogram("h_SR1_sub_leading_bjet_pt",sub_leading_bjet_pt,w);
//  FillHistogram("h_SR1_leading_bjet_eta",leading_bjet_eta,w);
//  FillHistogram("h_SR1_sub_leading_bjet_eta",sub_leading_bjet_eta,w);
//  FillHistogram("h_SR1_leading_bjet_phi",leading_bjet_phi,w);
//  FillHistogram("h_SR1_sub_leading_bjet_phi",sub_leading_bjet_phi,w);
//
//  FillHistogram("h_SR1_nj",NJ,w);
//  FillHistogram("h_SR1_nbj",NBJ,w);
//  FillHistogram("h_SR1_nel",NEL,w);
//  FillHistogram("h_SR1_nmu",NMU,w);
//
//}
//
//
//
//  if (HT_total > 350.) FillHistogram("h_SR2_N_1_mll",MLL,w);
//  if (MLL > 2400.) FillHistogram("h_SR2_N_1_HT_total",HT_total,w);
//
//
//  if (MLL > 2400. && HT_total > 350.)
//  {
//
//  FillHistogram("h_SR2_mll",MLL,w);
//  FillHistogram("h_SR2_mllb",MLLB,w);
//  FillHistogram("h_SR2_met",MET,w);
//  //FillHistogram("h_SR2_metphi",MET,w);
//  FillHistogram("h_SR2_HT_jets",HT_jets,w);
//  FillHistogram("h_SR2_HT_leptons",HT_leptons,w);
//  FillHistogram("h_SR2_HT_total",HT_total,w);
//
//  FillHistogram("h_SR2_ptll",PTLL,w);
//  FillHistogram("h_SR2_etall",ETALL,w);
//  FillHistogram("h_SR2_phill",PHILL,w);
//  FillHistogram("h_SR2_dphill",dphi_ll,w);
//  FillHistogram("h_SR2_detall",deta_ll,w);
//  FillHistogram("h_SR2_dRll",dR_ll,w);
//  FillHistogram("h_SR2_dphill_b",dphi_ll_b,w);
//  FillHistogram("h_SR2_detall_b",deta_ll_b,w);
//  FillHistogram("h_SR2_dRll_b",dR_ll_b,w);
//
//
//  FillHistogram("h_SR2_leading_lep_pt",leading_lep_pt,w);
//  FillHistogram("h_SR2_sub_leading_lep_pt",sub_leading_lep_pt,w);
//  FillHistogram("h_SR2_leading_lep_eta",leading_lep_eta,w);
//  FillHistogram("h_SR2_sub_leading_lep_eta",sub_leading_lep_eta,w);
//  FillHistogram("h_SR2_leading_lep_phi",leading_lep_phi,w);
//  FillHistogram("h_SR2_sub_leading_lep_phi",sub_leading_lep_phi,w);
//
//  FillHistogram("h_SR2_leading_jet_pt",leading_jet_pt,w);
//  FillHistogram("h_SR2_sub_leading_jet_pt",sub_leading_jet_pt,w);
//  FillHistogram("h_SR2_leading_jet_eta",leading_jet_eta,w);
//  FillHistogram("h_SR2_sub_leading_jet_eta",sub_leading_jet_eta,w);
//  FillHistogram("h_SR2_leading_jet_phi",leading_jet_phi,w);
//  FillHistogram("h_SR2_sub_leading_jet_phi",sub_leading_jet_phi,w);
//
//  FillHistogram("h_SR2_leading_bjet_pt",leading_bjet_pt,w);
//  FillHistogram("h_SR2_sub_leading_bjet_pt",sub_leading_bjet_pt,w);
//  FillHistogram("h_SR2_leading_bjet_eta",leading_bjet_eta,w);
//  FillHistogram("h_SR2_sub_leading_bjet_eta",sub_leading_bjet_eta,w);
//  FillHistogram("h_SR2_leading_bjet_phi",leading_bjet_phi,w);
//  FillHistogram("h_SR2_sub_leading_bjet_phi",sub_leading_bjet_phi,w);
//
//  FillHistogram("h_SR2_nj",NJ,w);
//  FillHistogram("h_SR2_nbj",NBJ,w);
//  FillHistogram("h_SR2_nel",NEL,w);
//  FillHistogram("h_SR2_nmu",NMU,w);
//
//}







  if (MLL > 130. && MLL < 250. && leading_lep_pt > 65.)
  {
    FillHistogram("h_CRZ_mll",MLL,w);
  }

  if (MLL > 250. && MLL < 400. && leading_lep_pt > 65.)
  {
    FillHistogram("h_VRZ_mll",MLL,w);
  }

  if (MLL > 130. && leading_lep_pt > 65. && NJ > 1 && NBJ >= 2)
  {
    FillHistogram("h_CRtt_mll",MLL,w);
  }





//  //std::cout << "mll: " << MLL << std::endl ;
//
//  if (HT_total > 0.) FillHistogram("h_opt_HT0_mll", MLL,w);
//  if (HT_total > 350.) FillHistogram("h_opt_HT350_mll", MLL,w);
//  if (HT_total > 400.) FillHistogram("h_opt_HT400_mll", MLL,w);
//  if (HT_total > 450.) FillHistogram("h_opt_HT450_mll", MLL,w);
//  if (HT_total > 500.) FillHistogram("h_opt_HT500_mll", MLL,w);
//  if (HT_total > 550.) FillHistogram("h_opt_HT550_mll", MLL,w);
//  if (HT_total > 600.) FillHistogram("h_opt_HT600_mll", MLL,w);
//  if (HT_total > 650.) FillHistogram("h_opt_HT650_mll", MLL,w);
//  if (HT_total > 700.) FillHistogram("h_opt_HT700_mll", MLL,w);
//  if (HT_total > 750.) FillHistogram("h_opt_HT750_mll", MLL,w);
//  if (HT_total > 800.) FillHistogram("h_opt_HT800_mll", MLL,w);
//  if (HT_total > 850.) FillHistogram("h_opt_HT850_mll", MLL,w);
//  if (HT_total > 900.) FillHistogram("h_opt_HT900_mll", MLL,w);
//  if (HT_total > 950.) FillHistogram("h_opt_HT950_mll", MLL,w);
//  if (HT_total > 1000.) FillHistogram("h_opt_HT1000_mll", MLL,w);
//
//
//  if (HT_total > 350. && MLLB > 1000.) FillHistogram("h_opt_HT350_mllb1000_mll", MLL,w);
//  if (HT_total > 350. && MLLB > 1100.) FillHistogram("h_opt_HT350_mllb1100_mll", MLL,w);
//  if (HT_total > 350. && MLLB > 1200.) FillHistogram("h_opt_HT350_mllb1200_mll", MLL,w);
//  if (HT_total > 350. && MLLB > 1300.) FillHistogram("h_opt_HT350_mllb1300_mll", MLL,w);
//  if (HT_total > 350. && MLLB > 1400.) FillHistogram("h_opt_HT350_mllb1400_mll", MLL,w);
//  if (HT_total > 350. && MLLB > 1500.) FillHistogram("h_opt_HT350_mllb1500_mll", MLL,w);
//  if (HT_total > 350. && MLLB > 1600.) FillHistogram("h_opt_HT350_mllb1600_mll", MLL,w);
//  if (HT_total > 350. && MLLB > 1700.) FillHistogram("h_opt_HT350_mllb1700_mll", MLL,w);
//  if (HT_total > 350. && MLLB > 1800.) FillHistogram("h_opt_HT350_mllb1800_mll", MLL,w);
//  if (HT_total > 350. && MLLB > 1900.) FillHistogram("h_opt_HT350_mllb1900_mll", MLL,w);
//  if (HT_total > 350. && MLLB > 2000.) FillHistogram("h_opt_HT350_mllb2000_mll", MLL,w);
//  if (HT_total > 350. && MLLB > 2100.) FillHistogram("h_opt_HT350_mllb2100_mll", MLL,w);
//  if (HT_total > 350. && MLLB > 2200.) FillHistogram("h_opt_HT350_mllb2200_mll", MLL,w);
//  if (HT_total > 350. && MLLB > 2300.) FillHistogram("h_opt_HT350_mllb2300_mll", MLL,w);
//  if (HT_total > 350. && MLLB > 2400.) FillHistogram("h_opt_HT350_mllb2400_mll", MLL,w);
//
//
//
//
//  if (MET < 80.) FillHistogram("h_opt_MET80_mll", MLL,w);
//  if (MET < 100.) FillHistogram("h_opt_MET100_mll", MLL,w);
//  if (MET < 120.) FillHistogram("h_opt_MET120_mll", MLL,w);
//  if (MET < 140.) FillHistogram("h_opt_MET140_mll", MLL,w);
//  if (MET < 160.) FillHistogram("h_opt_MET160_mll", MLL,w);
//  if (MET < 180.) FillHistogram("h_opt_MET180_mll", MLL,w);
//  if (MET < 200.) FillHistogram("h_opt_MET200_mll", MLL,w);
//
//  if (MET < 80.) FillHistogram("h_opt_MET80_mllb", MLLB,w);
//  if (MET < 100.) FillHistogram("h_opt_MET100_mllb", MLLB,w);
//  if (MET < 120.) FillHistogram("h_opt_MET120_mllb", MLLB,w);
//  if (MET < 140.) FillHistogram("h_opt_MET140_mllb", MLLB,w);
//  if (MET < 160.) FillHistogram("h_opt_MET160_mllb", MLLB,w);
//  if (MET < 180.) FillHistogram("h_opt_MET180_mllb", MLLB,w);
//  if (MET < 200.) FillHistogram("h_opt_MET200_mllb", MLLB,w);






//  FillHistogram("h_presel_mll",MLL,w);
//  FillHistogram("h_presel_mllb",MLLB,w);
//  FillHistogram("h_presel_met",MET,w);
//  //FillHistogram("h_presel_metphi",MET,w);
//  FillHistogram("h_presel_HT_jets",HT_jets,w);
//  FillHistogram("h_presel_HT_leptons",HT_leptons,w);
//  FillHistogram("h_presel_HT_total",HT_total,w);
//
//  FillHistogram("h_presel_ptll",PTLL,w);
//  FillHistogram("h_presel_etall",ETALL,w);
//  FillHistogram("h_presel_phill",PHILL,w);
//  FillHistogram("h_presel_dphill",dphi_ll,w);
//  FillHistogram("h_presel_detall",deta_ll,w);
//  FillHistogram("h_presel_dRll",dR_ll,w);
//  FillHistogram("h_presel_dphill_b",dphi_ll_b,w);
//  FillHistogram("h_presel_detall_b",deta_ll_b,w);
//  FillHistogram("h_presel_dRll_b",dR_ll_b,w);
//
//  FillHistogram("h_presel_leading_lep_pt",leading_lep_pt,w);
//  FillHistogram("h_presel_sub_leading_lep_pt",sub_leading_lep_pt,w);
//  FillHistogram("h_presel_leading_lep_eta",leading_lep_eta,w);
//  FillHistogram("h_presel_sub_leading_lep_eta",sub_leading_lep_eta,w);
//  FillHistogram("h_presel_leading_lep_phi",leading_lep_phi,w);
//  FillHistogram("h_presel_sub_leading_lep_phi",sub_leading_lep_phi,w);
//
//  FillHistogram("h_presel_leading_jet_pt",leading_jet_pt,w);
//  FillHistogram("h_presel_sub_leading_jet_pt",sub_leading_jet_pt,w);
//  FillHistogram("h_presel_leading_jet_eta",leading_jet_eta,w);
//  FillHistogram("h_presel_sub_leading_jet_eta",sub_leading_jet_eta,w);
//  FillHistogram("h_presel_leading_jet_phi",leading_jet_phi,w);
//  FillHistogram("h_presel_sub_leading_jet_phi",sub_leading_jet_phi,w);
//
//  FillHistogram("h_presel_leading_bjet_pt",leading_bjet_pt,w);
//  FillHistogram("h_presel_sub_leading_bjet_pt",sub_leading_bjet_pt,w);
//  FillHistogram("h_presel_leading_bjet_eta",leading_bjet_eta,w);
//  FillHistogram("h_presel_sub_leading_bjet_eta",sub_leading_bjet_eta,w);
//  FillHistogram("h_presel_leading_bjet_phi",leading_bjet_phi,w);
//  FillHistogram("h_presel_sub_leading_bjet_phi",sub_leading_bjet_phi,w);
//
//  FillHistogram("h_presel_nj",NJ,w);
//  FillHistogram("h_presel_nbj",NBJ,w);
//  FillHistogram("h_presel_nel",NEL,w);
//  FillHistogram("h_presel_nmu",NMU,w);
//
//
//
//
//
//  if(MLL<120.) return StatusCode::SUCCESS;
//  FillHistogram("h_mll120_mll",MLL,w);
//  FillHistogram("h_mll120_mllb",MLLB,w);
//  FillHistogram("h_mll120_met",MET,w);
//  //FillHistogram("h_mll120_metphi",MET,w);
//  FillHistogram("h_mll120_HT_jets",HT_jets,w);
//  FillHistogram("h_mll120_HT_leptons",HT_leptons,w);
//  FillHistogram("h_mll120_HT_total",HT_total,w);
//
//  FillHistogram("h_mll120_ptll",PTLL,w);
//  FillHistogram("h_mll120_etall",ETALL,w);
//  FillHistogram("h_mll120_phill",PHILL,w);
//  FillHistogram("h_mll120_dphill",dphi_ll,w);
//  FillHistogram("h_mll120_detall",deta_ll,w);
//  FillHistogram("h_mll120_dRll",dR_ll,w);
//  FillHistogram("h_mll120_dphill_b",dphi_ll_b,w);
//  FillHistogram("h_mll120_detall_b",deta_ll_b,w);
//  FillHistogram("h_mll120_dRll_b",dR_ll_b,w);
//
//  FillHistogram("h_mll120_leading_lep_pt",leading_lep_pt,w);
//  FillHistogram("h_mll120_sub_leading_lep_pt",sub_leading_lep_pt,w);
//  FillHistogram("h_mll120_leading_lep_eta",leading_lep_eta,w);
//  FillHistogram("h_mll120_sub_leading_lep_eta",sub_leading_lep_eta,w);
//  FillHistogram("h_mll120_leading_lep_phi",leading_lep_phi,w);
//  FillHistogram("h_mll120_sub_leading_lep_phi",sub_leading_lep_phi,w);
//
//  FillHistogram("h_mll120_leading_jet_pt",leading_jet_pt,w);
//  FillHistogram("h_mll120_sub_leading_jet_pt",sub_leading_jet_pt,w);
//  FillHistogram("h_mll120_leading_jet_eta",leading_jet_eta,w);
//  FillHistogram("h_mll120_sub_leading_jet_eta",sub_leading_jet_eta,w);
//  FillHistogram("h_mll120_leading_jet_phi",leading_jet_phi,w);
//  FillHistogram("h_mll120_sub_leading_jet_phi",sub_leading_jet_phi,w);
//
//  FillHistogram("h_mll120_leading_bjet_pt",leading_bjet_pt,w);
//  FillHistogram("h_mll120_sub_leading_bjet_pt",sub_leading_bjet_pt,w);
//  FillHistogram("h_mll120_leading_bjet_eta",leading_bjet_eta,w);
//  FillHistogram("h_mll120_sub_leading_bjet_eta",sub_leading_bjet_eta,w);
//  FillHistogram("h_mll120_leading_bjet_phi",leading_bjet_phi,w);
//  FillHistogram("h_mll120_sub_leading_bjet_phi",sub_leading_bjet_phi,w);
//
//  FillHistogram("h_mll120_nj",NJ,w);
//  FillHistogram("h_mll120_nbj",NBJ,w);
//  FillHistogram("h_mll120_nel",NEL,w);
//  FillHistogram("h_mll120_nmu",NMU,w);
//



  
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  return StatusCode::SUCCESS;
}



StatusCode BSLLAlg :: finalize (){
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged
  return StatusCode::SUCCESS;
}


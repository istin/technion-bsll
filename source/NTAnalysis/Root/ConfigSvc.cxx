/* author   :   Serhat Istin
<mailto:istin@cern.ch>
*/


#include "NTAnalysis/ConfigSvc.h"
#include <fstream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <iostream>
namespace NTAna{

ConfigSvc* ConfigSvc::m_inst=nullptr;

 
size_t SampleGroup::hash(){

    std::hash<std::string> hasher;
    return hasher(name+pattern);
}

ConfigSvc* ConfigSvc::init(const std::string& fname){
    if (m_inst == 0){
        m_inst = new ConfigSvc(fname);
    }

    return m_inst;
}
    
ConfigSvc::~ConfigSvc(){
    delete m_inst;
}

 
 
void ConfigSvc::dump(){
    std::cout<<"++++++ ConfigSvc Dump ++++++"<<std::endl;
    std::cout<<"Driver  = "<<m_driver <<std::endl;
    std::cout<<"UseSampleMetaData  = "<<m_writeSampleMetaData <<std::endl;
    std::cout<<"SampleMetaDataFile = "<< m_sampleMetaDataFile<<std::endl;
    std::cout<<"BlackListedSamples = "<< m_blacklistedSamples<<std::endl;
    auto print=[](const auto& ele){std::cout<<"*) "<<ele<<std::endl;};
    std::cout<<"*** Allowed Algorithms are *** "<<std::endl;
    std::for_each(m_allowedAlgorithms.begin(),m_allowedAlgorithms.end(),print);
    std::cout<<"\n****************************** "<<std::endl;
    

    

    std::cout<<"**********************************"<<std::endl;
    std::cout<<"Cross section units in ["<<m_xcnunit<<"]"<<std::endl;
    std::cout<<"Integrated Lumi unit in ["<<m_intlumiunit<<"]"<<std::endl;
    
    std::cout<<"\n****Sample Groups are ****"<<std::endl;
    auto printSampGroup=[](const auto & sg){
        std::cout<<"========================"<<std::endl;
        std::cout<<"name="<<sg.name<<std::endl;
        std::cout<<"pattern="<<sg.pattern<<std::endl;
        std::cout<<"========================"<<std::endl;
    };
    std::for_each(m_sampleGroups.begin(),m_sampleGroups.end(),printSampGroup);
    std::cout<<"**************************"<<std::endl;
    
    std::cout<<"++++++ End of ConfigSvc Dump ++++++"<<std::endl;
    
} 
 
ConfigSvc::ConfigSvc(const std::string& fname){
        std::ifstream file( fname );
        std::stringstream stream;;
        stream << file.rdbuf();


        boost::property_tree::ptree pt;
        boost::property_tree::read_json(stream, pt);
        
        m_writeSampleMetaData=pt.get<bool>("RunConfig.WriteSampleMetaData");
        m_sampleMetaDataFile=pt.get<std::string>("RunConfig.SampleMetaDataFile");
        m_blacklistedSamples=pt.get<std::string>("RunConfig.BlackListedSamples");
        m_driver=pt.get<std::string>("RunConfig.DefaultDriver");
        m_dsidpattern=pt.get<std::string>("RunConfig.dsidpattern");
        m_datapattern=pt.get<std::string>("RunConfig.datapattern");
        
        for (boost::property_tree::ptree::value_type &alwa : pt.get_child("RunConfig.AllowedAlgorithms")){
            auto obj=alwa.second;
            
            m_allowedAlgorithms.push_back(obj.data());
        }

        for (boost::property_tree::ptree::value_type &ro : pt.get_child("RunConfig.mcrounds")){
            auto obj=ro.second;
            
            std::string round=ro.first;
            
            float lumifrac=obj.get<float>("");
            m_lumifracs[round]=lumifrac;            
        }
       std::string xcnunit=pt.get<std::string>("SampleConfig.SampleXCNUnit");
       
       std::string intlumiunit=pt.get<std::string>("SampleConfig.IntLuimiUnit");
       
       if(xcnunit=="nb") m_xcnunit=1E-15;
       else if(xcnunit=="pb") m_xcnunit=1E-18;
       else if(xcnunit=="fb") m_xcnunit=1E-21;
       else{
           std::cerr<<"xcn unit must be either nb/pb/fb"<<std::endl;
           exit(8);
       }
       if(intlumiunit=="inb") m_intlumiunit=1E15;
       else if(intlumiunit=="ipb") m_intlumiunit=1E18;
       else if(intlumiunit=="ifb") m_intlumiunit=1E21;
       else{
           std::cerr<<"intlumiunit unit must be either inb/ipb/ifb"<<std::endl;
           exit(8);
       }       
        for (boost::property_tree::ptree::value_type &a : pt.get_child("SampleConfig.SampleGroups")){

          auto obj=a.second;
          SampleGroup sg;
          sg.name=obj.get<std::string>("name");
          sg.pattern=obj.get<std::string>("pattern");
          m_sampleGroups.push_back(sg);
        }
      
    }    
}

#include <NTAnalysis/GenericAlg.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <EventLoop/Worker.h>
#include <SampleHandler/MetaObject.h>
#include <TFile.h>
GenericAlg :: GenericAlg (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : Skeleton (name, pSvcLocator){

}


StatusCode GenericAlg::BookHistograms(){
    
    ANA_CHECK (book (TH1F ("h_EventWeight", "Event Weight", 80, -5, 5)));
    ANA_CHECK (book (TH1F ("h_presel_mll", "m_{ll}", 40, 0, 800)));
    ANA_CHECK (book (TH1F ("h_presel_ptll", "pT_{ll}", 70, 0, 700)));
    ANA_CHECK (book (TH1F ("h_presel_etall", "eta_{ll}", 20, -5, 5))); 
    ANA_CHECK (book (TH1F ("h_presel_phill", "phi_{ll}", 16, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_presel_nel", "#_{e}", 5, 0, 5)));
    ANA_CHECK (book (TH1F ("h_presel_nmu", "n_{#mu}", 5, 0, 5)));
    ANA_CHECK (book (TH1F ("h_presel_nj", "n_{j}", 10, 0, 10)));
    ANA_CHECK (book (TH1F ("h_presel_nbj", "n_{bj}", 6, 0, 6)));
    ANA_CHECK (book (TH1F ("h_presel_ptj", "pT_{j}", 40, 0, 800)));
    ANA_CHECK (book (TH1F ("h_presel_etaj", "eta_{j}", 20, -5, 5)));
    ANA_CHECK (book (TH1F ("h_presel_phij", "phi_{j}", 16, -3.2, 3.2)));    
    ANA_CHECK (book (TH1F ("h_presel_ej", "E_{j}", 50, 0, 1000))); 
    
    ANA_CHECK (book (TH1F ("h_presel_ptj0", "pT_{j0}", 40, 0, 800)));
    ANA_CHECK (book (TH1F ("h_presel_etaj0", "eta_{j0}", 20, -5, 5)));
    ANA_CHECK (book (TH1F ("h_presel_phij0", "phi_{j0}", 16, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_presel_ej0", "E_{j0}", 50, 0, 1000)));    

    ANA_CHECK (book (TH1F ("h_presel_dphill", "Delta#phi_{ll}", 16, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_presel_dRll", "DeltaR_{ll}", 16, 0, 8)));


    //z-window is not a cut
    ANA_CHECK (book (TH1F ("h_zwin_mll", "m_{ll}", 30, 75, 105)));
    ANA_CHECK (book (TH1F ("h_zwin_ptll", "pT_{ll}", 70, 0, 700)));
    ANA_CHECK (book (TH1F ("h_zwin_etall", "eta_{ll}", 20, -5, 5))); 
    ANA_CHECK (book (TH1F ("h_zwin_phill", "phi_{ll}", 16, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_zwin_nel", "n_{e}", 5, 0, 5)));
    ANA_CHECK (book (TH1F ("h_zwin_nmu", "n_{#mu}", 5, 0, 5)));
    ANA_CHECK (book (TH1F ("h_zwin_nj", "n_{j}", 10, 0, 10)));
    ANA_CHECK (book (TH1F ("h_zwin_nbj", "n_{bj}", 6, 0, 6)));
    ANA_CHECK (book (TH1F ("h_zwin_ptj", "pT_{j}", 40, 0, 800)));
    ANA_CHECK (book (TH1F ("h_zwin_etaj", "eta_{j}", 20, -5, 5)));
    ANA_CHECK (book (TH1F ("h_zwin_phij", "phi_{j}", 16, -3.2, 3.2)));    
    ANA_CHECK (book (TH1F ("h_zwin_ej", "E_{j}", 50, 0, 1000))); 
    
    ANA_CHECK (book (TH1F ("h_zwin_ptj0", "pT_{j0}", 40, 0, 800)));
    ANA_CHECK (book (TH1F ("h_zwin_etaj0", "eta_{j0}", 20, -5, 5)));
    ANA_CHECK (book (TH1F ("h_zwin_phij0", "phi_{j0}", 16, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_zwin_ej0", "E_{j0}", 50, 0, 1000)));    
    
    ANA_CHECK (book (TH1F ("h_zwin_dphill", "Delta#phi_{ll}", 16, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_zwin_dRll", "DeltaR_{ll}", 16, 0, 8)));

//ptll>20 GeV cut
    ANA_CHECK (book (TH1F ("h_ptll20_mll", "m_{ll}", 40, 0, 800)));
    ANA_CHECK (book (TH1F ("h_ptll20_mll_zwin", "m_{ll}", 30, 75, 105)));
    ANA_CHECK (book (TH1F ("h_ptll20_ptll", "pT_{ll}", 70, 0, 700)));
    ANA_CHECK (book (TH1F ("h_ptll20_etall", "eta_{ll}", 20, -5, 5))); 
    ANA_CHECK (book (TH1F ("h_ptll20_phill", "phi_{ll}", 16, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_ptll20_nel", "n_{e}", 5, 0, 5)));
    ANA_CHECK (book (TH1F ("h_ptll20_nmu", "n_{#mu}", 5, 0, 5)));
    ANA_CHECK (book (TH1F ("h_ptll20_nj", "n_{j}", 10, 0, 10)));
    ANA_CHECK (book (TH1F ("h_ptll20_nbj", "n_{bj}", 6, 0, 6)));
    ANA_CHECK (book (TH1F ("h_ptll20_ptj", "pT_{j}", 40, 0, 800)));
    ANA_CHECK (book (TH1F ("h_ptll20_etaj", "eta_{j}", 20, -5, 5)));
    ANA_CHECK (book (TH1F ("h_ptll20_phij", "phi_{j}", 16, -3.2, 3.2)));    
    ANA_CHECK (book (TH1F ("h_ptll20_ej", "E_{j}", 50, 0, 1000))); 
    
    ANA_CHECK (book (TH1F ("h_ptll20_ptj0", "pT_{j0}", 40, 0, 800)));
    ANA_CHECK (book (TH1F ("h_ptll20_etaj0", "eta_{j0}", 20, -5, 5)));
    ANA_CHECK (book (TH1F ("h_ptll20_phij0", "phi_{j0}", 16, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_ptll20_ej0", "E_{j0}", 50, 0, 1000)));    

    ANA_CHECK (book (TH1F ("h_ptll20_dphill", "Delta#phi_{ll}", 16, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_ptll20_dRll", "DeltaR_{ll}", 16, 0, 8)));


//>=2 jets cut


//ptll>20 GeV cut
    ANA_CHECK (book (TH1F ("h_2J_mll", "m_{ll}", 40, 0, 800)));
    ANA_CHECK (book (TH1F ("h_2J_mll_zwin", "m_{ll}", 30, 75, 105)));
    ANA_CHECK (book (TH1F ("h_2J_ptll", "pT_{ll}", 70, 0, 700)));
    ANA_CHECK (book (TH1F ("h_2J_etall", "eta_{ll}", 20, -5, 5))); 
    ANA_CHECK (book (TH1F ("h_2J_phill", "phi_{ll}", 16, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_2J_nel", "n_{e}", 5, 0, 5)));
    ANA_CHECK (book (TH1F ("h_2J_nmu", "n_{#mu}", 5, 0, 5)));
    ANA_CHECK (book (TH1F ("h_2J_nj", "n_{j}", 10, 0, 10)));
    ANA_CHECK (book (TH1F ("h_2J_nbj", "n_{bj}", 6, 0, 6)));
    ANA_CHECK (book (TH1F ("h_2J_ptj", "pT_{j}", 40, 0, 800)));
    ANA_CHECK (book (TH1F ("h_2J_etaj", "eta_{j}", 20, -5, 5)));
    ANA_CHECK (book (TH1F ("h_2J_phij", "phi_{j}", 16, -3.2, 3.2)));    
    ANA_CHECK (book (TH1F ("h_2J_ej", "E_{j}", 50, 0, 1000))); 
    
    ANA_CHECK (book (TH1F ("h_2J_ptj0", "pT_{j0}", 40, 0, 800)));
    ANA_CHECK (book (TH1F ("h_2J_etaj0", "eta_{j0}", 20, -5, 5)));
    ANA_CHECK (book (TH1F ("h_2J_phij0", "phi_{j0}", 16, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_2J_ej0", "E_{j0}", 50, 0, 1000)));    

    ANA_CHECK (book (TH1F ("h_2J_dphill", "Delta#phi_{ll}", 16, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_2J_dRll", "DeltaR_{ll}", 16, 0, 8)));



    return StatusCode::SUCCESS;
}

StatusCode GenericAlg::doAnalysis(){

    auto channel=wk()->metaData()->castString("channel");

    const auto& electrons=m_data->Electrons();
    const auto& muons=m_data->Muons();
    const auto& jets=m_data->Jets();
    const auto& bjets=m_data->BJets();
    const float MET=m_data->CaloMet();
    const unsigned  int NEL=electrons.size();
    const unsigned int NMU=muons.size();
    const unsigned int NJETS=jets.size();

     float dRll=-1;
     float dPhill=-1;
    
    NTAna::Particle dilepton;
  
    if(channel=="EE"){
      if( NMU != 0 || NEL != 2){
          return StatusCode::SUCCESS;
      }
      dilepton=*electrons.at(0)+*electrons.at(1);
      dRll=NTAna::Particle::DeltaR(electrons.at(0),electrons.at(1));
      dPhill=NTAna::Particle::DeltaPhi(electrons.at(0),electrons.at(1));
    }
    if(channel=="MM"){
      if( NEL != 0 || NMU != 2){
          return StatusCode::SUCCESS;
      }
      dilepton=*muons.at(0)+*muons.at(1);
      dRll=NTAna::Particle::DeltaR(muons.at(0),muons.at(1));
      dPhill=NTAna::Particle::DeltaPhi(muons.at(0),muons.at(1));
    }

    const float weight=m_data->EventWeight();
    hist("h_EventWeight")->Fill(weight,1);
    hist("h_presel_mll")->Fill (dilepton.M(),weight);  
    hist("h_presel_ptll")->Fill (dilepton.Pt(),weight);
    hist("h_presel_etall")->Fill (dilepton.Eta(),weight);
    hist("h_presel_phill")->Fill (dilepton.Phi(),weight);

    hist("h_presel_dphill")->Fill(dPhill,weight);
    hist("h_presel_dRll")->Fill(dRll,weight);

    hist("h_presel_nel")->Fill (electrons.size(),weight);  
    hist("h_presel_nmu")->Fill (muons.size(),weight);
    hist("h_presel_nj")->Fill (jets.size(),weight);
    hist("h_presel_nbj")->Fill (bjets.size(),weight);

    if(jets.size()!=0){
        hist("h_presel_ptj0")->Fill (jets.at(0)->Pt(),weight);  
        hist("h_presel_etaj0")->Fill (jets.at(0)->Eta(),weight);
        hist("h_presel_phij0")->Fill (jets.at(0)->Phi(),weight);
        hist("h_presel_ej0")->Fill (jets.at(0)->E(),weight);

    }

    for(const auto& jet: jets){
        hist("h_presel_ptj")->Fill (jet->Pt(),weight);  
        hist("h_presel_etaj")->Fill (jet->Eta(),weight);
        hist("h_presel_phij")->Fill (jet->Phi(),weight);
         hist("h_presel_ej")->Fill (jet->E(),weight);
    }

    const float deltaMZ=fabs(91.-dilepton.M());

    if(deltaMZ<15.){
        hist("h_zwin_mll")->Fill (dilepton.M(),weight);  
        hist("h_zwin_ptll")->Fill (dilepton.Pt(),weight);
        hist("h_zwin_etall")->Fill (dilepton.Eta(),weight);
        hist("h_zwin_phill")->Fill (dilepton.Phi(),weight);

        hist("h_zwin_dphill")->Fill(dPhill,weight);
        hist("h_zwin_dRll")->Fill(dRll,weight);

        hist("h_zwin_nel")->Fill (electrons.size(),weight);  
        hist("h_zwin_nmu")->Fill (muons.size(),weight);
        hist("h_zwin_nj")->Fill (jets.size(),weight);
        hist("h_zwin_nbj")->Fill (bjets.size(),weight);

        if(jets.size()!=0){
            hist("h_zwin_ptj0")->Fill (jets.at(0)->Pt(),weight);  
            hist("h_zwin_etaj0")->Fill (jets.at(0)->Eta(),weight);
            hist("h_zwin_phij0")->Fill (jets.at(0)->Phi(),weight);
            hist("h_zwin_ej0")->Fill (jets.at(0)->E(),weight);
        }

        for(const auto& jet: jets){
            hist("h_zwin_ptj")->Fill (jet->Pt(),weight);  
            hist("h_zwin_etaj")->Fill (jet->Eta(),weight);
            hist("h_zwin_phij")->Fill (jet->Phi(),weight);
            hist("h_zwin_ej")->Fill (jet->E(),weight);
        }
    }//eof zwin

    if(dilepton.Pt()<20) return StatusCode::SUCCESS;
    hist("h_ptll20_mll")->Fill (dilepton.M(),weight);
    hist("h_ptll20_mll_zwin")->Fill (dilepton.M(),weight); 
    hist("h_ptll20_ptll")->Fill (dilepton.Pt(),weight);
    hist("h_ptll20_etall")->Fill (dilepton.Eta(),weight);
    hist("h_ptll20_phill")->Fill (dilepton.Phi(),weight);
    hist("h_ptll20_dphill")->Fill(dPhill,weight);
    hist("h_ptll20_dRll")->Fill(dRll,weight);
    hist("h_ptll20_nel")->Fill (electrons.size(),weight);  
    hist("h_ptll20_nmu")->Fill (muons.size(),weight);
    hist("h_ptll20_nj")->Fill (jets.size(),weight);
    hist("h_ptll20_nbj")->Fill (bjets.size(),weight);

    if(jets.size()!=0){
        hist("h_ptll20_ptj0")->Fill (jets.at(0)->Pt(),weight);  
        hist("h_ptll20_etaj0")->Fill (jets.at(0)->Eta(),weight);
        hist("h_ptll20_phij0")->Fill (jets.at(0)->Phi(),weight);
        hist("h_ptll20_ej0")->Fill (jets.at(0)->E(),weight);

    }

    for(const auto& jet: jets){
        hist("h_ptll20_ptj")->Fill (jet->Pt(),weight);  
        hist("h_ptll20_etaj")->Fill (jet->Eta(),weight);
        hist("h_ptll20_phij")->Fill (jet->Phi(),weight);
        hist("h_ptll20_ej")->Fill (jet->E(),weight);
    }

    if(NJETS<2) return StatusCode::SUCCESS;

    hist("h_2J_mll")->Fill (dilepton.M(),weight);
    hist("h_2J_mll_zwin")->Fill (dilepton.M(),weight); 
    hist("h_2J_ptll")->Fill (dilepton.Pt(),weight);
    hist("h_2J_etall")->Fill (dilepton.Eta(),weight);
    hist("h_2J_phill")->Fill (dilepton.Phi(),weight);
    hist("h_2J_dphill")->Fill(dPhill,weight);
    hist("h_2J_dRll")->Fill(dRll,weight);

    hist("h_2J_nel")->Fill (electrons.size(),weight);  
    hist("h_2J_nmu")->Fill (muons.size(),weight);
    hist("h_2J_nj")->Fill (jets.size(),weight);
    hist("h_2J_nbj")->Fill (bjets.size(),weight);

    if(jets.size()!=0){
        hist("h_2J_ptj0")->Fill (jets.at(0)->Pt(),weight);  
        hist("h_2J_etaj0")->Fill (jets.at(0)->Eta(),weight);
        hist("h_2J_phij0")->Fill (jets.at(0)->Phi(),weight);
        hist("h_2J_ej0")->Fill (jets.at(0)->E(),weight);

    }
    
    for(const auto& jet: jets){
        hist("h_2J_ptj")->Fill (jet->Pt(),weight);  
        hist("h_2J_etaj")->Fill (jet->Eta(),weight);
        hist("h_2J_phij")->Fill (jet->Phi(),weight);
        hist("h_2J_ej")->Fill (jet->E(),weight);
    }

    return StatusCode::SUCCESS;
}


StatusCode GenericAlg::finalize(){ 
    return StatusCode::SUCCESS;
}


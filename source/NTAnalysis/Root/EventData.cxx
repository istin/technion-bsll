/* author   :   Serhat Istin
<mailto:istin@cern.ch>
*/

#include "NTAnalysis/EventData.h"
#include <TLeaf.h>

NTAna::EventData::EventData(){
        m_weight=1.;
        m_emunit=1;
        m_sf_variation_name="notset";
        m_sf_variation_scheme=-1;
}


NTAna::EventData::~EventData(){
    Reset();
}
void NTAna::EventData::Next(){//to be called once / per event (inside execute() )
    Reset();
    FetchEvent();
}

void NTAna::EventData::Reset(){
   
    
    for(auto i : m_Electrons){
        delete i;
    }
    
    for(auto j : m_Muons){
        delete j;
    }
    
    for(auto k : m_Jets){
        delete k;
    }

    for(auto k : m_BJets){
        delete k;
    }    
    
    m_Electrons.clear();
    m_Muons.clear();
    m_Jets.clear();
    m_BJets.clear();
}

void NTAna::EventData::setSF_Scheme(int sch,const std::string& sysname){//sysname-->branch name usually
    m_sf_variation_scheme=sch;
    m_sf_variation_name=sysname;
}


void NTAna::EventData::Initialize(TTree* tr){
    Init(tr);//Child Will call it
    m_tree=tr;

}

Double_t NTAna::EventData::PickLeafValue(const std::string& name)const{
    auto br=m_tree->GetBranch(name.c_str());
    if(br==nullptr) {
        std::cerr<<"Unable to retrieve branch : "<<name<<"\n";
        exit(8);
    }
    auto leaf=br->GetLeaf(name.c_str());
    if(leaf==nullptr){
         std::cerr<<"Unable to retrieve leaf : "<<name<<"\n";
         exit(8);
    }
    return leaf->GetValue();
}

Double_t NTAna::EventData::EventWeight()const{
    //the SF variation scheme is set by the base class Skeleton
    if(m_sf_variation_name=="notset" || m_sf_variation_name=="Nominal") return m_weight;
    Double_t w=-999;
    Double_t weight_variation=PickLeafValue(m_sf_variation_name);
    if(weight_variation < -1000) weight_variation=1;//dirty workaround for LQTrees :/
    switch(m_sf_variation_scheme){
        case(SF_VARIATION_SCHEME::MULTIPLIER):
            w= m_weight*weight_variation;
            break;
        case(SF_VARIATION_SCHEME::REPLACE):
            w=weight_variation;
            break;
        default:
            std::cerr<<"Nonsense SF Variation Scheme Set : "<<m_sf_variation_scheme<<std::endl;
            exit(8);
            break;        
    }
    return w;
}
#include <NTAnalysis/TorqueDriverTech.h>

#include <EventLoop/Job.h>
#include <RootCoreUtils/ThrowMsg.h>
#include <TSystem.h>
#include <sstream>
#include <chrono>
#include <thread>

//Serhat Istin : modified already existing torque driver to fit the Technion Cluster
//However array job submission seems to be problematic as David mentioned

//So you better not use this driver

namespace NTAna{

void TorqueDriverTech ::
  batchSubmit (const std::string& location, const SH::MetaObject& options,
               const std::vector<std::size_t>& jobIndices, bool resubmit)
    const
  {
    RCU_READ_INVARIANT (this);

    if (resubmit)
      RCU_THROW_MSG ("resubmission not supported for this driver");

    assert (!jobIndices.empty());
    assert (jobIndices.back() + 1 == jobIndices.size());
    const std::size_t njob = jobIndices.size();
    for(const auto& fragno : jobIndices){
        std::ostringstream cmd;
        cmd << "cd " << location << "/submit && qsub -t "<<fragno<<" "<< options.castString (EL::Job::optSubmitFlags)<< " run";
        if (gSystem->Exec (cmd.str().c_str()) != 0)
        RCU_THROW_MSG (("failed to execute: " + cmd.str()).c_str());
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }
  }

}

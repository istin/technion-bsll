/* author   :   Serhat Istin
<mailto:istin@cern.ch>
*/

#include "NTAnalysis/Particle.h"

namespace NTAna{
    Particle::Particle(){
        m_q=0;
        m_p4=new TLorentzVector();
    }

    Particle::Particle(float pt,float eta,float phi,float E,int q){
        m_p4=new TLorentzVector();
        m_p4->SetPtEtaPhiE(pt,eta,phi,E);
        m_q=q;
    }


    Particle::Particle( TLorentzVector* const  lv,int q){
        m_p4=new TLorentzVector(*lv);
        delete lv;
        m_q=q;
    }
    
    Particle::Particle(const Particle& that){
            m_p4=new TLorentzVector(*that.P4());
            this->m_q=that.Q();
    }
    
     Particle& Particle::operator=( const Particle& that){
       if(this !=&that){
         delete this->m_p4;
         this->m_p4=new TLorentzVector(*that.P4());
         this->m_q=that.Q();
       }
       return *this;  
    }
    
    Particle Particle::operator+(const Particle& other){
        TLorentzVector* lv= new TLorentzVector(*this->P4()+*other.P4());
        Particle p(lv,this->Q()+other.Q());// copy of the new TLorentzVector* is freed in the constructor so dont worry
        return p;
    }

    Particle::~Particle(){
        delete m_p4;
    }
}

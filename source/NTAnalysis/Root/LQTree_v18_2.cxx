#define LQTree_v18_2_cxx
#include "NTAnalysis/LQTree_v18_2.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

void LQTree_v18_2::Loop()
{
//   In a ROOT session, you can do:
//      root> .L LQTree_v18_2.C
//      root> LQTree_v18_2 t
//      root> t.GetEntry(12); // Fill t data members with entry number 12
//      root> t.Show();       // Show values of entry 12
//      root> t.Show(16);     // Read and show values of entry 16
//      root> t.Loop();       // Loop on all entries
//

//     This is the loop skeleton where:
//    jentry is the global entry number in the chain
//    ientry is the entry number in the current Tree
//  Note that the argument to GetEntry must be:
//    jentry for TChain::GetEntry
//    ientry for TTree::GetEntry and TBranch::GetEntry
//
//       To read only selected branches, Insert statements like:
// METHOD1:
//    fChain->SetBranchStatus("*",0);  // disable all branches
//    fChain->SetBranchStatus("branchname",1);  // activate branchname
// METHOD2: replace line
//    fChain->GetEntry(jentry);       //read all branches
//by  b_branchname->GetEntry(ientry); //read only this branch
   if (fChain == 0) return;

   Long64_t nentries = fChain->GetEntriesFast();

   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      // if (Cut(ientry) < 0) continue;
   }
}

void LQTree_v18_2::FetchEvent(){

    m_PU_weight=pileup_weight;
    m_weight=weight;
    m_trkMET=track_met/this->EMUNIT();//track_met/this->EMUNIT();
    m_caloMET=calo_met/this->EMUNIT();//calo_met/this->EMUNIT();
    m_runNo=run_number;
    for(unsigned int i=0;i<electron_n;i++){
        int charge=electron_charge->at(i);
        float pt=electron_pt->at(i)/this->EMUNIT();
        float eta=electron_eta->at(i);
        float phi=electron_phi->at(i);
        float E=electron_e->at(i)/this->EMUNIT();
        NTAna::Electron* ele=new NTAna::Electron(pt,eta,phi,E,charge);
        m_Electrons.push_back(ele);
    }

    for(unsigned int i=0;i<muon_n;i++){
        int charge=muon_charge->at(i);
        float pt=muon_pt->at(i)/this->EMUNIT();
        float eta=muon_eta->at(i);
        float phi=muon_phi->at(i);
        float E=muon_e->at(i)/this->EMUNIT();
        NTAna::Muon* mu=new NTAna::Muon(pt,eta,phi,E,charge);
        m_Muons.push_back(mu);
    }
    
    for(unsigned int i=0;i<jet_n;i++){
        float pt=jet_pt->at(i)/this->EMUNIT();
        float eta=jet_eta->at(i);
        float phi=jet_phi->at(i);
        float E=jet_e->at(i)/this->EMUNIT();
        NTAna::Jet* jet=new NTAna::Jet(pt,eta,phi,E,0);
        jet->setBtag(jet_has_btag->at(i));
        m_Jets.push_back(jet);
        
        if(jet_has_btag->at(i)){
            NTAna::Jet* bjet=new NTAna::Jet(pt,eta,phi,E,0);
            m_BJets.push_back(bjet);
        }
        
    }    
    
    static auto sortbyPt=[&](NTAna::Particle* p1,NTAna::Particle* p2){return p1->Pt()>p2->Pt();};
    std::sort(m_Jets.begin(),m_Jets.end(),sortbyPt);
    std::sort(m_Electrons.begin(),m_Electrons.end(),sortbyPt);
    std::sort(m_Muons.begin(),m_Muons.end(),sortbyPt);
    std::sort(m_BJets.begin(),m_BJets.end(),sortbyPt);
    return;
}






/* author   :   Serhat Istin
<mailto:istin@cern.ch>
*/

#include <AsgTools/MessageCheck.h>
#include <NTAnalysis/Skeleton.h>
#include <EventLoop/Worker.h>
#include "TH1F.h"
#include "TTree.h"
#include <boost/filesystem.hpp>
#include <NTAnalysis/LQTree_v18_2.h>
#include <NTAnalysis/LQTree_v25.h>
#include <NTAnalysis/LQTree_v26.h>
#include <TLeaf.h>
#include <TFile.h>
#include <SampleHandler/MetaObject.h>



Skeleton :: Skeleton (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
    
    //In the new AnalAlgorithm those methods seem to be disabled by default
    //put the below lines to re-enable them. but remember to override the methods here
    
    m_datasource="None";
    m_allowedSysts={};
    declareProperty("configfile", m_config ="UNKNOWN",
                   "Analysis Specific Configuration file");
    
    declareProperty("systematic", m_sys ="Nominal",
                   "Nuissance parameter of choice during run");
    
}


Skeleton::~Skeleton(){
}


bool Skeleton::hasObjectSystematic() const{
     if(m_sys.find("weight_") == std::string::npos && m_sys!="Nominal"){
        return true;
    }
    return false;
}

bool Skeleton::hasSFSystematic() const{
    if(! hasObjectSystematic() && m_sys!="Nominal"){
        return true;
    }
    return false;
}

 StatusCode Skeleton::AllocateAndAssignMetaData(){
    if ( ! wk()->metaData()->castBool("writeMetaData") ) return StatusCode::SUCCESS;
    m_denom=new TParameter<double>("denom",0);
    wk()->addOutput(m_denom);
    if(m_datasource=="LQTree_v18_2" || m_datasource=="LQTree_v25" ||m_datasource=="LQTree_v26" ){
        //Access metadata configured during job submission from SH
        //Embed those inside the resulting rootfiles
        double xcn=wk()->metaData()->castDouble("xcn");
        double kfac=wk()->metaData()->castDouble("kfac");
        double feff=wk()->metaData()->castDouble("feff");
        int did=wk()->metaData()->castInteger("dsid");
        double lumifrac=wk()->metaData()->castDouble("lumifrac");
        auto t_xcn=new TParameter<double>("xcn",xcn);
        auto t_kfac=new TParameter<double>("kfac",kfac);
        auto t_feff=new TParameter<double>("feff",feff);
        auto t_did=new TParameter<int>("dsid",did);
        auto t_lumifrac=new TParameter<double>("lumifrac",lumifrac);
        t_xcn-> SetMergeMode('f');//First! S.I : do not sum while collecting job fragments!
        t_feff-> SetMergeMode('f');
        t_kfac-> SetMergeMode('f');
        t_did-> SetMergeMode('f');
        t_lumifrac-> SetMergeMode('f');
        wk()->addOutput(t_xcn);    
        wk()->addOutput(t_kfac);
        wk()->addOutput(t_feff);
        wk()->addOutput(t_did);
        wk()->addOutput(t_lumifrac);
    }

    else{
        ANA_MSG_FATAL("Skeleton::AllocateAndAssignMetaData() : Unsupported Data Type :"<<m_datasource);
        return StatusCode::FAILURE;
    }

    return StatusCode::SUCCESS;
 }


StatusCode Skeleton :: CheckFileConfiguration(){
    auto status=boost::filesystem::status(m_config.c_str());
    
    if ( ! (status.type() == boost::filesystem::regular_file) ||(status.type() == boost::filesystem::symlink_file) ){
        ANA_MSG_FATAL("Error Reading the configuration file  : "<<m_config);
        return StatusCode::FAILURE;
    }

    ANA_CHECK( ConfigureFromFile() );
    if(m_allowedSysts.size()==0)  ANA_MSG_WARNING("No Configuration File supllied... Using the default"<<m_config);

    if(std::find(m_allowedSysts.begin(),m_allowedSysts.end(),m_sys)==m_allowedSysts.end() && m_sys!="Nominal" ){
        ANA_MSG_FATAL("Supllied systematic \""<<m_sys <<"\" is not in the list of allowed systematics");
        return StatusCode::FAILURE;
    }
            
    if(m_sys=="Nominal"){
        ANA_MSG_DEBUG("No systematic  is set . Using the default "<<m_sys);
    }
            
    else if(this->hasObjectSystematic() ){
        ANA_MSG_DEBUG("Object systematic \""<<m_sys<<"\" is set ...");    
    }
            
    else{
        ANA_MSG_DEBUG("Scale factor systematic \""<<m_sys<<"\" is set ...");
    }

    return StatusCode::SUCCESS;
}


StatusCode Skeleton :: initialize (){
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

    ANA_CHECK(requestFileExecute());
    ANA_CHECK(requestBeginInputFile());
    ANA_CHECK(CheckFileConfiguration());
    ANA_CHECK(BookHistograms());
    ANA_CHECK(AllocateAndAssignMetaData());

    return StatusCode::SUCCESS;

}//end of initialize
 
 
StatusCode Skeleton::ConfigureFromFile(){
    ANA_MSG_DEBUG("Configuring analysis from the file "<<m_config);
    using boost::property_tree::ptree;
    ptree propt;
    read_ini(m_config, propt);

    //Get Properties map
    for (auto& section : propt){
        std::string sectname=section.first;
        if(sectname=="DataConfig") continue;
        for (auto& key : section.second){
            std::string varname=key.first;
            std::string value=key.second.get_value<std::string>();
             m_AnalysisProperties[varname]=value;
        }
    }
    
    auto datasource=propt.get_child_optional( "DataConfig.data_source" );
    
    if(! datasource){
        ANA_MSG_FATAL("Error during base configration from file ");
        ANA_MSG_FATAL("No data_source in DataConfig!");
       return StatusCode::FAILURE;
    }
    m_datasource=propt.get<std::string>("DataConfig.data_source");
    ANA_MSG_DEBUG("Using data source "<<m_datasource);
    std::string eunit=propt.get<std::string>("DataConfig.emunit");
    float enerygom, energymomconv;
    if(eunit=="MeV"){
        enerygom=1E3;
        
    }
    else if(eunit=="GeV"){
        enerygom=1E6;
    }
    else{
        ANA_MSG_ERROR("energy-momentum unit must be GeV or MeV!");
        return StatusCode::FAILURE;
    }
    auto unitconv =propt.get_child_optional( "DataConfig.convert_to" );
    if(unitconv){
        std::string econv=propt.get<std::string>("DataConfig.convert_to");
        if(econv=="MeV"){
            energymomconv=1E3;
            
        }
        else if(econv=="GeV"){
            energymomconv=1E6;
        }
        else{
            ANA_MSG_ERROR("energy-momentum target unit must be GeV or MeV!");
            return StatusCode::FAILURE;
        } 
        
        m_emunit=energymomconv/enerygom;
        ANA_MSG_DEBUG("Energy-momentum will be scaled with a factor of 1/"<<m_emunit);
    }
    else{
        ANA_MSG_DEBUG("No Energy-momentum unit conversion will be applied...");
    }

    auto sysdef =propt.get_child_optional( "DataConfig.syst_defs" );
    if( !sysdef ){
    
        ANA_MSG_DEBUG("NO configuration file found for systematics list...");
        ANA_MSG_DEBUG("Systematics names will not be picked up...");

    }
    else{
        //I assume you set your environment up at this stage 
        auto sysfile=propt.get<std::string>("DataConfig.syst_defs");
        char*  bsllproj=std::getenv("NTANADIR");
        auto fullfilepath=std::string(bsllproj)+"/data/"+sysfile;
        std::ifstream in(fullfilepath.c_str());
        if(!in){
            ANA_MSG_FATAL("Error during base configuration!");
            ANA_MSG_FATAL("Cannot find "<<fullfilepath);
            exit(8);
        }
        ANA_MSG_DEBUG("Picking up systematics definitions");

        std::string line;
        //while (std::getline(in, line,'\n')){
        while(in>>line){
            if(line.size() > 0 && line.at(0)!='#')
                m_allowedSysts.push_back(line);
        }
        for(const auto& s : m_allowedSysts){
            ANA_MSG_VERBOSE("FOUND SYSTEMATIC ==> "<<s);
        }
    }
    if(m_allowedSysts.size()!=0){
            if(std::find(m_allowedSysts.begin(),m_allowedSysts.end(),m_sys)==m_allowedSysts.end() && m_sys!="Nominal" ){
                ANA_MSG_FATAL("Supplied systematic \""<<m_sys <<"\" is not in the list of allowed systamtics");
                return StatusCode::FAILURE;
            }
            
            if(m_sys=="Nominal"){
                 ANA_MSG_DEBUG("No systematic  is set by the user . Using the default "<<m_sys);
            }
            
            else if(this->hasObjectSystematic() ){
                ANA_MSG_DEBUG("Object systematic \""<<m_sys<<"\" is set ...");
                
            }
            
            else{
                ANA_MSG_DEBUG("Scale factor systematic \""<<m_sys<<"\" is set ...");
            }
    } 
     
    return StatusCode::SUCCESS;
} //end of ConfigureFromFile

StatusCode Skeleton::AllocateAnalysisTree(){
       if( m_datasource == "LQTree_v18_2"){
             m_data = new LQTree_v18_2(wk()->tree());
        }
        else if( m_datasource == "LQTree_v25"){
             m_data = new LQTree_v25(wk()->tree());
        }
        else if( m_datasource == "LQTree_v26"){
             m_data = new LQTree_v26(wk()->tree());
        }
        //add additional ntuple classes here
        else{
             ANA_MSG_FATAL("unsupported data source "<<m_datasource);
            return StatusCode::FAILURE;
        }
        return StatusCode::SUCCESS;
}

StatusCode Skeleton::SetupDataSourceAttributes(){
        //**** Data Type invariant settings/Common For all  ****
        m_data->setEMUNIT(m_emunit);//these two are common to all
        if ( wk()->metaData()->castBool("writeMetaData") ) {
            m_data->setDSID(wk()->metaData()->castInteger("dsid"));
        }
        //*******************************************************

        //Scale Factor systematics related settings are applied from now on
        if( ! this->hasSFSystematic() ) return StatusCode::SUCCESS;

        if(m_datasource=="LQTree_v18_2" || m_datasource=="LQTree_v25" || m_datasource=="LQTree_v26" ){
            bool isPdfScaleUncert=(m_sys.find("weight_pdf_uncert_") != std::string::npos);
            bool isSherpaZjets =(m_data-> dsid() >= 364100 && m_data-> dsid()<= 364141);
            std::string sysToset=m_sys;
            if(  isPdfScaleUncert && !isSherpaZjets){
                 ANA_MSG_INFO(" Uncertainty "<<m_sys<<" not defined for this sample. Switching back to Nominal");
                 sysToset="Nominal";
            }
             m_data->setSF_Scheme(NTAna::SF_VARIATION_SCHEME::MULTIPLIER,sysToset);

            if( m_datasource=="LQTree_v18_2" ) m_data->setSF_Scheme(NTAna::SF_VARIATION_SCHEME::MULTIPLIER,"Nominal");//there are no systematics for this version

        }
        //put some more  else if statements as you add support for various trees
        else{
            ANA_MSG_FATAL("Unsupported data type : "<<m_data);
            return StatusCode::FAILURE;
        }
        return StatusCode::SUCCESS;
}

StatusCode Skeleton::InitializeDataSource(){
         m_data->Initialize(wk()->tree());
         return StatusCode::SUCCESS;
}


StatusCode Skeleton::beginInputFile(){
    
        if(hasObjectSystematic() && hasSFSystematic()){
            ANA_MSG_FATAL("Both Object and ScaleFactor Systs are set!");
            return StatusCode::FAILURE;
        }
     
        ANA_CHECK( AllocateAnalysisTree() ); 
        ANA_CHECK( SetupDataSourceAttributes() );
        ANA_CHECK( InitializeDataSource() );
       
        return StatusCode::SUCCESS;
     
 }
 
 StatusCode Skeleton::IncrementDenominator(){
    if ( ! wk()->metaData()->castBool("writeMetaData") ) return StatusCode::SUCCESS;
    if(m_datasource == "LQTree_v18_2" || m_datasource == "LQTree_v25" || m_datasource=="LQTree_v26"){
     TH1D* h_DAOD_CF=(TH1D*)wk()->inputFile()->Get("Nominal/cutflow_DxAOD");
     if(h_DAOD_CF== nullptr){
         ANA_MSG_FATAL("Unable to retrive cutflow histogram");
         return StatusCode::FAILURE;
     }
     double N=h_DAOD_CF->GetBinContent(2);
     m_denom->SetVal(m_denom->GetVal()+N);  
    }

    else{
        ANA_MSG_FATAL("Skeleton::IncrementDenominator() : Unknown DataType : "<<m_datasource);
        return StatusCode::FAILURE;
    }

    return StatusCode::SUCCESS;
 }

 
StatusCode Skeleton::fileExecute(){
    ANA_CHECK( IncrementDenominator() );

    return StatusCode::SUCCESS;

}


StatusCode Skeleton::IterateData(){
    unsigned int iEntry=wk()->treeEntry();
    wk()->tree()->GetEntry(iEntry);
    m_data->Next( );

    return StatusCode::SUCCESS;
}


StatusCode Skeleton :: execute (){     
    ANA_CHECK(IterateData());
    ANA_CHECK(doAnalysis());

  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  return StatusCode::SUCCESS;
}



StatusCode Skeleton :: finalize (){
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  return StatusCode::SUCCESS;
}


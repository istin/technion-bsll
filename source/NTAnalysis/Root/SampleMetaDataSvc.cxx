/* author   :   Serhat Istin
<mailto:istin@cern.ch>
*/

#include <NTAnalysis/SampleMetaDataSvc.h>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <system_error>
#include <sstream>
namespace NTAna{
    
    
    SampleMetaDataSvc* SampleMetaDataSvc::m_inst=nullptr;
    SampleMetaDataSvc* SampleMetaDataSvc::init(const std::string& fname){
        if (m_inst == 0){
            m_inst = new SampleMetaDataSvc(fname);
        }

        return m_inst;
    }
    
    SampleMetaDataSvc::~SampleMetaDataSvc(){
        delete m_inst;
    }
    
    SampleMetaDataSvc::SampleMetaDataSvc(const std::string& fname){
        std::ifstream cf;
            cf.open(fname.c_str());
            if(!cf){
                std::cerr<<"Error Reading file "<<fname<<std::endl;
                exit(8);
            }
            SampleMetaData smd;
            std::string line;
            while (std::getline(cf, line)){
               if(line.at(0)=='#') continue;
               if(cf.eof()) return;
               std::istringstream iss(line);
               int did;
               if (!(iss >> did >> smd.proc>>smd.xcn>>smd.kfact>>smd.feff)) { 
                 break; 
               }
            m_smd[did]=smd;
           }
        

    }
    
    void SampleMetaDataSvc::dump(int did)const{
        std::cout<<"Sample :"<<this->proc(did)<<std::endl;
        std::cout<<"DID    :"<<did<<std::endl;
        std::cout<<"XCN    :"<<this->xcn(did)<<std::endl;
        std::cout<<"FEFF   :"<<this->filteff(did)<<std::endl;
        std::cout<<"KFACTOR:"<<this->kfactor(did)<<std::endl;
        return;
    }
}

/* author   :   Serhat Istin
<mailto:istin@cern.ch>
*/

#include <SampleHandler/SampleHandler.h>
#include <SampleHandler/Sample.h>
#include <SampleHandler/MetaObject.h>
#include <SampleHandler/ScanDir.h>
#include <SampleHandler/DiskListLocal.h>
#include <NTAnalysis/SampleMetaDataSvc.h>
#include <NTAnalysis/ConfigSvc.h>
#include <TH1F.h>
#include <TParameter.h>

#include <TFile.h>
#include <TKey.h>
#include <TH1.h>
#include <TH2.h>
#include <TTree.h>

#include <NTAnalysis/anyoption.h>
#include <boost/filesystem.hpp>

#include <vector>
#include <string>
#include <boost/range/iterator_range.hpp>
#include <regex>
#include <SampleHandler/Sample.h>
#include <SampleHandler/SampleHandler.h>


static const char* bsllproj=std::getenv("NTANADIR");
static NTAna::ConfigSvc* cs=NTAna::ConfigSvc::init(std::string(bsllproj)+"/data/master.json");
static auto mdfile=cs->sampleMetaDataFile();
static NTAna::SampleMetaDataSvc* smdh=NTAna::SampleMetaDataSvc::init(std::string(bsllproj)+"/data/"+mdfile);


std::vector<std::string> subdirs(const std::string& rootdir){
    std::vector<std::string> res;
    boost::filesystem::path p(rootdir);
    if(is_directory(p)) {
        for(auto& entry : boost::make_iterator_range(boost::filesystem::directory_iterator(p), {}))
           if(is_directory(entry)){
             res.push_back(entry.path().string());
           }
    }
    
    if(res.size()==0) return {rootdir};
    return res;
}

      //sh.print();


std::vector<std::string> scanFile(TFile* source,std::string dir="/"){//returs a list of all histograms in a root file
    std::vector<std::string> res;
    source->cd(dir.c_str());
    TDirectory *current_sourcedir = gDirectory;
    TKey *key=0;
    TIter nextkey( current_sourcedir->GetListOfKeys() );
    while ( (key = (TKey*)nextkey())) {
        TObject *obj = key->ReadObj();
        //filter out only TH1X and TH2X as we are only using them in the analysis
        if ( obj->IsA()->InheritsFrom( TH1::Class()) || obj->IsA()->InheritsFrom( TH2::Class())  ) {
            //std::cout<<"Found "<<obj->GetName()<<std::endl;
            res.push_back(obj->GetName());
        }
        
    }

    /* Critical fix  : April 30 2019
      Sometimes  rootfiles seems to have duplicates of the same histogram. 
      That caused the data-MC discrepancy with MC being 2x  higher than the value its actual value
      because of this bug or whatsoever, as we normalized and summed each histogram twice
      because we used this method (scanFile) to make a list of the histograms to be added.
    */

    //Remove dupes
    res.erase( std::unique( res.begin(), res.end() ), res.end() );
    return res;
}

void normalizeSamples(std::string sampleOutdir,std::string sampleGroupName){
      
       //For a secure run, first connect to the SampleMetaDataSvc and check for any incompatibilities
      //**** Do not allow any run on any sample that is not registered with the SampleMetaDataSvc ***
      
      //*************** Use the metadata inside the sample configured during the initial run or not.
      //if you want to use the metadata i.e xcn fefff etc.. from another source set this to false
      //the new metadata will be retrieved from the SampleMetaDataSvc
      //This feature may be needed in the situations when someone finds better cross sections NXLO etc.
      //so that you dont have to re-run the entire analysis chain to use the new xcns. It is a way too expensive task   

      // **************************
  SH::SampleHandler sh; 
  bool isdir=boost::filesystem::is_directory(sampleOutdir);
  bool issymlink=boost::filesystem::is_symlink(sampleOutdir);
  
  if(! isdir && ! issymlink){
     std::cerr<<"invalid input: "<<sampleOutdir<<std::endl;
     exit(8);
  }
  for(const auto& subdir : subdirs(sampleOutdir)){
    isdir=boost::filesystem::is_directory(subdir+"/hist");
    issymlink=boost::filesystem::is_symlink(subdir+"/hist");

    if(! isdir && ! issymlink) continue;
    //Read The location of origin ....
    std::ifstream locfile(subdir+"/location");
    if(! locfile){
      std::cout<<"unable to read location file"<<subdir+"/location"<<std::endl;
      exit(8);
    }
    sh.load(subdir+"/hist");
    

    std::string origin;
    std::getline(locfile,origin );
    std::size_t found = origin.find_last_of("/");
    sh.updateLocation (origin.substr(0,found), sampleOutdir);
  }

  if(sh.size()==0){
    std::cerr<<"Fatal! Located 0 sample(s)"<<std::endl;
    exit(8);
  }

  static std::map<std::string, std::regex> sampleGroup;
  for( const auto& smpg : cs->sampleGroups()){
     sampleGroup[smpg.name]=std::regex(smpg.pattern);;
  }
      
  auto this_regex=sampleGroup[sampleGroupName];
  auto firstsample=sh.at(0);
  auto files=firstsample->makeFileList();
  
  auto firstfilename=files.at(0);
  TFile *firstfile=TFile::Open(firstfilename.c_str(),"READ");
  auto histo_names=scanFile(firstfile);
  //get an initial histogram list with all bins set to zero
  //make it from the first file
  std::map<std::string,TH1F*> allHistos;
      
      
  for(const auto& histname: histo_names){
     TH1F *thehist=(TH1F*)firstfile->Get(histname.c_str())->Clone();
     thehist->SetDirectory(0);
        for(int i=0;i<thehist->GetNbinsX();i++){
                thehist->SetBinContent(i+1,0);
        }
        allHistos[histname]=thehist;
  }
      
  firstfile->Close();
  for(const auto& sample : sh ){//loop over samples
      auto dsid=sample->meta()->castInteger("dsid");
      //check if the metadata inside the rootfile is registered
      if(dsid==0) continue;//skip data samples. dsids should be already set to zero
      auto check=smdh->proc(dsid);
      float xcn=sample->meta()->castDouble("xcn");
      float lumifrac=sample->meta()->castDouble("lumifrac");
      float filteff=sample->meta()->castDouble("feff");
      float kfactor=sample->meta()->castDouble("kfac");
            
      auto proc=sample->meta()->castString("proc");
      std::smatch sm;
      if(! std::regex_search(proc,sm,this_regex)) continue;
      std::cout<<"Normalising "<< sampleGroupName << " dsid="<<sample->meta()->castInteger("dsid") <<std::endl;
      //just to check if the sample is registered. if not this will give an error            
      auto files=sample->makeFileList();
      auto curr_file=files.at(0);//We guarentee that it has only one file 
      TFile *thefile=TFile::Open(curr_file.c_str(),"READ");
      for(const auto& name : histo_names){
            auto DNM=(TParameter<double>*)thefile->Get("denom");
            double denom=DNM->GetVal();
            if(denom==0){
                  std::cout<<"Zero denominator "<<dsid<<std::endl;
                  exit(7);
            } 
            float normfact=(xcn*kfactor*filteff*lumifrac*cs->xcnunit()*cs->ilumiunit())/denom;
            TH1F *ahist=(TH1F*)thefile->Get(name.c_str())->Clone();
            ahist->SetDirectory(0);
            ahist->Scale(normfact);
            //std::cout<<"From "<< curr_file<<" Adding "<<ahist->GetName()<< " to "<< allHistos[name]->GetName()<<std::endl;
            allHistos[name]->Add(ahist);
      }
      thefile->Close();
  }
      
  TFile* ofile=TFile::Open((sampleGroupName+".root").c_str(),"RECREATE");
  ofile->cd();
  for (const auto& p : allHistos){
    p.second->SetDirectory(ofile);
         
  }
  ofile->Write();
}


int main(int argc,char* argv[]){
    
  AnyOption *opt = new AnyOption();
  opt->addUsage( "" );
  opt->addUsage( "Usage: " );
  opt->addUsage( "" );
  opt->addUsage( " -h  --help  	      : Prints this help " );    
  opt->addUsage( " -i  --in           : Input sample output directory(mandatory)" );
      
  opt->setOption("in",'i');
  opt->processCommandArgs( argc, argv );    
  if(opt->getValue('h')!=NULL){
      opt->printUsage();
      delete opt;
      return 0;
  }
  
  if(opt->getValue('i')==NULL){
      opt->printUsage();
      delete opt;
      return 0;      
  }
  
  std::string indir(opt->getValue('i'));
  
  for(const auto & sg : cs->sampleGroups()){
    //if a sample with samplegroupname does noe exist skip

    normalizeSamples(indir,sg.name); 
  }
    return 0;
}

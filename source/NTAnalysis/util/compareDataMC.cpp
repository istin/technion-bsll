/* author   :   Serhat Istin
<mailto:istin@cern.ch>
*/

#include "TROOT.h"
#include "TKey.h"
#include "TFile.h"
#include "TSystem.h"
#include "TClass.h"
#include "TStyle.h"
#include "TLine.h"
#include "TH1F.h"
#include "TH1.h"
#include "TLegend.h"
#include "TPaveText.h"
#include "THStack.h"
#include "TCanvas.h"
#include "Rtypes.h"
#include "NTAnalysis/anyoption.h"
#include <sstream>
#include <algorithm>
#include <cctype>
#include <cstdlib>
#include <fstream>
#include <stdlib.h>
#include <string>
#include <vector>
#include "NTAnalysis/AtlasStyle.h"
#include "NTAnalysis/tostr.h"
#include "TF1.h"
#include "TText.h"
#include "TLatex.h"
#include "NTAnalysis/PostProcess.h"

std::vector<std::string > readListFromFile(std::string filename){
  std::ifstream ifs(filename.c_str());
  std::vector<std::string> lines;
  for (std::string line; std::getline( ifs, line ); /**/ ){
    lines.push_back( line+"_PRETAG");
    lines.push_back( line+"_0b" );
    lines.push_back( line+"_1b" );
    lines.push_back( line+"_2b");
  }
return lines;
}

bool checkName(std::string name,std::vector<std::string> list){
  if(std::find(list.begin(), list.end(), name)!=list.end()) return true;
  return false;
}



std::vector<std::string> TH1List(TDirectory *source){
   std::vector<std::string> res;
   //loop on all TH1 entries of this directory
   TKey *key;
   TIter nextkey(source->GetListOfKeys());
   while ((key = (TKey*)nextkey())) {
      const char *classname = key->GetClassName();
      TClass *cl = gROOT->GetClass(classname);
      if (!cl) continue;
      //std::cout<<key->GetName()<<std::endl;
      if (cl->InheritsFrom(TH1F::Class())) {
       res.push_back(std::string(key->GetName()));
      }
  }
 return res;
}

bool isSharpHist(TH1* hist){
  float xfactor=5.0;
 if( hist->GetMaximum()*hist->GetNbinsX() > (hist->Integral()-hist->GetMaximum())*xfactor) return true;
  return false;
}


/*
bool contains(std::string name,std::string subname){
    if(name.find(subname)!=std::string::npos) return true;
    return false;
}
*/
bool blindIt(std::string histname){

	if (
        (contains(histname,"Reco") && !contains(histname,"low")&& ( contains(histname,"PRETAG") || contains(histname,"0b"))  )||
        (contains(histname,"PTZCUT") &&  ( contains(histname,"0b")|| contains(histname,"PRETAG")) )||
        (contains(histname,"HTCUT") &&  ( contains(histname,"0b") || contains(histname,"PRETAG")) ||
        (contains(histname,"h_eff")) )
    ) return true;
	return false;
}


int main(int argc, char* argv[]){
  //std::string histopath="nominal/";
  std::string histopath="";
  AnyOption *opt = new AnyOption();
  opt->addUsage( "" );
  opt->addUsage( "Usage: " );
  opt->addUsage( "" );
  opt->addUsage( " -h  --help  	      : Prints this help " );
  opt->addUsage( " -m  --mcFiles      : list of MonteCarlo Sample files. If you supply multiple files separate them with whitespace and quote with \"" );
  opt->addUsage( " -d  --dataFile     :	data File (mandatory) ");
  opt->addUsage( " -o  --output       : output ROOT file where plots will be written (Mandatory)" );
  opt->addUsage( " -s  --signal       :	Signal File. If Given it is superimposed on the stack plot (optional)" );
  opt->addUsage( " -l  --log          :	draw all plots is logscale (optional - default is smartlog)" );
  opt->addUsage( " -u  --unblind      : unblind (optional) Unblind final results");
  opt->addUsage( " -L  --linear		  : draw all plots in linear scale (optional - default is smartlog)");
  opt->addUsage( " -O  --outDir       : output directory where plots will be written in the format specified(optional)" );
  opt->addUsage( " -p  --postprocess  : postprocecss histograms ... Rebin etc...");
  opt->addUsage( " -X  --ext          : image format (eps .. jpeg etc) default is png (optional)" );
  opt->addUsage( " -H  --histograms   : consider only histograms in the list. (OPTIONAL: Default is all)");
  opt->addUsage( " -c  --contains     : consider only histograms contaning the substing. (OPTIONAL: Default is all)");
  opt->addUsage( " -b  --Lambda     : Lambda value for signal. (Default is 1)");
  opt->addUsage( "" );
  /////////////////////////////
  opt->setFlag("help", 'h' );
  opt->setOption("mcFiles",'-m');
  opt->setOption("dataFile",'-d');
  opt->setOption("output",'-o');
  opt->setOption("outDir",'-O');
  opt->setFlag("postprocecss",'-p');
  opt->setOption("ext",'-X');
  opt->setOption("signal",'-s');
  opt->setFlag("log",'-l');
  opt->setFlag("linear",'-L');
  opt->setFlag("unblind",'-u' );
  opt->setOption("histograms",'-H');
  opt->setOption("contains",'-c');
  opt->setOption("Lambda",'-b');
  //Process arguments
  opt->processCommandArgs( argc, argv );
  //Check if user supplied mandatory arguments
   if( opt->getValue( 'd' )==NULL){
   std::cerr<<"CommandLine error. No data File supplied by user!"<<std::endl;
   opt->printUsage();
   exit(8);
  }
  if( opt->getValue( 'm' )==NULL){
   std::cerr<<"CommandLine error. Give a list of MC samples"<<std::endl;
   opt->printUsage();
   exit(8);
  }
  if( opt->getValue( 'o' )==NULL){
   std::cerr<<"CommandLine error. No output file supplied by user"<<std::endl;
   opt->printUsage();
   exit(8);
  }
  std::string outdir="";
  std::string defaultExt="png";
  if(opt->getValue('O')!=NULL){
  if(opt->getValue('X')!=NULL) defaultExt=std::string(opt->getValue('X'));
  std::cout<<opt->getValue('O')<<std::endl;
  outdir=std::string(opt->getValue('O'));
     std::string remove="rm -rf "+outdir;
     system(remove.c_str());
     std::string mkdir="mkdir "+outdir;
     system(mkdir.c_str());
  }

  //Open data file
  std::ifstream data_file (opt->getValue('d') );
  if(!data_file){
    std::cout<<"File "<<opt->getValue('d')<<" does not exist"<<std::endl;
    exit(8);
  }

  TFile *dataFile=new TFile(TString(opt->getValue('d')),"READ");
  //Turn mc file list in a string stream then tokenize them to mkae a std::vector<strings> out of it
  std::stringstream ssin(opt->getValue('m'));
  std::vector<std::string> MCFiles;
  while (ssin.good()){
    std::string currfile;
    //ignore whitespace
    ssin >> currfile;
   if(currfile.empty()) continue;//protection against whitespcae in commline
    MCFiles.push_back(currfile);
   }
  std::cout<<"# of mc files to process :"<<MCFiles.size()<<std::endl;


  for(unsigned int i=0;i<MCFiles.size();i++){
    std::cout<<MCFiles[i]<<std::endl;
  }
  //Setup color list
  std::vector<int> kolorlist;
  kolorlist.push_back(kRed+1);
  kolorlist.push_back(kOrange+1);//
  kolorlist.push_back(kYellow);
  kolorlist.push_back(kGreen+1);//
  kolorlist.push_back(kBlue-1);
  kolorlist.push_back(kCyan+1);
  kolorlist.push_back(kMagenta-2);//
  kolorlist.push_back(kGray);

  //Now Biuld Root stuff below
  std::vector<std::string> histonames;
  if(opt->getValue('H')!=NULL) histonames=readListFromFile(std::string(opt->getValue('H')));
  else {
    histonames=TH1List(dataFile);
  }
  TFile* ofile=new TFile(opt->getValue('o'),"RECREATE");

  bool drawsignal=false;
  TFile* signalfile=NULL;
  std::string signame="";
  if(opt->getValue('s')!=NULL) drawsignal=true;
  if(drawsignal){
    std::cout<<"Signal Draw set to true"<<std::endl;

    signalfile=new TFile(opt->getValue('s'),"READ");
    std::string sgfile=std::string(opt->getValue('s'));
    unsigned int end_postion = sgfile.find(".root");
    signame=sgfile.substr(0,end_postion);
  }
  ofile->cd();
  SetAtlasStyle();
  bool postproc=false;
  if(opt->getFlag('p')!=NULL){
    postproc=true;
  }
  for(unsigned int i=0;i<histonames.size();i++){//loop over histogram names
    if ( opt->getValue('c')!=NULL && !contains(histonames[i],std::string(opt->getValue('c'))) ) continue;
    //std::cout<<"processing histogram: "<<histonames[i]<<std::endl;
    bool logy=false;
    bool islinear=false;
    if (opt->getFlag('L')) islinear=true;
    if(opt->getFlag('l')) logy=true;
    TH1::SetDefaultSumw2(kTRUE);
    TCanvas* canvas=new TCanvas(histonames[i].c_str(),"data-mc comparison",1600,1400);
    //TCanvas* canvas=new TCanvas(histonames[i].c_str());

    THStack* stak=new THStack();
    canvas->Clear();
    float div = 0.2; // portion of canvas to use for ratio plot
    float margin_up=gStyle->GetPadTopMargin();
    float margin_down=gStyle->GetPadBottomMargin();
    float useable_height=1-(margin_up+margin_down);

    TPad *mainPad = new TPad("mainPad","mainPad", 0, 0, 1., 1.);
    mainPad->SetTopMargin(margin_up);
    mainPad->SetBottomMargin(margin_down+div*useable_height);
    mainPad->SetFillStyle(4000);
    mainPad->Draw();
    TPad *pad2 = new TPad("pad2","pad2", 0., 0., 1., 1.);
    //TPad *pad2 = new TPad();
    pad2->SetTopMargin(margin_up+(1-div)*useable_height);
    pad2->SetFillStyle(4000);
    pad2->SetFillColor(kWhite);
    pad2->Draw();
    mainPad->cd();
    TH1F* datahist;//Data histogram;
    TH1F* signalhist;
    dataFile->cd();
    dataFile->GetObject(histonames[i].c_str(),datahist);
    if(datahist==NULL){
      std::cout<<" Got null datahistogram: "<<histonames[i]<<std::endl;
      exit(9);
    }
    if (postproc) datahist=PostProcess(datahist);
    if(drawsignal){
      signalfile->cd();
      signalfile->GetObject((histonames[i]).c_str(),signalhist);
      if(signalhist==NULL){
	std::cout<<" Got null signal histogram: "<<histonames[i]<<std::endl;
	exit(8);
      }

      if(opt->getValue('b')!=NULL) signalhist->Scale(1/pow(stof(opt->getValue('b')),4)) ;

      if (postproc) signalhist=PostProcess(signalhist);
      if(signalhist==NULL){
	std::cout<<"Error retrieving signal histogram: "<<histonames[i]<<std::endl;
	continue;
      }
    }

    float wbin=datahist->GetBinWidth(0);
    if(datahist==NULL){
        std::cout<<"NULL histogram "<<histonames[i]<<" skipping"<<std::endl;
        continue;
    }
/*
    if(isSharpHist(datahist)||isSharpHist(bgsum)){//do not log final selection histgrams
      mainPad->SetLogy(1);
      logy=true;
    }
*/

    mainPad->Update();

    ofile->cd();
    //Double_t xl1=0.7, yl1=0.65, xl2=xl1+0.2, yl2=yl1+0.25;
    Double_t xl1=0.72, yl1=0.68, xl2=xl1+0.2, yl2=yl1+0.25;
    TLegend* legend=new TLegend(xl1,yl1,xl2,yl2);
    //TLegend* legend=new TLegend(0.66,0.5,0.95,0.95);
    legend->SetFillStyle(0);
    legend->SetTextSize(0.03);
    legend->SetBorderSize(2);
    if(opt->getFlag('u')) legend->AddEntry(datahist,"data","P");
    //datahist->SetStats(kFALSE);
    datahist->SetMarkerStyle(8);
    datahist->SetMarkerSize(1.0);
    datahist->SetMarkerColor(kBlack);
    datahist->SetStats(0);
    datahist->Sumw2();
    if(drawsignal){
      signalhist->SetStats(0);
      signalhist->SetMarkerSize(0);
      signalhist->SetLineWidth(4);
      signalhist->SetLineColor(kRed);
      signalhist->SetLineStyle(2);
    }
    //datahist->SetBit(TH1::kNoTitle);//remove titles
    TH1F* bgsum;
    std::string ytitle;
    for(unsigned int j=0;j<MCFiles.size();j++){//loop over background histograms in the current file
      TFile *bgfile=TFile::Open(MCFiles[j].c_str(),"READ");
      if(bgfile==NULL){
        std::cerr<<"Can't open file: "<<MCFiles[j]<<std::endl;
        exit(8);
      }
      bgfile->cd();
      TH1F* bghist;
      bghist=(TH1F*)bgfile->Get((histopath+histonames[i]).c_str());
      if (postproc) bghist=PostProcess(bghist);
      if(bghist==NULL){
        std::cerr<<" got null BG histogram: "<<histonames[i]<<" from file :"<<MCFiles[j]<<std::endl;
        exit(8);
      }
      float wbin=bghist->GetBinWidth(0);
       ytitle="Entries/"+tostr(wbin);
      bghist->SetDirectory(0);
      bgfile->Close();
      if(bghist==NULL){
          std::cout<<"NULL histogram "<<histonames[i]<<" skipping"<<std::endl;
          continue;
      }
      if(j==0){
        bgsum=(TH1F*)bghist->Clone();
      }
      else{
        bgsum->Add(bghist);
      }
      bghist->SetLineWidth(3);
      bghist->SetStats(0);
      bghist->SetFillColor(kolorlist[j]);
      bghist->SetLineColor(kolorlist[j]);
      bghist->SetMarkerSize(0);
      bghist->SetFillStyle(1001);
      bghist->SetBit(TH1::kNoTitle);//remove title

      std::string str=MCFiles[j];
      unsigned int end_postion = str.find(".root");
      
      if(str == "singletop.root") {legend->AddEntry(bghist,"Wt","F" ); }
      else if(str == "ttbarV.root") {legend->AddEntry(bghist,"t#bar{t}+V","F" ); }
      else if(str == "ttbar.root") {legend->AddEntry(bghist,"t#bar{t}","F" ); }
      else if(str == "dibosons.root") {legend->AddEntry(bghist,"VV","F" ); }
      else if(str == "Sherpa_Z+jets.root") {legend->AddEntry(bghist,"Z/#gamma^{*}+jets","F" ); }
      else if(str == "Powheg_Z+jets.root") {legend->AddEntry(bghist,"Z/#gamma^{*}+jets","F" ); }
      else if(str == "W+jets.root") {legend->AddEntry(bghist,"W+jets","F" ); }


      ofile->cd();
      stak->Add((TH1F*)bghist);
    }
    float rangeY=0;
    if(datahist->GetMaximum()>bgsum->GetMaximum()){
      rangeY=datahist->GetMaximum();
    }
    else{
      rangeY=bgsum->GetMaximum();
    }

    if(( isSharpHist(datahist) || isSharpHist(bgsum) ) && (!islinear)){
      logy=true;
    }

    bool blindData=true;
    if(opt->getFlag('u')) blindData=false ;

    if(blindIt(histonames[i])){
      blindData=true;//set t to TRUE for auto blind
      logy=false;
    }
    if (logy) mainPad->SetLogy(1);

    double axis_up = (logy) ? 1E4 : 1.75;
    rangeY*=axis_up;//Better visibility
    stak->SetMaximum(rangeY);
    stak->SetMinimum(1E-1);
    stak->Draw("HIST");
    stak->GetXaxis()->SetLabelColor(kWhite);
    stak->GetYaxis()->SetLabelSize(0.04);
    stak->GetYaxis()->SetTitle(ytitle.c_str());
    bgsum->SetFillColor(kBlack);
    auto xtitle=datahist->GetTitle();
    datahist->SetBit(TH1::kNoTitle);
    bgsum->SetFillStyle(3004);//3018
    bgsum->SetMarkerSize(0);
    bgsum->Draw("e2same");


    if(!blindData||opt->getFlag('u')) datahist->Draw("EPSAME");
    int signal_scale=1;
    std::string signalLegendString="signal, #Lambda = 2 TeV";
    if(!logy&&!blindData&&drawsignal){
    	signal_scale=(int)(stak->GetMaximum()/signalhist->GetMaximum()*0.8);
    	signal_scale-=signal_scale%10;
    	if(signal_scale==0) signal_scale++;
        signalhist->Scale(signal_scale);
        signalLegendString+="X"+tostr(signal_scale);
    }
    if(drawsignal){
        legend->AddEntry(signalhist,signalLegendString.c_str());
        signalhist->Draw("HSAME");
    }
    legend->Draw("SAME");

	double yATLAS = 0.75;  //pt jet 1
	TLatex l; //l.SetTextAlign(12);
	l.SetTextSize(0.035);
	l.SetNDC();
	l.SetTextFont(72);
//  l.DrawLatex(0.515,0.4,"ATLAS Preliminary");
//  l.DrawLatex(0.518,yATLAS,"ATLAS Preliminary");
	l.DrawLatex(0.21, yATLAS + 0.12, "ATLAS Preliminary");
//l.DrawLatex(0.15, yATLAS - 0.08, "Internal");

	TLatex n1;
	n1.SetNDC();
	n1.SetTextFont(72);
	n1.SetTextSize(0.04);
	n1.SetTextColor(kBlack);
	n1.DrawLatex(0.20, yATLAS + 0.06,
			"L=140 fb^{-1},#sqrt{s}=13 TeV");

    //datahist->Draw("SAME");
    pad2->cd();
    TH1F* ratio=(TH1F*)datahist->Clone();
    ratio->Sumw2();
    ratio->Divide(bgsum);
    ratio->SetYTitle("Data/MC");
    ratio->SetFillColor(kGray+3);
    ratio->SetMarkerSize(1.0);
    ratio->SetFillStyle(3018);
    ratio->SetMaximum(1.7);
    ratio->SetMinimum(0.4);
    ratio->GetYaxis()->SetNdivisions(5);
    ratio->GetYaxis()->SetLabelSize(0.04);
    ratio->GetXaxis()->SetLabelSize(0.04);
    //ratio->GetXaxis()->SetTitle(datahist->GetXaxis()->GetTitle());
    ratio->SetXTitle(xtitle);
    ratio->GetXaxis()->CenterTitle();
    float xmax=ratio->GetXaxis()->GetXmax();
    float xmin=ratio->GetXaxis()->GetXmin();
    TLine *normline=new TLine (xmin,1,xmax,1);
    normline->SetLineColor(kRed);
    normline->SetLineWidth(2);

    if(blindData&&!opt->getFlag('u')){
      ratio->SetMarkerColor(kWhite);
      ratio->SetFillColor(kWhite);
      ratio->SetLineColor(kWhite);
      TLatex* lat=new TLatex(1,0.8,"DATA IS BLINDED AT THIS SELECTION LEVEL");
      ratio->DrawCopy("ep");
      lat->SetTextSize(0.03);
      lat->Draw("SAME");
    }
    else{
      ratio->DrawCopy("ep");
      normline->Draw();
    }
    mainPad->cd();
    canvas->Write();
    if(opt->getValue('O')!=NULL){
      std::cout<<"writing canvas: "<<outdir+"/"+histonames[i]+defaultExt<<std::endl;
      std::string savePath=outdir+"/"+histonames[i]+"."+defaultExt;
      canvas->SaveAs(savePath.c_str());
    }
  }

  /*
    for(unsigned int i=0;i<histonames.size();i++){
    TCanvas* cvs=compare(std::string(opt->getValue('d')), MCFiles,histonames[i], true);
    std::cout<<cvs->GetName()<<std::endl;
  }
  */


  return 0;
}

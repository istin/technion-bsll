/* author   :   Serhat Istin
<mailto:istin@cern.ch>
*/


#include <NTAnalysis/BSLLAlg.h>
#include <NTAnalysis/GenericAlg.h>
#include <NTAnalysis/SampleMetaDataSvc.h>
#include <NTAnalysis/ConfigSvc.h>
#include <xAODRootAccess/Init.h>
#include <NTAnalysis/anyoption.h>


#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/filesystem.hpp>
#include <boost/asio/thread_pool.hpp>
#include <boost/asio.hpp>
#include <string>
#include <regex>
#include <thread>
#include "AsgTools/MessageCheck.h"

#include <AnaAlgorithm/AnaAlgorithmConfig.h>
#include <SampleHandler/SampleHandler.h>
#include <SampleHandler/Sample.h>
#include <SampleHandler/ScanDir.h>
#include <EventLoop/Job.h>
#include <EventLoop/DirectDriver.h>
#include <EventLoop/LocalDriver.h>
#include <EventLoop/LSFDriver.h>

#include <TSystem.h> 
#include <cstdlib>
#include <algorithm>


std::string treename="NONE";


bool isDir(const std::string & dirname){

    bool isdir=boost::filesystem::is_directory(dirname);
    bool issymlink=boost::filesystem::is_symlink(dirname);
    
    if (isdir || issymlink) return true;
    return false;
}




NTAna::ConfigSvc* cs;
NTAna::SampleMetaDataSvc* smdh;
const char* ntAnaProj; 


void InitRun (const std::string& indir){
  
    ntAnaProj=std::getenv("NTANADIR");
    if( ! ntAnaProj ){
        std::cerr<<"NTANADIR not setup"<<std::endl;
        exit(8);
    }
    cs=NTAna::ConfigSvc::init(std::string(ntAnaProj)+"/data/master.json");
    //cs->dump();
    auto mdfile=cs->sampleMetaDataFile();
    smdh=NTAna::SampleMetaDataSvc::init(std::string(ntAnaProj)+"/data/"+mdfile);
    if(!isDir(indir)){
        std::cerr<<"inpur dir "<<indir<<" does not exist"<<std::endl;
        return;
    }

  StatusCode::enableFailure();
}


SH::SampleHandler ConfigureSamples(const std::string& indir,bool useMeta,std::string systematic,bool dataonly=false){

  SH::SampleHandler sh;
  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
  //Set Sample Cross sections automatically from sample names
  //A regular expression for DSID is searched for inside the sample names.
  //if the regex pattern is not matched  the sample is assigned to be the data instead of being a monte carlo /or vice versa
  SH::ScanDir().filePattern("*.root").scan(sh,indir);
  std::string rootfolder="Nominal";//default 
  
  if(systematic.find("weight_") == std::string::npos){//this is a bit fragile to lqtrees but who cares. This file is not supposed to be so generic. And I dont want to waste time on making it generic.I domeone wants to implement the coorect way, stuff like "weight_ , BaseSelection_lq_tree_syst_Final" can be configured throught the master.json
      rootfolder=systematic;
  }
  // set the name of the tree in our files
    using boost::property_tree::ptree;
    ptree propt;
    read_ini(std::string(ntAnaProj)+"/data/Algorithm.ini", propt);
    auto datasource=propt.get_child_optional( "DataConfig.data_source" );
    if( ! datasource ){
        std::cout<<"ubanle to read ini\n";
        exit(8);
    }

    treename=propt.get<std::string>("DataConfig.treename");
    sh.setMetaString ("nc_tree", rootfolder+"/"+treename);
    if(! useMeta ) return sh;
  

  //Meta Data Assignments start here below
  std::regex rx_dsid(cs->dsidpattern());
  std::regex rx_data(cs->datapattern());
  std::smatch sm;
  std::string auto_did="data";
  // Hehe (:
  std::vector<std::string> mcroundnames;
  for(const auto & k : cs->lumifracs()){
        mcroundnames.push_back(k.first);
  }
 
   /*
  r9364 - MC16a - 2015+16 data; 
  r10201-MC16d-2017 data; 
  r10724 - MC16e -2018 data;
  mc16a: 36.1
  mc16d: 43.6
  mc16e: 47.8
  */
  
 
  std::string mcroundspattern="(";
  for(unsigned int i=0;i<mcroundnames.size()-1;i++){
        mcroundspattern+=mcroundnames.at(i)+"|";
  }
  mcroundspattern+=mcroundnames.at(mcroundnames.size()-1)+")";
  std::regex rx_rounds(mcroundspattern);
  std::regex rx_blaklist(cs->blacklistedSamples())  ;
  
  auto mcrounds=cs->lumifracs();
  
  std::string matched_mcround="none";
  
  for(auto samp : sh){

        if(std::regex_search(samp->name(),sm,rx_dsid)) {//if MC
            samp->meta()->setBool("writeMetaData",cs->writeSampleMetaData());
            for(unsigned int i=0;i<sm.size();++i){
                auto_did=sm[i];
               
            }
            if(std::regex_search(samp->name(),sm,rx_rounds)){
                matched_mcround=sm[0];
                matched_mcround.erase(std::remove(matched_mcround.begin(), matched_mcround.end(), '_'),matched_mcround.end());
                samp->meta()->setDouble("lumifrac",mcrounds[matched_mcround]);
                samp->meta()->setString("round",matched_mcround);
            }             
            auto_did.erase(std::remove(auto_did.begin(), auto_did.end(), '.'), auto_did.end());
            //std::cout<<"Found 6 digit DSID: " << auto_did <<std::endl;
            //still c type strings...
            int i_did=atoi(auto_did.c_str());
            auto process=smdh->proc(i_did);
            //std::cout<<"*********************************************"<<std::endl;
            //std::cout<<"Assigning Sample Metadata: "<<std::endl;
            //smdh->dump(i_did);
            //std::cout<<"*********************************************"<<std::endl;
            samp->meta()->setBool("isMC",true);
            samp->meta()->setDouble("xcn",smdh->xcn(i_did));
            samp->meta()->setDouble("kfac",smdh->kfactor(i_did)); 
            samp->meta()->setDouble("feff",smdh->filteff(i_did));
            samp->meta()->setString("proc",process);
            samp->meta()->setInteger("dsid",i_did);
            samp->meta()->setString("didstr",auto_did);
            

        }//if MC
        
        else if(std::regex_search(samp->name(),sm,rx_data)){//if data
            
            //auto did should be period[A-Z] in this case...
            //std::cout<<"This  seems to be a data sample "<<samp->name()<< " no metadata is assigned"<<std::endl;
            for(unsigned int i=0;i<sm.size();++i){
                auto_did=sm[i];
            }
            
            auto_did.erase(std::remove(auto_did.begin(), auto_did.end(), '.'), auto_did.end());
            //std::cout<<"Found 6 digit DSID: "<<auto_did<<std::endl;
            samp->meta()->setString("didstr",auto_did);
            samp->meta()->setBool("isMC",false);
        }//if data
        
        
        else{
            std::cerr<<"Unable to resolve if this is a data or MC sample. Follow naming conventions or run without --usemeta !"<<std::endl;
            std::cerr<<"Sample "<<samp->name()<<" seems to be problematic"<<std::endl;
            exit(9);
        }
        
  }
  ///////////////// end of metadata assignment ////////////////////////////  
  SH::SampleHandler shfiltered;
  for(const auto& s : sh){
      auto proc=s->meta()->castString("proc");
      if (std::regex_search(proc,sm,std::regex(rx_blaklist))){
          //std::cout<<"Sample "<<proc<<" is blacklisted"<<std::endl;
          continue;
      }

    // if dataonly is set then filter out MC
     if( dataonly && s->meta()->castBool("isMC") ) continue;

    //if dataonly is not set then filter out data. By default it is false and the entire run selects MC only!
     if( !dataonly && ! s->meta()->castBool("isMC") ) continue;


    shfiltered.add(s);
  }
  //shfiltered.print ();
  return shfiltered;
}

//to be posted to the threadpool
auto SubmitSample=[=]( const std::string& algorithm,
                       SH::Sample* sample,
                       const std::string& submitTag,
                       const std::string& chan,
                       const std::string& systematic,
                       bool useMeta,
                       int level) {
    static std::mutex mu;
    static int datactr=0; 

     //Channel check might be left to the analysis algorithm through metadata
    static std::vector<std::string> allowedChannels={"EE","MM"};
    if(std::find(allowedChannels.begin(),allowedChannels.end(),chan)==allowedChannels.end()){
        std::cerr<<"Wrong channel set"<<std::endl;
        exit(8);
    }
    
    SH::SampleHandler shsub;
     

    sample->meta()->setString("channel",chan);//should be set for both MC and data! and set from the CLI       

    shsub.add(sample);
    EL::Job job;
    job.sampleHandler (shsub); // use SampleHandler in this job
    job.options()->setDouble (EL::Job::optMaxEvents, -1);
    EL::AnaAlgorithmConfig alg;
    alg.setUseXAODs(false);

    static auto AllowedAlgorithms={"GenericAlg","BSLLAlg"};
    if(std::find(AllowedAlgorithms.begin(),AllowedAlgorithms.end(),algorithm)==AllowedAlgorithms.end()){
        std::cerr<<"Unsupported Algorithm : "<<algorithm<<std::endl;
        exit(8);
    }
    alg.setType (algorithm);

    alg.setProperty("OutputLevel",level).ignore();
    alg.setProperty("systematic",systematic).ignore();
    alg.setProperty( "configfile",std::string(ntAnaProj)+"/data/Algorithm.ini" ).ignore();
    alg.setName (algorithm);

    job.algsAdd (alg);
    //After discussing with David :
    //Array job submission is not posssible at Technion cluster.
    //Torque driver works but it creates issues at the task scheduler, dont use it
    EL::DirectDriver driver;
    std::string submitdir=submitTag;
    
    if(useMeta){
        submitdir+="_"+sample->meta()->castString("didstr");
        if(sample->meta()->castString("didstr")==""){
            std::cerr<<"Empty did for "<<sample->name()<<std::endl;
            exit(8);
        }

        submitdir+="_"+chan;
        if(sample->meta()->castBool("isMC")){
            submitdir+="_"+systematic;
            submitdir+="_"+sample->meta()->castString("round");
        }
        else{//data
            //append an integre to data ssampleout folder
            std::lock_guard<std::mutex> lock(mu);
            submitdir+="_"+std::to_string(datactr);
            datactr++;
        }
    }//usemeta
    else{
        submitdir=sample->name();
    }
    if(isDir(submitdir)){
        std::cerr<<"out dir "<<submitdir<<" already exist! try different one or remove/backup this"<<std::endl;
        exit(7);
    }

    driver.submit (job, submitdir);
     
};


//limit the number of maximum threads  to that of the machine's specs
//leave at least 1 core free
void Launch( const std::string& algtype,
            const SH::SampleHandler& sh,
            const std::string& submitTag,
            const std::string& chan,
            const std::string& systematic,
            unsigned int nthreads,
            bool useMeta,
            int msglvl){
    
    
    
    unsigned hardware_threads = std::thread::hardware_concurrency();
    
    if(nthreads>= hardware_threads){
        nthreads=hardware_threads-2;
    }
    if(nthreads <=0) nthreads=1;
    
    boost::asio::thread_pool pool(nthreads);
    for( auto& sample : sh){
        boost::asio::post(pool,std::bind(SubmitSample,algtype,sample,submitTag,chan,systematic,useMeta,msglvl));

    }
    pool.join();

}


int main(int argc,char* argv[]){
               
    gErrorIgnoreLevel =kFatal; //kPrint, kInfo, kWarning, kError, kBreak, kSysError, kFatal;

    AnyOption *opt = new AnyOption();
    opt->addUsage( "" );
    opt->addUsage( "Usage: " );
    opt->addUsage( "" );
    opt->addUsage( " -h  --help  	    : Prints this help " );
    opt->addUsage( " -A  --algorithm    : Select an algorithm to run (mandatory)");
    opt->addUsage( " -D  --dataOnly     : Run on Data only (optional / default is Monte Carlo)");
    opt->addUsage( " -I  --in           : Input sample directory(mandatory)" );
    opt->addUsage( " -R  --runtag       : Tag to append for this run(mandatory)" );
    opt->addUsage( " -C  --channel      : Analysis channel (EE or MM) (mandatory)" );

    opt->addUsage( " -m  --useMeta      : Use MetaData for samples (optional / default is false)");
    opt->addUsage( " -t  --threads      : Number of threads (optional / default is 1)" );
    opt->addUsage( " -e  --systError    : Run on given single systematics.(optional / default is Nominal)");
    opt->addUsage( " -l  --msgLvl       : Message Level(optional / default is INFO )" );

    opt->setFlag("help", 'h' );
    opt->setOption("algorithm",'A');
    opt->setOption("in",'I');
    opt->setOption("runtag",'R');
    opt->setOption("channel",'C');

    opt->setFlag("useMeta",'m');
    opt->setFlag("dataOnly",'D');
    opt->setOption("threads",'t');
    opt->setOption("systError",'e');
    opt->setOption("msgLvl",'l');
    
    
    
    opt->processCommandArgs( argc, argv );
    if( ! opt->hasOptions()) { /* print usage if no options */
        opt->printUsage();
        delete opt;
        return 0;
    } 
    if(opt->getValue('h')!=NULL){
        opt->printUsage();
        delete opt;
        return 0;
    }

    if(opt->getValue('I')==NULL || opt->getValue('I')==NULL || opt->getValue('C')==NULL || opt->getValue('A')==NULL  ){
        opt->printUsage();
        return 0;
    }
    bool useMeta=false;
    if( opt->getValue('m')!=NULL){
        useMeta=true;
    }

    int msglvl=MSG::INFO;//default

    if(opt->getValue('l')!=NULL){
        auto lvl=std::string(opt->getValue('l'));
        if(lvl=="INFO") msglvl=MSG::INFO;
        if(lvl=="DEBUG") msglvl=MSG::DEBUG;
        if(lvl=="VERBOSE") msglvl=MSG::VERBOSE;
        if(lvl=="WARNING") msglvl=MSG::WARNING;
        if(lvl=="ERROR") msglvl=MSG::ERROR;
        if(lvl=="FATAL") msglvl=MSG::FATAL;
    }
    
    std::string ind(opt->getValue('I'));

    if(opt->getValue('R')==nullptr ){
        opt->printUsage();
        return 0;
    }
    std::string tag(opt->getValue('R'));
    std::string chan(opt->getValue('C'));
    std::string systematic="Nominal";//default
    
    if(opt->getValue('e')!=NULL){
        systematic=std::string(opt->getValue('e'));
    }
    
    unsigned int nthreads=1;
    if(opt->getValue('t')!=NULL){
        nthreads=atoi(opt->getValue('t'));
    }
    ROOT::EnableThreadSafety();
    InitRun(ind);

     auto clalg=std::string(opt->getValue('A'));

     bool isData=false;
     if(opt->getValue('D')!=nullptr ){
         isData=true;
         //useMeta=true; 
     }


    auto selected_samples = ConfigureSamples(ind,useMeta,systematic,isData);

    for(const auto& samp : selected_samples){
            std::cout<<" Will submit sample : "<<samp->name()<<std::endl;


    }
    
    Launch(clalg,selected_samples,tag,chan,systematic,nthreads,useMeta,msglvl);
    

    return 0;
}

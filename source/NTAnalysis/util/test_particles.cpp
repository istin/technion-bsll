/* author   :   Serhat Istin
<mailto:istin@cern.ch>
*/

#include "NTAnalysis/Electron.h"
#include "NTAnalysis/Muon.h"
#include "NTAnalysis/Jet.h"
#include <iostream>
#include <NTAnalysis/SampleMetaDataSvc.h>
#include <NTAnalysis/ConfigSvc.h>
#include <cstdlib>
//utilize a small test program to demonstrate particle building and properties
//check also potential memory leaks (seems ok so far)
//If you are not sure how to free resources upon a "new" allocation dont use it!

using namespace NTAna;

int main(/*int argc, char* argv[]*/){
    
    const char* bsllproj=std::getenv("NTANADIR");
    if( ! bsllproj ){
        std::cerr<<"NTANADIR not setup"<<std::endl;
        exit(8);
    }
    
    
    ConfigSvc* cs=ConfigSvc::init(std::string(bsllproj)+"/data/master.json");
    cs->dump();
    auto mdfile=cs->sampleMetaDataFile();
    SampleMetaDataSvc* xh=SampleMetaDataSvc::init(std::string(bsllproj)+"/data/"+mdfile);


    Electron el1(new TLorentzVector(0,0,0,0),-1);//never attempt to delete such Lorentz vectors manually!
    Electron el2(new TLorentzVector(0,0,0,0),1);
    
    
    

    el2=el1;
    
    for(unsigned int i=0;i<100000000;i++){
        Electron *ele1=new Electron(new TLorentzVector(),-3);
        Electron *ele2=new Electron(new TLorentzVector(),4);
        
        Electron e1(0,0,0,0,1);
        Electron e2(1,2,3,4,5);
        Particle p=e2+e1;
        p=e2;
        Particle p2(p);
        Electron electron1;
        Electron electron2;
        electron2=electron1;

        Muon * m1=new Muon(0,0,0,0,-4);
        delete m1;
        delete ele2;
        delete ele1;
        
    }
    
    
    return 0;
}

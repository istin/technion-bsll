# The name of the package:
atlas_subdir (NTAnalysis)
set(CMAKE_CXX_FLAGS "-std=c++14")
find_package( Boost  )
find_package( ROOT COMPONENTS Core Tree MathCore  Hist Gpad Graf Physics RIO  )


atlas_depends_on_subdirs(
   PUBLIC
   ${extra_deps}
   )



atlas_add_library (NTAnaLib
  NTAnalysis/*.h Root/*.cxx
  PUBLIC_HEADERS NTAnalysis
  LINK_LIBRARIES AnaAlgorithmLib EventLoop  ${ROOT_LIBRARIES} ${CLHEP_LIBRARIES} ${EIGEN_LIBRARIES} ${BOOST_LIBRARIES} ${TBB_LIBRARIES} ${HEPMC_LIBRARIES} ${extra_libs} )
 
 

 
if (XAOD_STANDALONE)
 # Add the dictionary (for AnalysisBase only): 
    set( extra_deps Control/xAODRootAccess EventLoop AnaAlgorithm Root 
      PRIVATE )
    set( extra_libs xAODRootAccess EventLoop   AnaAlgorithm Root   ) 
    atlas_add_dictionary (NTAnaDict
    NTAnalysis/NTAnaDict.h
    NTAnalysis/selection.xml
    LINK_LIBRARIES  NTAnaLib )
  
endif ()

if (NOT XAOD_STANDALONE)
  # Add a component library for AthAnalysis only:
  atlas_add_component (NTAna
    src/components/*.cxx
    LINK_LIBRARIES  NTAnaLib )
endif ()




file (GLOB util_sources RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}/util" "${CMAKE_CURRENT_SOURCE_DIR}/util/[a-zA-Z0-9]*.cpp")
  foreach (SOURCE ${util_sources})	
    string (REGEX REPLACE ".cpp$" "" FILE ${SOURCE})
    atlas_add_executable (${FILE} SOURCES util/${FILE}.cpp LINK_LIBRARIES  NTAnaLib)
  endforeach (SOURCE ${util_sources})

# Install files from the package:
atlas_install_joboptions( share/*_jobOptions.py )
atlas_install_scripts( python/*.py scripts/*)

message("S.I : PACKAGE         : ${CMAKE_CURRENT_SOURCE_DIR}")
execute_process(COMMAND bash -c "echo export NTANADIR=${CMAKE_CURRENT_SOURCE_DIR}>setupFramework.sh" OUTPUT_VARIABLE ov)
execute_process(COMMAND bash -c "echo export NTANABUILD=${PROJECT_BINARY_DIR}>>setupFramework.sh" OUTPUT_VARIABLE ov)


execute_process(COMMAND bash -c "echo source ${PROJECT_BINARY_DIR}/x86*/setup.sh >>  setupFramework.sh" OUTPUT_VARIABLE ov)

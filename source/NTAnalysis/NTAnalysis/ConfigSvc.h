/* author   :   Serhat Istin
<mailto:istin@cern.ch>
*/

#ifndef CONFIGSVC_H
#define CONFIGSVC_H
#include <string>
#include <vector>
#include <unordered_map>
namespace NTAna{
    
    

struct SampleGroup{
        std::string name;
        std::string pattern;
        size_t hash();
};


    
    
    class ConfigSvc{
        public:
            ConfigSvc(const ConfigSvc&) = delete;
            ConfigSvc& operator=(const ConfigSvc&) = delete;
            ConfigSvc(ConfigSvc&&) = delete;
            ConfigSvc& operator=(ConfigSvc&&) = delete;
            static ConfigSvc* init(const std::string&);
            ~ConfigSvc();
            void dump();
            double xcnunit() const{return m_xcnunit;}
            double ilumiunit() const{return m_intlumiunit;}
            std::string blacklistedSamples() const{ return m_blacklistedSamples;}
            std::string sampleMetaDataFile() const{return m_sampleMetaDataFile;} 
            auto allowedAlgorithms() const{return m_allowedAlgorithms;}
            auto sampleGroups()const{return m_sampleGroups;}
            bool writeSampleMetaData() const{return m_writeSampleMetaData;}
            float lumifrac(std::string round)const{return m_lumifracs.at(round);}
            auto lumifracs()const{return m_lumifracs;}
            std::string datapattern()const{return m_datapattern;}
            std::string dsidpattern()const{return m_dsidpattern;}
        private:
            ConfigSvc(const std::string&);
            static ConfigSvc* m_inst;
            
            double m_xcnunit;
            double m_intlumiunit;
            bool m_writeSampleMetaData;
            std::string m_blacklistedSamples;
            std::string m_sampleMetaDataFile;
            std::vector<std::string> m_allowedAlgorithms;
            std::string m_driver;
            std::vector<SampleGroup> m_sampleGroups;
            std::unordered_map<std::string,float> m_lumifracs;
            std::string m_datapattern;
            std::string m_dsidpattern;
    };
    
    
}



#endif

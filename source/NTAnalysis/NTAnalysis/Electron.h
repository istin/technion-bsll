/* author   :   Serhat Istin
<mailto:istin@cern.ch>
*/

#ifndef ELECTRON_H
#define ELECTRON_H
#include "NTAnalysis/Particle.h"
namespace NTAna{
    class Electron : public Particle{
        public:
            Electron():Particle(),
            m_d0sig(0),m_z0sig(0),m_trgmatch(false),m_veryloose(false),m_loose(false),m_medium(false),m_tight(false),
            m_isolLooseTrackOnly(0),m_isolLoose(0),m_isolGradient(0),m_isolGradientLoose(0),m_isolFixedCutTightTrackOnly(0),
            m_isolFixedCutLoose(0),m_isolFixedCutTight(0),m_isolFixedCutTrackCone40(0),m_isolFixedCutHighPtCaloOnly(0)
            {
                
            }
            Electron(float pt,float eta,float phi,float m,int q):Particle( pt, eta, phi, m, q){}
            Electron( TLorentzVector* const lv,int q):Particle(lv,q){}
            ~Electron(){}

            bool trgmatch() const { return m_trgmatch;}
            bool veryloose() const { return m_veryloose; }
            bool loose() const { return m_loose;}
            bool medium() const { return m_medium;}
            bool tight() const { return m_tight;}
            
        private:
            float m_d0sig;
            float m_z0sig;
            bool m_trgmatch;
            bool m_veryloose;
            bool m_loose;
            bool m_medium;
            bool m_tight;
            int m_isolLooseTrackOnly;
            int m_isolLoose;
            int m_isolGradient;
            int m_isolGradientLoose;
            int m_isolFixedCutTightTrackOnly;
            int m_isolFixedCutLoose;
            int m_isolFixedCutTight;
            int m_isolFixedCutTrackCone40;
            int m_isolFixedCutHighPtCaloOnly;
            //expand electron features here
    };
}

#endif

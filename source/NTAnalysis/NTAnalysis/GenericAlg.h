/* author   :   Serhat Istin
<mailto:istin@cern.ch>
*/

#ifndef GENERICALG_H
#define GENERICALG_H

#include <NTAnalysis/Skeleton.h>
#include <TParameter.h>
class GenericAlg : public Skeleton{
public:
   GenericAlg (const std::string& name, ISvcLocator* pSvcLocator);
   StatusCode BookHistograms() override;
   StatusCode doAnalysis() override;
   virtual StatusCode finalize () override;   //could be thrown int skeleton once we agreeon a generalized procedure
private:    

};


#endif

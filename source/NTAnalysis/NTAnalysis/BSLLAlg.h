/* author   :   Serhat Istin
<mailto:istin@cern.ch>
*/

#ifndef BSLL_ALG_H
#define BSLL_ALG_H


#include <NTAnalysis/Skeleton.h>

class BSLLAlg : public Skeleton
{
public:
  // this is a standard algorithm constructor
  BSLLAlg (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode finalize () override;
  virtual StatusCode BookHistograms() override;
  StatusCode doAnalysis() override;
  
  StatusCode BookHistogram(std::string ,int  ,float  ,float ,std::string tit="None");
  void FillHistogram(const std::string& , float,float);
   ~BSLLAlg();
private:
    

    float  MLLMIN;
    float  PTLLMIN;
    float  PTJ0MIN;
    float  PTJ1MIN;
    float  HTMAX;
    float  HTJMAX;
    float  ZMASSWIN;
    float  NJETSMIN;
    float  ZMASS;
    float  WMASS;
};

#endif

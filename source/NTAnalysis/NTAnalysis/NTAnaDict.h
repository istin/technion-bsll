/* author   :   Serhat Istin
<mailto:istin@cern.ch>
*/

#include <SampleHandler/Sample.h>
#include <SampleHandler/SampleHandler.h>
#include <SampleHandler/MetaData.h>
#include <NTAnalysis/anyoption.h>
#include <EventLoop/AnaAlgorithmWrapper.h>
#include  <NTAnalysis/TorqueDriverTech.h>
#include  <NTAnalysis/BSLLAlg.h>
#include  <NTAnalysis/ConfigSvc.h>
#include  <NTAnalysis/Electron.h>
#include  <NTAnalysis/EventData.h>
#include  <NTAnalysis/GenericAlg.h>
#include  <NTAnalysis/Jet.h>
#include  <NTAnalysis/Muon.h>
#include  <NTAnalysis/Particle.h>
#include  <NTAnalysis/SampleMetaDataSvc.h>
#include <NTAnalysis/LQTree_v18_2.h>
#include <NTAnalysis/LQTree_v25.h>
#include <NTAnalysis/LQTree_v26.h>

#include  <NTAnalysis/Skeleton.h>


#ifdef __ROOTCLING__



#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ namespace EL;
#pragma link C++ namespace SH;


#pragma link C++ class BSLLAlg+;
#pragma link C++ class GenericAlg+;
#pragma link C++ class Skeleton+;
#pragma link C++ class Electron+;
#pragma link C++ class EventData+;
#pragma link C++ class Jet+;
#pragma link C++ class Particle+;
#pragma link C++ class ConfigSvc+;
#pragma link C++ class LQTree_v18_2+;
#pragma link C++ class LQTree_v25+;
#pragma link C++ class LQTree_v26+;
#pragma link C++ class Muon+;
#pragma link C++ class SampleMetaDataSvc+;
#pragma link C++ class TorqueDriverTech+;
#pragma link C++ class anyoption+;
#pragma link C++ class Sample+;
#pragma link C++ class SampleHandler+;
#pragma link C++ class SH::MetaData<double>+;
#pragma link C++ class EL::AnaAlgorithmWrapper+;

#endif

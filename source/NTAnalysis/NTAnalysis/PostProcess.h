/* author   :   Serhat Istin
<mailto:istin@cern.ch>
*/

#include "TH1F.h"
#include "TFile.h"
#include <string>
#include <vector>
#include <map>

#ifndef POSTPROCESS_H
#define POSTPROCESS_H

bool contains(std::string name,std::string subname){
    if(name.find(subname)!=std::string::npos) return true;
    return false;
}

bool NameIs(TH1F* hist,std::string name){
    std::string actualname=std::string(hist->GetName());
    if(actualname==name+"_0b" || actualname==name+"_1b" || actualname==name+"_2b" ||actualname==name+"_PRETAG") return true;

    return false;
}

TH1F* PostProcess(TH1F* origHisto){
  std::string histname=std::string(origHisto->GetName());

  if (NameIs(origHisto,"h_ptZ_Wide") ){
    double bins[]={0,60,120,180,240,300,360,420,480,540,600,660,720,900};
    int nbins=sizeof(bins)/sizeof(bins[0]);
    return (TH1F*)origHisto->Rebin(nbins-1,histname.c_str(),bins);
  }

  else if (NameIs(origHisto,"h_ptZ_Wide_4J") ){
    double bins[]={0,60,120,180,240,300,360,420,480,540,600,660,720,900};
    int nbins=sizeof(bins)/sizeof(bins[0]);
    return (TH1F*)origHisto->Rebin(nbins-1,histname.c_str(),bins);
  }

  else if ( NameIs(origHisto,"h_HT_Wide") ){
    double bins[]={0,160,320,480,640,800,1000,1200,1400,1600,2000};
    int nbins=sizeof(bins)/sizeof(bins[0]);
    return (TH1F*)origHisto->Rebin(nbins-1,histname.c_str(),bins);
  }

  else if ( NameIs(origHisto,"h_HT_Wide_4J") ){
    double bins[]={0,160,320,480,640,800,1000,1200,1400,1600,2000};
    int nbins=sizeof(bins)/sizeof(bins[0]);
    return (TH1F*)origHisto->Rebin(nbins-1,histname.c_str(),bins);
  }

  else if (NameIs(origHisto,"h_ptZ") ){
    double bins[]={0,60,120,180,240,300,360,420,500,580,640,800};
    int nbins=sizeof(bins)/sizeof(bins[0]);
    return (TH1F*)origHisto->Rebin(nbins-1,histname.c_str(),bins);
  }
  else if ( NameIs(origHisto,"h_HT") ){
    double bins[]={0,80,160,320,480,640,800,1000,1200,1400,1600,2000};
    int nbins=sizeof(bins)/sizeof(bins[0]);
    return (TH1F*)origHisto->Rebin(nbins-1,histname.c_str(),bins);
  }
  else if ( NameIs(origHisto,"h_MET") ){
    double bins[]={0,30,60,90,120,150,210,270,330,400,500};
    int nbins=sizeof(bins)/sizeof(bins[0]);
    return (TH1F*)origHisto->Rebin(nbins-1,histname.c_str(),bins);
  }
  else if ( NameIs(origHisto,"h_PTJ") ){
    double bins[]={0,60,120,180,240,300,360,420,480,600,800};
    int nbins=sizeof(bins)/sizeof(bins[0]);
    return (TH1F*)origHisto->Rebin(nbins-1,histname.c_str(),bins);
  }
  else if ( NameIs(origHisto,"h_PTLEP") ){
    double bins[]={0,60,120,180,240,300,360,420,480,600,800};
    int nbins=sizeof(bins)/sizeof(bins[0]);
    return (TH1F*)origHisto->Rebin(nbins-1,histname.c_str(),bins);
  }
  else if ( NameIs(origHisto,"h_ETALEP") ){

    return (TH1F*)origHisto->Rebin(5);
  }

  
  //2J histograms

  else if (NameIs(origHisto,"h_mZ_2J")){

    return (TH1F*)origHisto->Rebin(2);
  }
  else if (NameIs(origHisto,"h_ptZ_2J") ){
    double bins[]={0,60,120,180,240,300,360,420,480,540,660,900};
    int nbins=sizeof(bins)/sizeof(bins[0]);
    return (TH1F*)origHisto->Rebin(nbins-1,histname.c_str(),bins);
  }
  else if ( NameIs(origHisto,"h_HT_2J") ){
    double bins[]={0,80,160,320,480,640,800,1000,1200,1400,1600,2000};
    int nbins=sizeof(bins)/sizeof(bins[0]);
    return (TH1F*)origHisto->Rebin(nbins-1,histname.c_str(),bins);
  }
  else if ( NameIs(origHisto,"h_MET_2J") ){
    double bins[]={0,30,60,90,120,150,210,270,330,400,500};
    int nbins=sizeof(bins)/sizeof(bins[0]);
    return (TH1F*)origHisto->Rebin(nbins-1,histname.c_str(),bins);
  }
  else if ( NameIs(origHisto,"h_PTJ_2J") ){
    double bins[]={0,60,120,180,240,300,360,420,480,600,800};
    int nbins=sizeof(bins)/sizeof(bins[0]);
    return (TH1F*)origHisto->Rebin(nbins-1,histname.c_str(),bins);
  }
  else if ( NameIs(origHisto,"h_PTLEP_2J") ){
    double bins[]={0,60,120,180,240,300,360,420,480,600,800};
    int nbins=sizeof(bins)/sizeof(bins[0]);
    return (TH1F*)origHisto->Rebin(nbins-1,histname.c_str(),bins);
  }

  else if ( NameIs(origHisto,"h_ETALEP_2J") ){

    return (TH1F*)origHisto->Rebin(5);
  }  
  
  
  

  //4J histograms

  else if (NameIs(origHisto,"h_mZ_4J")){

    return (TH1F*)origHisto->Rebin(2);
  }
  else if (NameIs(origHisto,"h_ptZ_4J") ){
    double bins[]={0,60,120,180,240,300,360,420,480,540,660,900};
    int nbins=sizeof(bins)/sizeof(bins[0]);
    return (TH1F*)origHisto->Rebin(nbins-1,histname.c_str(),bins);
  }
  else if ( NameIs(origHisto,"h_HT_4J") ){
    double bins[]={0,80,160,320,480,640,800,1000,1200,1400,1600,2000};
    int nbins=sizeof(bins)/sizeof(bins[0]);
    return (TH1F*)origHisto->Rebin(nbins-1,histname.c_str(),bins);
  }
  else if ( NameIs(origHisto,"h_MET_4J") ){
    double bins[]={0,30,60,90,120,150,210,270,330,400,500};
    int nbins=sizeof(bins)/sizeof(bins[0]);
    return (TH1F*)origHisto->Rebin(nbins-1,histname.c_str(),bins);
  }
  else if ( NameIs(origHisto,"h_PTJ_4J") ){
    double bins[]={0,60,120,180,240,300,360,420,480,600,800};
    int nbins=sizeof(bins)/sizeof(bins[0]);
    return (TH1F*)origHisto->Rebin(nbins-1,histname.c_str(),bins);
  }
  else if ( NameIs(origHisto,"h_PTLEP_4J") ){
    double bins[]={0,60,120,180,240,300,360,420,480,600,800};
    int nbins=sizeof(bins)/sizeof(bins[0]);
    return (TH1F*)origHisto->Rebin(nbins-1,histname.c_str(),bins);
  }

  else if ( NameIs(origHisto,"h_ETALEP_4J") ){

    return (TH1F*)origHisto->Rebin(5);
  }

  else if ( NameIs(origHisto,"h_HTCUT_mZJ0_4J") ){

    return (TH1F*)origHisto->Rebin(2);
  }

  else if ( NameIs(origHisto,"h_Reco_mHQLEP") ){

    return (TH1F*)origHisto->Rebin(10);
  }
  else if ( NameIs(origHisto,"h_Reco_mVHAD") ){

    return (TH1F*)origHisto->Rebin(3);
  }
  else if ( NameIs(origHisto,"h_Reco_mHQHAD") ){

    return (TH1F*)origHisto->Rebin(10);
  }
   else if ( NameIs(origHisto,"h_Reco_mHQAVG") ){

    return (TH1F*)origHisto->Rebin(10);
  }
  
  
  else if ( NameIs(origHisto,"h_Reco_dMHQcut_mHQLEP") ){

    return (TH1F*)origHisto->Rebin(2);
  }

    else if ( NameIs(origHisto,"h_Reco_dMHQcut_mHQHAD") ){

    return (TH1F*)origHisto->Rebin(2);
  }

    else if ( NameIs(origHisto,"h_Reco_dMHQcut_mHQAVG") ){

    return (TH1F*)origHisto->Rebin(2);
  }



    else if ( NameIs(origHisto,"h_Reco_deltaMVcut_mHQHAD") ){

    return (TH1F*)origHisto->Rebin(2);
  }

      else if ( NameIs(origHisto,"h_Reco_deltaMVcut_mHQLEP") ){

    return (TH1F*)origHisto->Rebin(2);
  }
      else if ( NameIs(origHisto,"h_Reco_deltaMVcut_mHQAVG") ){

    return (TH1F*)origHisto->Rebin(2);
  }





  else{
    return (TH1F*)origHisto;
  }
}
#endif


/* author   :   Serhat Istin
<mailto:istin@cern.ch>
*/

#ifndef JET_H
#define JET_H
#include "NTAnalysis/Particle.h"
namespace NTAna{
    class Jet : public Particle{
        public:
            Jet():Particle(),m_isbtag(false),m_btagscore(0){}
            Jet(float pt,float eta,float phi,float m,int q):Particle(pt,eta,phi,m,q){}
            Jet( TLorentzVector* const lv,int q):Particle(lv,q){}
            ~Jet(){}
            bool isbTag() const{ return m_isbtag;}
            void setBtag(bool b) { m_isbtag=b;}
        private:
            bool m_isbtag;
            float m_btagscore;
            //expand jet features here ...
        
    };
}

#endif

/* author   :   Serhat Istin
<mailto:istin@cern.ch>
*/

#ifndef MUON_H
#define MUON_H
#include "NTAnalysis/Particle.h"
namespace NTAna{
    
    class Muon : public Particle{
        public:
            Muon():Particle(),m_d0sig(0),m_z0sig(0),m_isolFixedCutTight(0),m_isolLooseTrackOnly(0),
            m_isolLoose(0),m_isolGradient(0),m_isolGradientLoose(0),m_isolFixedCutTightTrackOnly(0),
            m_isolFixedCutLoose(0),m_isolFixedCutHighPtTrackOnly(0){
                
            }
            Muon(float pt,float eta,float phi,float m,int q):Particle( pt, eta, phi, m, q){}
            Muon( TLorentzVector* const lv,int q):Particle(lv,q){}
            ~Muon(){}        
        private:
            float m_d0sig;
            float m_z0sig;
            int m_isolFixedCutTight;
            int m_isolLooseTrackOnly;
            int m_isolLoose;
            int m_isolGradient;
            int m_isolGradientLoose;
            int m_isolFixedCutTightTrackOnly;
            int m_isolFixedCutLoose;
            int m_isolFixedCutHighPtTrackOnly;            
            //expand muon features here
    };
}

#endif

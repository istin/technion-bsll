/* author   :   Serhat Istin
<mailto:istin@cern.ch>
*/

#ifndef PARTICLE_H
#define PARTICLE_H
#include <TLorentzVector.h>
//Derive various particle flavours from this base class
namespace NTAna{
    class Particle {
        public:
            Particle();
            Particle(float,float,float,float,int);
            Particle( TLorentzVector* const,int);
            Particle (const Particle&);
            virtual ~Particle();
            int Q() const{ return m_q;}
            void setCharge(int q){m_q=q;}
            Particle operator+(const  Particle&);
            Particle& operator=(const Particle&);
            TLorentzVector* P4() const {return m_p4;}
            
            float Pt()const{return m_p4->Pt();}
            float Eta()const{return m_p4->Eta();}
            float Phi()const{return m_p4->Phi();}
            float M()const{return m_p4->M();}
            float E()const{return m_p4->E();}
            
            static float DeltaR(const Particle&l,const Particle&r){ return l.P4()->DeltaR(*r.P4()); }
            static float DeltaR(const Particle* l,const Particle* r){ return l->P4()->DeltaR(*r->P4()); }
            static float DeltaPhi(const Particle *l,const Particle* r){ return l->P4()->DeltaPhi(*r->P4()); }
            
        private:    
            int m_q;
            TLorentzVector *m_p4;
            
        
    };
}
#endif

/* author   :   Serhat Istin
<mailto:istin@cern.ch>
*/

#ifndef SAMPLEMETADATASVC_H
#define SAMPLEMETADATASVC_H
#include <string>
#include <unordered_map>
#include <iostream>
namespace NTAna{

//shall be accessed through DID keys
struct SampleMetaData{
  std::string proc;
  float xcn;
  float feff;
  float kfact;
};


    class SampleMetaDataSvc{
        //avoid the unintended way of using the class
        SampleMetaDataSvc(const SampleMetaDataSvc&) = delete;
        SampleMetaDataSvc& operator=(const SampleMetaDataSvc&) = delete;
        SampleMetaDataSvc(SampleMetaDataSvc&&) = delete;
        SampleMetaDataSvc& operator=(SampleMetaDataSvc&&) = delete;

        public:
            static SampleMetaDataSvc* init(const std::string&);
            std::unordered_map<int,SampleMetaData> metaData()const{return m_smd;}
            SampleMetaData get(int did)const {
                if (m_smd.find(did)==m_smd.end()){
                    std::cout<<"DID "<<did <<" does not exist in the map"<<std::endl;
                    exit(9);
                }
                return m_smd.at(did); 
                
            }
            std::string proc(int did)const{return get(did).proc;}
            float xcn(int did)const{return get(did).xcn;}
            float kfactor(int did)const{return get(did).kfact;}
            float filteff(int did) const{return get(did).feff;}
            void dump(int)const;
            ~SampleMetaDataSvc();
        private:
            SampleMetaDataSvc(const std::string&);
            static SampleMetaDataSvc* m_inst;
            std::unordered_map<int,SampleMetaData> m_smd;
    
    };
}

#endif


#ifndef LOOP_TORQUE_DRIVER_TECH_HH
#define LOOP_TORQUE_DRIVER_TECH_HH

#include <EventLoop/Global.h>

#include <EventLoop/TorqueDriver.h>
#include <SampleHandler/Global.h>

namespace NTAna{
  class TorqueDriverTech : public EL::TorqueDriver
  {
  //private:
     void
    batchSubmit ( const std::string& location, const SH::MetaObject& options,
               const std::vector<std::size_t>& jobIndices, bool resubmit) const override;


#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wunknown-pragmas"
#pragma GCC diagnostic ignored "-Winconsistent-missing-override"
#pragma GCC diagnostic pop
  };
}

#endif


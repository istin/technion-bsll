/* author   :   Serhat Istin
<mailto:istin@cern.ch>
*/

#ifndef EVENTDATA_H
#define EVENTDATA_H
#include "NTAnalysis/Particle.h"
#include "NTAnalysis/Electron.h"
#include "NTAnalysis/Muon.h"
#include "NTAnalysis/Jet.h"
#include <TParameter.h>
#include <unordered_map>
#include <TBranch.h>
#include <TTree.h>



namespace NTAna{

    //below is how we handle scalefactor systematics. Most generally, systematic variation of the event weight come in
    //a separate branch in this we just juse that branch as is instead of the nominal branch
    //In some cases : E.g LQTree, the relevant systematic variaton is handled as an additional multiplier 
    //on top of the nominal weight

    enum SF_VARIATION_SCHEME{REPLACE,MULTIPLIER};    

    class EventData{
        public:
            EventData();
            virtual ~EventData();          
            virtual void FetchEvent()=0;//to be manually implemented in the child class (Usually the one out of MakeClass)
            virtual void Init(TTree*)=0;// !!! DO NOT  Implement this manually !!!! it will be automatically overriden by MakeClass' Init(Tree*)
            
            
            virtual void Initialize(TTree*);//binds to the tree. we dont own the Tree*          
            void Reset();
            void Next();
            std::vector<Particle*> Electrons() const{ return m_Electrons;}
            std::vector<Particle*> Muons() const{ return m_Muons;}
            std::vector<Particle*> Jets() const{ return m_Jets;}
            std::vector<Particle*> BJets() const{ return m_BJets;}
            float puWeight() const {return m_PU_weight;}
            float TrkMET() const {return m_trkMET;}
            float CaloMet()const {return m_caloMET;}
            Double_t EventWeight() const;
            Double_t Weight() const{return m_weight;}//NOMINAL WEIGHT!!!
            Double_t PickLeafValue(const std::string& )const;
            int RunNo()const{return m_runNo;}
            void setEMUNIT(float U){ m_emunit=U;}
            float EMUNIT() const {return m_emunit;}
            void setSF_Scheme(int sch,const std::string& sysname);//sysname-->branch name usually

            int dsid()const{ return m_dsid;}
            void setDSID(int d){ m_dsid=d; }
        protected:
            std::vector<Particle*> m_Electrons;
            std::vector<Particle*> m_Muons;
            std::vector<Particle*> m_Jets;
            std::vector<Particle*> m_BJets;
            float m_trkMET;
            float m_caloMET;
            Double_t m_weight;//nominal
            float m_PU_weight;
            float m_btagSF;
            int m_runNo;
            int m_dsid;
            
            float m_emunit;

            TTree* m_tree;
            int m_sf_variation_scheme;
            std::string m_sf_variation_name;




    };
}


#endif

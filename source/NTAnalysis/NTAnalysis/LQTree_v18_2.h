//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon May 27 14:30:49 2019 by ROOT version 6.16/00
// from TTree BaseSelection_lq_tree_Final/
// found on file: user.yafik.16958274._000001.hist-output.root
//////////////////////////////////////////////////////////

#ifndef LQTree_v18_2_h
#define LQTree_v18_2_h
#include "NTAnalysis/EventData.h"
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include <vector>
using std::vector;


class LQTree_v18_2 : public NTAna::EventData {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain
   void FetchEvent();
// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   vector<float>   *jet_MV2cl100_discriminant;
   vector<float>   *jet_MV2c100_discriminant;
   vector<float>   *jet_MV2c10_discriminant;
   vector<float>   *jet_DL1_pb;
   vector<float>   *jet_DL1_pc;
   vector<float>   *jet_DL1_pu;
   vector<float>   *jet_DL1mu_pb;
   vector<float>   *jet_DL1mu_pc;
   vector<float>   *jet_DL1mu_pu;
   Float_t         pileup_weight;
   Float_t         mc_weight;
   vector<float>   *mc_weights;
   Float_t         total_electron_sf;
   Float_t         total_muon_iso_sf;
   Float_t         total_muon_reco_sf;
   Float_t         average_int_per_xing;
   Int_t           run_number;
   Int_t           event_number;
   UInt_t          muon_n;
   Int_t           muon_prompt_n;
   vector<float>   *muon_e;
   vector<float>   *muon_pt;
   vector<float>   *muon_eta;
   vector<float>   *muon_phi;
   vector<float>   *muon_d0sig;
   vector<float>   *muon_z0sig;
   vector<float>   *muon_z0sintheta;
   vector<float>   *muon_charge;
   vector<int>     *muon_has_truth_match;
   vector<int>     *muon_truth_type;
   vector<int>     *muon_truth_origin;
   vector<int>     *muon_truth_charge;
   vector<vector<float> > *muon_jet_dr;
   vector<vector<float> > *muon_jet_dhi;
   vector<float>   *muon_met_dphi;
   vector<float>   *muon_met_dr;
   vector<unsigned int> *muon_pt_order;
   vector<unsigned int> *muon_lepton_pt_order;
   vector<float>   *muon_iso_sf;
   vector<float>   *muon_reco_sf;
   vector<bool>    *muon_is_prompt;
   vector<bool>    *muon_is_num;
   vector<bool>    *muon_is_denom;
   vector<float>   *muon_ptvar30_over_pt;
   vector<float>   *muon_id_pt;
   vector<float>   *muon_ms_pt;
   UInt_t          electron_n;
   Int_t           electron_prompt_n;
   vector<float>   *electron_e;
   vector<float>   *electron_pt;
   vector<float>   *electron_eta;
   vector<float>   *electron_phi;
   vector<float>   *electron_d0sig;
   vector<float>   *electron_z0sig;
   vector<float>   *electron_z0sintheta;
   vector<float>   *electron_charge;
   vector<int>     *electron_has_truth_match;
   vector<int>     *electron_has_trigger_match;
   vector<int>     *electron_truth_type;
   vector<int>     *electron_truth_origin;
   vector<int>     *electron_truth_charge;
   vector<float>   *electron_met_dphi;
   vector<float>   *electron_met_dr;
   vector<unsigned int> *electron_pt_order;
   vector<unsigned int> *electron_lepton_pt_order;
   vector<float>   *electron_scale_factor;
   vector<float>   *electron_reco_sf;
   vector<float>   *electron_id_sf;
   vector<float>   *electron_iso_sf;
   vector<bool>    *electron_is_prompt;
   vector<bool>    *electron_is_num;
   vector<bool>    *electron_is_denom;
   vector<bool>    *electron_is_denom_id;
   vector<bool>    *electron_is_denom_d0;
   vector<char>    *electron_is_very_loose;
   vector<char>    *electron_is_loose;
   vector<char>    *electron_is_bl_loose;
   vector<char>    *electron_is_medium;
   vector<char>    *electron_is_tight;
   UInt_t          jet_n;
   vector<float>   *jet_e;
   vector<float>   *jet_pt;
   vector<float>   *jet_eta;
   vector<float>   *jet_phi;
   vector<float>   *jet_m;
   vector<float>   *jet_jvt;
   vector<bool>    *jet_has_btag;
   vector<float>   *jet_btag_sf;
   vector<float>   *jet_btag_score;
   vector<float>   *jet_met_dphi;
   vector<float>   *jet_met_dr;
   Float_t         track_met;
   Float_t         calo_met;
   Float_t         track_met_phi;
   Float_t         calo_met_phi;
   Float_t         inv_mass;
   Float_t         inv_mass_light_leptons;
   Float_t         inv_mass_electrons;
   Float_t         inv_mass_muons;
   Float_t         inv_mass_taus;
   vector<float>   *lq_mass;
   Float_t         lq_mass_max;
   Float_t         eff_mass;
   Float_t         eff_mass_light_leptons;
   Float_t         eff_mass_electrons;
   Float_t         eff_mass_muons;
   Float_t         eff_mass_taus;
   Float_t         trans_mass;
   Float_t         trans_mass_track_met;
   vector<float>   *inv_dilep_mass;
   vector<bool>    *inv_dielectron_mask;
   vector<bool>    *inv_dimuon_mask;
   vector<bool>    *inv_ditau_mask;
   vector<bool>    *inv_Z_mask;
   Float_t         dimuon_pt;
   Float_t         dielectron_pt;
   Float_t         ht_jets;
   Float_t         ht_leptons;
   Double_t        weight;

   // List of branches
   TBranch        *b_jet_MV2cl100_discriminant;   //!
   TBranch        *b_jet_MV2c100_discriminant;   //!
   TBranch        *b_jet_MV2c10_discriminant;   //!
   TBranch        *b_jet_DL1_pb;   //!
   TBranch        *b_jet_DL1_pc;   //!
   TBranch        *b_jet_DL1_pu;   //!
   TBranch        *b_jet_DL1mu_pb;   //!
   TBranch        *b_jet_DL1mu_pc;   //!
   TBranch        *b_jet_DL1mu_pu;   //!
   TBranch        *b_pileup_weight;   //!
   TBranch        *b_mc_weight;   //!
   TBranch        *b_mc_weights;   //!
   TBranch        *b_total_electron_sf;   //!
   TBranch        *b_total_muon_iso_sf;   //!
   TBranch        *b_total_muon_reco_sf;   //!
   TBranch        *b_average_int_per_xing;   //!
   TBranch        *b_run_number;   //!
   TBranch        *b_event_number;   //!
   TBranch        *b_muon_n;   //!
   TBranch        *b_muon_prompt_n;   //!
   TBranch        *b_muon_e;   //!
   TBranch        *b_muon_pt;   //!
   TBranch        *b_muon_eta;   //!
   TBranch        *b_muon_phi;   //!
   TBranch        *b_muon_d0sig;   //!
   TBranch        *b_muon_z0sig;   //!
   TBranch        *b_muon_z0sintheta;   //!
   TBranch        *b_muon_charge;   //!
   TBranch        *b_muon_has_truth_match;   //!
   TBranch        *b_muon_truth_type;   //!
   TBranch        *b_muon_truth_origin;   //!
   TBranch        *b_muon_truth_charge;   //!
   TBranch        *b_muon_jet_dr;   //!
   TBranch        *b_muon_jet_dhi;   //!
   TBranch        *b_muon_met_dphi;   //!
   TBranch        *b_muon_met_dr;   //!
   TBranch        *b_muon_pt_order;   //!
   TBranch        *b_muon_lepton_pt_order;   //!
   TBranch        *b_muon_iso_sf;   //!
   TBranch        *b_muon_reco_sf;   //!
   TBranch        *b_muon_is_prompt;   //!
   TBranch        *b_muon_is_num;   //!
   TBranch        *b_muon_is_denom;   //!
   TBranch        *b_muon_ptvar30_over_pt;   //!
   TBranch        *b_muon_id_pt;   //!
   TBranch        *b_muon_ms_pt;   //!
   TBranch        *b_electron_n;   //!
   TBranch        *b_electron_prompt_n;   //!
   TBranch        *b_electron_e;   //!
   TBranch        *b_electron_pt;   //!
   TBranch        *b_electron_eta;   //!
   TBranch        *b_electron_phi;   //!
   TBranch        *b_electron_d0sig;   //!
   TBranch        *b_electron_z0sig;   //!
   TBranch        *b_electron_z0sintheta;   //!
   TBranch        *b_electron_charge;   //!
   TBranch        *b_electron_has_truth_match;   //!
   TBranch        *b_electron_has_trigger_match;   //!
   TBranch        *b_electron_truth_type;   //!
   TBranch        *b_electron_truth_origin;   //!
   TBranch        *b_electron_truth_charge;   //!
   TBranch        *b_electron_met_dphi;   //!
   TBranch        *b_electron_met_dr;   //!
   TBranch        *b_electron_pt_order;   //!
   TBranch        *b_electron_lepton_pt_order;   //!
   TBranch        *b_electron_scale_factor;   //!
   TBranch        *b_electron_reco_sf;   //!
   TBranch        *b_electron_id_sf;   //!
   TBranch        *b_electron_iso_sf;   //!
   TBranch        *b_electron_is_prompt;   //!
   TBranch        *b_electron_is_num;   //!
   TBranch        *b_electron_is_denom;   //!
   TBranch        *b_electron_is_denom_id;   //!
   TBranch        *b_electron_is_denom_d0;   //!
   TBranch        *b_electron_is_very_loose;   //!
   TBranch        *b_electron_is_loose;   //!
   TBranch        *b_electron_is_bl_loose;   //!
   TBranch        *b_electron_is_medium;   //!
   TBranch        *b_electron_is_tight;   //!
   TBranch        *b_jet_n;   //!
   TBranch        *b_jet_e;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_m;   //!
   TBranch        *b_jet_jvt;   //!
   TBranch        *b_jet_has_btag;   //!
   TBranch        *b_jet_btag_sf;   //!
   TBranch        *b_jet_btag_score;   //!
   TBranch        *b_jet_met_dphi;   //!
   TBranch        *b_jet_met_dr;   //!
   TBranch        *b_track_met;   //!
   TBranch        *b_calo_met;   //!
   TBranch        *b_track_met_phi;   //!
   TBranch        *b_calo_met_phi;   //!
   TBranch        *b_inv_mass;   //!
   TBranch        *b_inv_mass_light_leptons;   //!
   TBranch        *b_inv_mass_electrons;   //!
   TBranch        *b_inv_mass_muons;   //!
   TBranch        *b_inv_mass_taus;   //!
   TBranch        *b_lq_mass;   //!
   TBranch        *b_lq_mass_max;   //!
   TBranch        *b_eff_mass;   //!
   TBranch        *b_eff_mass_light_leptons;   //!
   TBranch        *b_eff_mass_electrons;   //!
   TBranch        *b_eff_mass_muons;   //!
   TBranch        *b_eff_mass_taus;   //!
   TBranch        *b_trans_mass;   //!
   TBranch        *b_trans_mass_track_met;   //!
   TBranch        *b_inv_dilep_mass;   //!
   TBranch        *b_inv_dielectron_mask;   //!
   TBranch        *b_inv_dimuon_mask;   //!
   TBranch        *b_inv_ditau_mask;   //!
   TBranch        *b_inv_Z_mask;   //!
   TBranch        *b_dimuon_pt;   //!
   TBranch        *b_dielectron_pt;   //!
   TBranch        *b_ht_jets;   //!
   TBranch        *b_ht_leptons;   //!
   TBranch        *b_weight;   //!

   LQTree_v18_2(TTree *tree=0);
   virtual ~LQTree_v18_2();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef LQTree_v18_2_cxx
LQTree_v18_2::LQTree_v18_2(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("user.yafik.16958274._000001.hist-output.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("user.yafik.16958274._000001.hist-output.root");
      }
      TDirectory * dir = (TDirectory*)f->Get("user.yafik.16958274._000001.hist-output.root:/Nominal");
      dir->GetObject("BaseSelection_lq_tree_Final",tree);

   }
   Init(tree);
}

LQTree_v18_2::~LQTree_v18_2()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t LQTree_v18_2::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t LQTree_v18_2::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void LQTree_v18_2::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   jet_MV2cl100_discriminant = 0;
   jet_MV2c100_discriminant = 0;
   jet_MV2c10_discriminant = 0;
   jet_DL1_pb = 0;
   jet_DL1_pc = 0;
   jet_DL1_pu = 0;
   jet_DL1mu_pb = 0;
   jet_DL1mu_pc = 0;
   jet_DL1mu_pu = 0;
   mc_weights = 0;
   muon_e = 0;
   muon_pt = 0;
   muon_eta = 0;
   muon_phi = 0;
   muon_d0sig = 0;
   muon_z0sig = 0;
   muon_z0sintheta = 0;
   muon_charge = 0;
   muon_has_truth_match = 0;
   muon_truth_type = 0;
   muon_truth_origin = 0;
   muon_truth_charge = 0;
   muon_jet_dr = 0;
   muon_jet_dhi = 0;
   muon_met_dphi = 0;
   muon_met_dr = 0;
   muon_pt_order = 0;
   muon_lepton_pt_order = 0;
   muon_iso_sf = 0;
   muon_reco_sf = 0;
   muon_is_prompt = 0;
   muon_is_num = 0;
   muon_is_denom = 0;
   muon_ptvar30_over_pt = 0;
   muon_id_pt = 0;
   muon_ms_pt = 0;
   electron_e = 0;
   electron_pt = 0;
   electron_eta = 0;
   electron_phi = 0;
   electron_d0sig = 0;
   electron_z0sig = 0;
   electron_z0sintheta = 0;
   electron_charge = 0;
   electron_has_truth_match = 0;
   electron_has_trigger_match = 0;
   electron_truth_type = 0;
   electron_truth_origin = 0;
   electron_truth_charge = 0;
   electron_met_dphi = 0;
   electron_met_dr = 0;
   electron_pt_order = 0;
   electron_lepton_pt_order = 0;
   electron_scale_factor = 0;
   electron_reco_sf = 0;
   electron_id_sf = 0;
   electron_iso_sf = 0;
   electron_is_prompt = 0;
   electron_is_num = 0;
   electron_is_denom = 0;
   electron_is_denom_id = 0;
   electron_is_denom_d0 = 0;
   electron_is_very_loose = 0;
   electron_is_loose = 0;
   electron_is_bl_loose = 0;
   electron_is_medium = 0;
   electron_is_tight = 0;
   jet_e = 0;
   jet_pt = 0;
   jet_eta = 0;
   jet_phi = 0;
   jet_m = 0;
   jet_jvt = 0;
   jet_has_btag = 0;
   jet_btag_sf = 0;
   jet_btag_score = 0;
   jet_met_dphi = 0;
   jet_met_dr = 0;
   lq_mass = 0;
   inv_dilep_mass = 0;
   inv_dielectron_mask = 0;
   inv_dimuon_mask = 0;
   inv_ditau_mask = 0;
   inv_Z_mask = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("jet_MV2cl100_discriminant", &jet_MV2cl100_discriminant, &b_jet_MV2cl100_discriminant);
   fChain->SetBranchAddress("jet_MV2c100_discriminant", &jet_MV2c100_discriminant, &b_jet_MV2c100_discriminant);
   fChain->SetBranchAddress("jet_MV2c10_discriminant", &jet_MV2c10_discriminant, &b_jet_MV2c10_discriminant);
   fChain->SetBranchAddress("jet_DL1_pb", &jet_DL1_pb, &b_jet_DL1_pb);
   fChain->SetBranchAddress("jet_DL1_pc", &jet_DL1_pc, &b_jet_DL1_pc);
   fChain->SetBranchAddress("jet_DL1_pu", &jet_DL1_pu, &b_jet_DL1_pu);
   fChain->SetBranchAddress("jet_DL1mu_pb", &jet_DL1mu_pb, &b_jet_DL1mu_pb);
   fChain->SetBranchAddress("jet_DL1mu_pc", &jet_DL1mu_pc, &b_jet_DL1mu_pc);
   fChain->SetBranchAddress("jet_DL1mu_pu", &jet_DL1mu_pu, &b_jet_DL1mu_pu);
   fChain->SetBranchAddress("pileup_weight", &pileup_weight, &b_pileup_weight);
   fChain->SetBranchAddress("mc_weight", &mc_weight, &b_mc_weight);
   fChain->SetBranchAddress("mc_weights", &mc_weights, &b_mc_weights);
   fChain->SetBranchAddress("total_electron_sf", &total_electron_sf, &b_total_electron_sf);
   fChain->SetBranchAddress("total_muon_iso_sf", &total_muon_iso_sf, &b_total_muon_iso_sf);
   fChain->SetBranchAddress("total_muon_reco_sf", &total_muon_reco_sf, &b_total_muon_reco_sf);
   fChain->SetBranchAddress("average_int_per_xing", &average_int_per_xing, &b_average_int_per_xing);
   fChain->SetBranchAddress("run_number", &run_number, &b_run_number);
   fChain->SetBranchAddress("event_number", &event_number, &b_event_number);
   fChain->SetBranchAddress("muon_n", &muon_n, &b_muon_n);
   fChain->SetBranchAddress("muon_prompt_n", &muon_prompt_n, &b_muon_prompt_n);
   fChain->SetBranchAddress("muon_e", &muon_e, &b_muon_e);
   fChain->SetBranchAddress("muon_pt", &muon_pt, &b_muon_pt);
   fChain->SetBranchAddress("muon_eta", &muon_eta, &b_muon_eta);
   fChain->SetBranchAddress("muon_phi", &muon_phi, &b_muon_phi);
   fChain->SetBranchAddress("muon_d0sig", &muon_d0sig, &b_muon_d0sig);
   fChain->SetBranchAddress("muon_z0sig", &muon_z0sig, &b_muon_z0sig);
   fChain->SetBranchAddress("muon_z0sintheta", &muon_z0sintheta, &b_muon_z0sintheta);
   fChain->SetBranchAddress("muon_charge", &muon_charge, &b_muon_charge);
   fChain->SetBranchAddress("muon_has_truth_match", &muon_has_truth_match, &b_muon_has_truth_match);
   fChain->SetBranchAddress("muon_truth_type", &muon_truth_type, &b_muon_truth_type);
   fChain->SetBranchAddress("muon_truth_origin", &muon_truth_origin, &b_muon_truth_origin);
   fChain->SetBranchAddress("muon_truth_charge", &muon_truth_charge, &b_muon_truth_charge);
   fChain->SetBranchAddress("muon_jet_dr", &muon_jet_dr, &b_muon_jet_dr);
   fChain->SetBranchAddress("muon_jet_dhi", &muon_jet_dhi, &b_muon_jet_dhi);
   fChain->SetBranchAddress("muon_met_dphi", &muon_met_dphi, &b_muon_met_dphi);
   fChain->SetBranchAddress("muon_met_dr", &muon_met_dr, &b_muon_met_dr);
   fChain->SetBranchAddress("muon_pt_order", &muon_pt_order, &b_muon_pt_order);
   fChain->SetBranchAddress("muon_lepton_pt_order", &muon_lepton_pt_order, &b_muon_lepton_pt_order);
   fChain->SetBranchAddress("muon_iso_sf", &muon_iso_sf, &b_muon_iso_sf);
   fChain->SetBranchAddress("muon_reco_sf", &muon_reco_sf, &b_muon_reco_sf);
   fChain->SetBranchAddress("muon_is_prompt", &muon_is_prompt, &b_muon_is_prompt);
   fChain->SetBranchAddress("muon_is_num", &muon_is_num, &b_muon_is_num);
   fChain->SetBranchAddress("muon_is_denom", &muon_is_denom, &b_muon_is_denom);
   fChain->SetBranchAddress("muon_ptvar30_over_pt", &muon_ptvar30_over_pt, &b_muon_ptvar30_over_pt);
   fChain->SetBranchAddress("muon_id_pt", &muon_id_pt, &b_muon_id_pt);
   fChain->SetBranchAddress("muon_ms_pt", &muon_ms_pt, &b_muon_ms_pt);
   fChain->SetBranchAddress("electron_n", &electron_n, &b_electron_n);
   fChain->SetBranchAddress("electron_prompt_n", &electron_prompt_n, &b_electron_prompt_n);
   fChain->SetBranchAddress("electron_e", &electron_e, &b_electron_e);
   fChain->SetBranchAddress("electron_pt", &electron_pt, &b_electron_pt);
   fChain->SetBranchAddress("electron_eta", &electron_eta, &b_electron_eta);
   fChain->SetBranchAddress("electron_phi", &electron_phi, &b_electron_phi);
   fChain->SetBranchAddress("electron_d0sig", &electron_d0sig, &b_electron_d0sig);
   fChain->SetBranchAddress("electron_z0sig", &electron_z0sig, &b_electron_z0sig);
   fChain->SetBranchAddress("electron_z0sintheta", &electron_z0sintheta, &b_electron_z0sintheta);
   fChain->SetBranchAddress("electron_charge", &electron_charge, &b_electron_charge);
   fChain->SetBranchAddress("electron_has_truth_match", &electron_has_truth_match, &b_electron_has_truth_match);
   fChain->SetBranchAddress("electron_has_trigger_match", &electron_has_trigger_match, &b_electron_has_trigger_match);
   fChain->SetBranchAddress("electron_truth_type", &electron_truth_type, &b_electron_truth_type);
   fChain->SetBranchAddress("electron_truth_origin", &electron_truth_origin, &b_electron_truth_origin);
   fChain->SetBranchAddress("electron_truth_charge", &electron_truth_charge, &b_electron_truth_charge);
   fChain->SetBranchAddress("electron_met_dphi", &electron_met_dphi, &b_electron_met_dphi);
   fChain->SetBranchAddress("electron_met_dr", &electron_met_dr, &b_electron_met_dr);
   fChain->SetBranchAddress("electron_pt_order", &electron_pt_order, &b_electron_pt_order);
   fChain->SetBranchAddress("electron_lepton_pt_order", &electron_lepton_pt_order, &b_electron_lepton_pt_order);
   fChain->SetBranchAddress("electron_scale_factor", &electron_scale_factor, &b_electron_scale_factor);
   fChain->SetBranchAddress("electron_reco_sf", &electron_reco_sf, &b_electron_reco_sf);
   fChain->SetBranchAddress("electron_id_sf", &electron_id_sf, &b_electron_id_sf);
   fChain->SetBranchAddress("electron_iso_sf", &electron_iso_sf, &b_electron_iso_sf);
   fChain->SetBranchAddress("electron_is_prompt", &electron_is_prompt, &b_electron_is_prompt);
   fChain->SetBranchAddress("electron_is_num", &electron_is_num, &b_electron_is_num);
   fChain->SetBranchAddress("electron_is_denom", &electron_is_denom, &b_electron_is_denom);
   fChain->SetBranchAddress("electron_is_denom_id", &electron_is_denom_id, &b_electron_is_denom_id);
   fChain->SetBranchAddress("electron_is_denom_d0", &electron_is_denom_d0, &b_electron_is_denom_d0);
   fChain->SetBranchAddress("electron_is_very_loose", &electron_is_very_loose, &b_electron_is_very_loose);
   fChain->SetBranchAddress("electron_is_loose", &electron_is_loose, &b_electron_is_loose);
   fChain->SetBranchAddress("electron_is_bl_loose", &electron_is_bl_loose, &b_electron_is_bl_loose);
   fChain->SetBranchAddress("electron_is_medium", &electron_is_medium, &b_electron_is_medium);
   fChain->SetBranchAddress("electron_is_tight", &electron_is_tight, &b_electron_is_tight);
   fChain->SetBranchAddress("jet_n", &jet_n, &b_jet_n);
   fChain->SetBranchAddress("jet_e", &jet_e, &b_jet_e);
   fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("jet_m", &jet_m, &b_jet_m);
   fChain->SetBranchAddress("jet_jvt", &jet_jvt, &b_jet_jvt);
   fChain->SetBranchAddress("jet_has_btag", &jet_has_btag, &b_jet_has_btag);
   fChain->SetBranchAddress("jet_btag_sf", &jet_btag_sf, &b_jet_btag_sf);
   fChain->SetBranchAddress("jet_btag_score", &jet_btag_score, &b_jet_btag_score);
   fChain->SetBranchAddress("jet_met_dphi", &jet_met_dphi, &b_jet_met_dphi);
   fChain->SetBranchAddress("jet_met_dr", &jet_met_dr, &b_jet_met_dr);
   fChain->SetBranchAddress("track_met", &track_met, &b_track_met);
   fChain->SetBranchAddress("calo_met", &calo_met, &b_calo_met);
   fChain->SetBranchAddress("track_met_phi", &track_met_phi, &b_track_met_phi);
   fChain->SetBranchAddress("calo_met_phi", &calo_met_phi, &b_calo_met_phi);
   fChain->SetBranchAddress("inv_mass", &inv_mass, &b_inv_mass);
   fChain->SetBranchAddress("inv_mass_light_leptons", &inv_mass_light_leptons, &b_inv_mass_light_leptons);
   fChain->SetBranchAddress("inv_mass_electrons", &inv_mass_electrons, &b_inv_mass_electrons);
   fChain->SetBranchAddress("inv_mass_muons", &inv_mass_muons, &b_inv_mass_muons);
   fChain->SetBranchAddress("inv_mass_taus", &inv_mass_taus, &b_inv_mass_taus);
   fChain->SetBranchAddress("lq_mass", &lq_mass, &b_lq_mass);
   fChain->SetBranchAddress("lq_mass_max", &lq_mass_max, &b_lq_mass_max);
   fChain->SetBranchAddress("eff_mass", &eff_mass, &b_eff_mass);
   fChain->SetBranchAddress("eff_mass_light_leptons", &eff_mass_light_leptons, &b_eff_mass_light_leptons);
   fChain->SetBranchAddress("eff_mass_electrons", &eff_mass_electrons, &b_eff_mass_electrons);
   fChain->SetBranchAddress("eff_mass_muons", &eff_mass_muons, &b_eff_mass_muons);
   fChain->SetBranchAddress("eff_mass_taus", &eff_mass_taus, &b_eff_mass_taus);
   fChain->SetBranchAddress("trans_mass", &trans_mass, &b_trans_mass);
   fChain->SetBranchAddress("trans_mass_track_met", &trans_mass_track_met, &b_trans_mass_track_met);
   fChain->SetBranchAddress("inv_dilep_mass", &inv_dilep_mass, &b_inv_dilep_mass);
   fChain->SetBranchAddress("inv_dielectron_mask", &inv_dielectron_mask, &b_inv_dielectron_mask);
   fChain->SetBranchAddress("inv_dimuon_mask", &inv_dimuon_mask, &b_inv_dimuon_mask);
   fChain->SetBranchAddress("inv_ditau_mask", &inv_ditau_mask, &b_inv_ditau_mask);
   fChain->SetBranchAddress("inv_Z_mask", &inv_Z_mask, &b_inv_Z_mask);
   fChain->SetBranchAddress("dimuon_pt", &dimuon_pt, &b_dimuon_pt);
   fChain->SetBranchAddress("dielectron_pt", &dielectron_pt, &b_dielectron_pt);
   fChain->SetBranchAddress("ht_jets", &ht_jets, &b_ht_jets);
   fChain->SetBranchAddress("ht_leptons", &ht_leptons, &b_ht_leptons);
   fChain->SetBranchAddress("weight", &weight, &b_weight);
   Notify();
}

Bool_t LQTree_v18_2::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void LQTree_v18_2::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t LQTree_v18_2::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef LQTree_v18_2_cxx

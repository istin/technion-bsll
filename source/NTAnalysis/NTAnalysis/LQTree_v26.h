//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sun Jun  2 17:03:53 2019 by ROOT version 6.14/08
// from TTree BaseSelection_lq_tree_syst_Final/
// found on file: user.morgens.361106.e3601_s3126_r10201_p3705.LQ.v26.0_hist/user.morgens.18174523._000008.hist-output.root
//////////////////////////////////////////////////////////

#ifndef LQTree_v26_h
#define LQTree_v26_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <NTAnalysis/EventData.h>
// Header file for the classes stored in the TTree if any.
#include <vector>
using std::vector;

class LQTree_v26 : public NTAna::EventData {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain
   void FetchEvent();
// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   UInt_t          muon_n;
   vector<float>   *muon_e;
   vector<float>   *muon_pt;
   vector<float>   *muon_eta;
   vector<float>   *muon_phi;
   UInt_t          electron_n;
   vector<float>   *electron_e;
   vector<float>   *electron_pt;
   vector<float>   *electron_eta;
   vector<float>   *electron_phi;
   UInt_t          jet_n;
   vector<float>   *jet_e;
   vector<float>   *jet_pt;
   vector<float>   *jet_eta;
   vector<float>   *jet_phi;
   vector<float>   *jet_m;
   vector<bool>    *jet_has_btag;
   Float_t         calo_met;
   Float_t         inv_mass_electrons;
   Float_t         inv_mass_muons;
   Float_t         lq_mass_max;
   Float_t         dimuon_pt;
   Float_t         dielectron_pt;
   Float_t         ht_jets;
   Float_t         ht_leptons;
   Double_t        weight;
   Int_t           HLT_e60_lhmedium_nod0_acceptance;
   Int_t           HLT_e140_lhloose_nod0_acceptance;
   Int_t           HLT_mu50_acceptance;
   Float_t         pileup_weight;
   Float_t         mc_weight;
   Float_t         truth_mll;
   Float_t         average_int_per_xing;
   Int_t           run_number;
   Int_t           event_number;
   vector<float>   *jet_jvt;
   vector<float>   *jet_btag_sf;
   Double_t        weight_PRW_DATASF__1up;
   Double_t        weight_PRW_DATASF__1down;
   Double_t        weight_MUON_EFF_TTVA_STAT__1up;
   Double_t        weight_MUON_EFF_TTVA_STAT__1down;
   Double_t        weight_MUON_EFF_TTVA_SYS__1up;
   Double_t        weight_MUON_EFF_TTVA_SYS__1down;
   Double_t        weight_MUON_EFF_ISO_STAT__1up;
   Double_t        weight_MUON_EFF_ISO_STAT__1down;
   Double_t        weight_MUON_EFF_ISO_SYS__1up;
   Double_t        weight_MUON_EFF_ISO_SYS__1down;
   Double_t        weight_MUON_EFF_RECO_STAT__1up;
   Double_t        weight_MUON_EFF_RECO_STAT__1down;
   Double_t        weight_MUON_EFF_RECO_SYS__1up;
   Double_t        weight_MUON_EFF_RECO_SYS__1down;
   Double_t        weight_JET_JvtEfficiency__1up;
   Double_t        weight_JET_JvtEfficiency__1down;
   Double_t        weight_FT_EFF_extrapolation__1up;
   Double_t        weight_FT_EFF_extrapolation__1down;
   Double_t        weight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Double_t        weight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Double_t        weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Double_t        weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Double_t        weight_EL_EFF_Trigger_TotalCorrUncertainty__1up;
   Double_t        weight_EL_EFF_Trigger_TotalCorrUncertainty__1down;
   Double_t        weight_EL_EFF_TriggerEff_TotalCorrUncertainty__1up;
   Double_t        weight_EL_EFF_TriggerEff_TotalCorrUncertainty__1down;
   Double_t        weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Double_t        weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Double_t        weight_MUON_EFF_TrigStatUncertainty__1up;
   Double_t        weight_MUON_EFF_TrigStatUncertainty__1down;
   Double_t        weight_MUON_EFF_TrigSystUncertainty__1up;
   Double_t        weight_MUON_EFF_TrigSystUncertainty__1down;
   Double_t        weight_FT_EFF_Eigen_B_0__1up;
   Double_t        weight_FT_EFF_Eigen_B_0__1down;
   Double_t        weight_FT_EFF_Eigen_B_1__1up;
   Double_t        weight_FT_EFF_Eigen_B_1__1down;
   Double_t        weight_FT_EFF_Eigen_B_2__1up;
   Double_t        weight_FT_EFF_Eigen_B_2__1down;
   Double_t        weight_FT_EFF_Eigen_B_3__1up;
   Double_t        weight_FT_EFF_Eigen_B_3__1down;
   Double_t        weight_FT_EFF_Eigen_B_4__1up;
   Double_t        weight_FT_EFF_Eigen_B_4__1down;
   Double_t        weight_FT_EFF_Eigen_B_5__1up;
   Double_t        weight_FT_EFF_Eigen_B_5__1down;
   Double_t        weight_FT_EFF_Eigen_B_6__1up;
   Double_t        weight_FT_EFF_Eigen_B_6__1down;
   Double_t        weight_FT_EFF_Eigen_B_7__1up;
   Double_t        weight_FT_EFF_Eigen_B_7__1down;
   Double_t        weight_FT_EFF_Eigen_B_8__1up;
   Double_t        weight_FT_EFF_Eigen_B_8__1down;

   // List of branches
   TBranch        *b_muon_n;   //!
   TBranch        *b_muon_e;   //!
   TBranch        *b_muon_pt;   //!
   TBranch        *b_muon_eta;   //!
   TBranch        *b_muon_phi;   //!
   TBranch        *b_electron_n;   //!
   TBranch        *b_electron_e;   //!
   TBranch        *b_electron_pt;   //!
   TBranch        *b_electron_eta;   //!
   TBranch        *b_electron_phi;   //!
   TBranch        *b_jet_n;   //!
   TBranch        *b_jet_e;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_m;   //!
   TBranch        *b_jet_has_btag;   //!
   TBranch        *b_calo_met;   //!
   TBranch        *b_inv_mass_electrons;   //!
   TBranch        *b_inv_mass_muons;   //!
   TBranch        *b_lq_mass_max;   //!
   TBranch        *b_dimuon_pt;   //!
   TBranch        *b_dielectron_pt;   //!
   TBranch        *b_ht_jets;   //!
   TBranch        *b_ht_leptons;   //!
   TBranch        *b_weight;   //!
   TBranch        *b_HLT_e60_lhmedium_nod0_acceptance;   //!
   TBranch        *b_HLT_e140_lhloose_nod0_acceptance;   //!
   TBranch        *b_HLT_mu50_acceptance;   //!
   TBranch        *b_pileup_weight;   //!
   TBranch        *b_mc_weight;   //!
   TBranch        *b_truth_mll;   //!
   TBranch        *b_average_int_per_xing;   //!
   TBranch        *b_run_number;   //!
   TBranch        *b_event_number;   //!
   TBranch        *b_jet_jvt;   //!
   TBranch        *b_jet_btag_sf;   //!
   TBranch        *b_weight_PRW_DATASF__1up;   //!
   TBranch        *b_weight_PRW_DATASF__1down;   //!
   TBranch        *b_weight_MUON_EFF_TTVA_STAT__1up;   //!
   TBranch        *b_weight_MUON_EFF_TTVA_STAT__1down;   //!
   TBranch        *b_weight_MUON_EFF_TTVA_SYS__1up;   //!
   TBranch        *b_weight_MUON_EFF_TTVA_SYS__1down;   //!
   TBranch        *b_weight_MUON_EFF_ISO_STAT__1up;   //!
   TBranch        *b_weight_MUON_EFF_ISO_STAT__1down;   //!
   TBranch        *b_weight_MUON_EFF_ISO_SYS__1up;   //!
   TBranch        *b_weight_MUON_EFF_ISO_SYS__1down;   //!
   TBranch        *b_weight_MUON_EFF_RECO_STAT__1up;   //!
   TBranch        *b_weight_MUON_EFF_RECO_STAT__1down;   //!
   TBranch        *b_weight_MUON_EFF_RECO_SYS__1up;   //!
   TBranch        *b_weight_MUON_EFF_RECO_SYS__1down;   //!
   TBranch        *b_weight_JET_JvtEfficiency__1up;   //!
   TBranch        *b_weight_JET_JvtEfficiency__1down;   //!
   TBranch        *b_weight_FT_EFF_extrapolation__1up;   //!
   TBranch        *b_weight_FT_EFF_extrapolation__1down;   //!
   TBranch        *b_weight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_weight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_weight_EL_EFF_Trigger_TotalCorrUncertainty__1up;   //!
   TBranch        *b_weight_EL_EFF_Trigger_TotalCorrUncertainty__1down;   //!
   TBranch        *b_weight_EL_EFF_TriggerEff_TotalCorrUncertainty__1up;   //!
   TBranch        *b_weight_EL_EFF_TriggerEff_TotalCorrUncertainty__1down;   //!
   TBranch        *b_weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_weight_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch        *b_weight_MUON_EFF_TrigStatUncertainty__1down;   //!
   TBranch        *b_weight_MUON_EFF_TrigSystUncertainty__1up;   //!
   TBranch        *b_weight_MUON_EFF_TrigSystUncertainty__1down;   //!
   TBranch        *b_weight_FT_EFF_Eigen_B_0__1up;   //!
   TBranch        *b_weight_FT_EFF_Eigen_B_0__1down;   //!
   TBranch        *b_weight_FT_EFF_Eigen_B_1__1up;   //!
   TBranch        *b_weight_FT_EFF_Eigen_B_1__1down;   //!
   TBranch        *b_weight_FT_EFF_Eigen_B_2__1up;   //!
   TBranch        *b_weight_FT_EFF_Eigen_B_2__1down;   //!
   TBranch        *b_weight_FT_EFF_Eigen_B_3__1up;   //!
   TBranch        *b_weight_FT_EFF_Eigen_B_3__1down;   //!
   TBranch        *b_weight_FT_EFF_Eigen_B_4__1up;   //!
   TBranch        *b_weight_FT_EFF_Eigen_B_4__1down;   //!
   TBranch        *b_weight_FT_EFF_Eigen_B_5__1up;   //!
   TBranch        *b_weight_FT_EFF_Eigen_B_5__1down;   //!
   TBranch        *b_weight_FT_EFF_Eigen_B_6__1up;   //!
   TBranch        *b_weight_FT_EFF_Eigen_B_6__1down;   //!
   TBranch        *b_weight_FT_EFF_Eigen_B_7__1up;   //!
   TBranch        *b_weight_FT_EFF_Eigen_B_7__1down;   //!
   TBranch        *b_weight_FT_EFF_Eigen_B_8__1up;   //!
   TBranch        *b_weight_FT_EFF_Eigen_B_8__1down;   //!

   LQTree_v26(TTree *tree=0);
   virtual ~LQTree_v26();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef LQTree_v26_cxx
LQTree_v26::LQTree_v26(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("user.morgens.361106.e3601_s3126_r10201_p3705.LQ.v26.0_hist/user.morgens.18174523._000008.hist-output.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("user.morgens.361106.e3601_s3126_r10201_p3705.LQ.v26.0_hist/user.morgens.18174523._000008.hist-output.root");
      }
      TDirectory * dir = (TDirectory*)f->Get("user.morgens.361106.e3601_s3126_r10201_p3705.LQ.v26.0_hist/user.morgens.18174523._000008.hist-output.root:/Nominal");
      dir->GetObject("BaseSelection_lq_tree_syst_Final",tree);

   }
   Init(tree);
}

LQTree_v26::~LQTree_v26()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t LQTree_v26::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t LQTree_v26::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void LQTree_v26::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   muon_e = 0;
   muon_pt = 0;
   muon_eta = 0;
   muon_phi = 0;
   electron_e = 0;
   electron_pt = 0;
   electron_eta = 0;
   electron_phi = 0;
   jet_e = 0;
   jet_pt = 0;
   jet_eta = 0;
   jet_phi = 0;
   jet_m = 0;
   jet_has_btag = 0;
   jet_jvt = 0;
   jet_btag_sf = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("muon_n", &muon_n, &b_muon_n);
   fChain->SetBranchAddress("muon_e", &muon_e, &b_muon_e);
   fChain->SetBranchAddress("muon_pt", &muon_pt, &b_muon_pt);
   fChain->SetBranchAddress("muon_eta", &muon_eta, &b_muon_eta);
   fChain->SetBranchAddress("muon_phi", &muon_phi, &b_muon_phi);
   fChain->SetBranchAddress("electron_n", &electron_n, &b_electron_n);
   fChain->SetBranchAddress("electron_e", &electron_e, &b_electron_e);
   fChain->SetBranchAddress("electron_pt", &electron_pt, &b_electron_pt);
   fChain->SetBranchAddress("electron_eta", &electron_eta, &b_electron_eta);
   fChain->SetBranchAddress("electron_phi", &electron_phi, &b_electron_phi);
   fChain->SetBranchAddress("jet_n", &jet_n, &b_jet_n);
   fChain->SetBranchAddress("jet_e", &jet_e, &b_jet_e);
   fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("jet_m", &jet_m, &b_jet_m);
   fChain->SetBranchAddress("jet_has_btag", &jet_has_btag, &b_jet_has_btag);
   fChain->SetBranchAddress("calo_met", &calo_met, &b_calo_met);
   fChain->SetBranchAddress("inv_mass_electrons", &inv_mass_electrons, &b_inv_mass_electrons);
   fChain->SetBranchAddress("inv_mass_muons", &inv_mass_muons, &b_inv_mass_muons);
   fChain->SetBranchAddress("lq_mass_max", &lq_mass_max, &b_lq_mass_max);
   fChain->SetBranchAddress("dimuon_pt", &dimuon_pt, &b_dimuon_pt);
   fChain->SetBranchAddress("dielectron_pt", &dielectron_pt, &b_dielectron_pt);
   fChain->SetBranchAddress("ht_jets", &ht_jets, &b_ht_jets);
   fChain->SetBranchAddress("ht_leptons", &ht_leptons, &b_ht_leptons);
   fChain->SetBranchAddress("weight", &weight, &b_weight);
   fChain->SetBranchAddress("HLT_e60_lhmedium_nod0_acceptance", &HLT_e60_lhmedium_nod0_acceptance, &b_HLT_e60_lhmedium_nod0_acceptance);
   fChain->SetBranchAddress("HLT_e140_lhloose_nod0_acceptance", &HLT_e140_lhloose_nod0_acceptance, &b_HLT_e140_lhloose_nod0_acceptance);
   fChain->SetBranchAddress("HLT_mu50_acceptance", &HLT_mu50_acceptance, &b_HLT_mu50_acceptance);
   fChain->SetBranchAddress("pileup_weight", &pileup_weight, &b_pileup_weight);
   fChain->SetBranchAddress("mc_weight", &mc_weight, &b_mc_weight);
   fChain->SetBranchAddress("truth_mll", &truth_mll, &b_truth_mll);
   fChain->SetBranchAddress("average_int_per_xing", &average_int_per_xing, &b_average_int_per_xing);
   fChain->SetBranchAddress("run_number", &run_number, &b_run_number);
   fChain->SetBranchAddress("event_number", &event_number, &b_event_number);
   fChain->SetBranchAddress("jet_jvt", &jet_jvt, &b_jet_jvt);
   fChain->SetBranchAddress("jet_btag_sf", &jet_btag_sf, &b_jet_btag_sf);
   fChain->SetBranchAddress("weight_PRW_DATASF__1up", &weight_PRW_DATASF__1up, &b_weight_PRW_DATASF__1up);
   fChain->SetBranchAddress("weight_PRW_DATASF__1down", &weight_PRW_DATASF__1down, &b_weight_PRW_DATASF__1down);
   fChain->SetBranchAddress("weight_MUON_EFF_TTVA_STAT__1up", &weight_MUON_EFF_TTVA_STAT__1up, &b_weight_MUON_EFF_TTVA_STAT__1up);
   fChain->SetBranchAddress("weight_MUON_EFF_TTVA_STAT__1down", &weight_MUON_EFF_TTVA_STAT__1down, &b_weight_MUON_EFF_TTVA_STAT__1down);
   fChain->SetBranchAddress("weight_MUON_EFF_TTVA_SYS__1up", &weight_MUON_EFF_TTVA_SYS__1up, &b_weight_MUON_EFF_TTVA_SYS__1up);
   fChain->SetBranchAddress("weight_MUON_EFF_TTVA_SYS__1down", &weight_MUON_EFF_TTVA_SYS__1down, &b_weight_MUON_EFF_TTVA_SYS__1down);
   fChain->SetBranchAddress("weight_MUON_EFF_ISO_STAT__1up", &weight_MUON_EFF_ISO_STAT__1up, &b_weight_MUON_EFF_ISO_STAT__1up);
   fChain->SetBranchAddress("weight_MUON_EFF_ISO_STAT__1down", &weight_MUON_EFF_ISO_STAT__1down, &b_weight_MUON_EFF_ISO_STAT__1down);
   fChain->SetBranchAddress("weight_MUON_EFF_ISO_SYS__1up", &weight_MUON_EFF_ISO_SYS__1up, &b_weight_MUON_EFF_ISO_SYS__1up);
   fChain->SetBranchAddress("weight_MUON_EFF_ISO_SYS__1down", &weight_MUON_EFF_ISO_SYS__1down, &b_weight_MUON_EFF_ISO_SYS__1down);
   fChain->SetBranchAddress("weight_MUON_EFF_RECO_STAT__1up", &weight_MUON_EFF_RECO_STAT__1up, &b_weight_MUON_EFF_RECO_STAT__1up);
   fChain->SetBranchAddress("weight_MUON_EFF_RECO_STAT__1down", &weight_MUON_EFF_RECO_STAT__1down, &b_weight_MUON_EFF_RECO_STAT__1down);
   fChain->SetBranchAddress("weight_MUON_EFF_RECO_SYS__1up", &weight_MUON_EFF_RECO_SYS__1up, &b_weight_MUON_EFF_RECO_SYS__1up);
   fChain->SetBranchAddress("weight_MUON_EFF_RECO_SYS__1down", &weight_MUON_EFF_RECO_SYS__1down, &b_weight_MUON_EFF_RECO_SYS__1down);
   fChain->SetBranchAddress("weight_JET_JvtEfficiency__1up", &weight_JET_JvtEfficiency__1up, &b_weight_JET_JvtEfficiency__1up);
   fChain->SetBranchAddress("weight_JET_JvtEfficiency__1down", &weight_JET_JvtEfficiency__1down, &b_weight_JET_JvtEfficiency__1down);
   fChain->SetBranchAddress("weight_FT_EFF_extrapolation__1up", &weight_FT_EFF_extrapolation__1up, &b_weight_FT_EFF_extrapolation__1up);
   fChain->SetBranchAddress("weight_FT_EFF_extrapolation__1down", &weight_FT_EFF_extrapolation__1down, &b_weight_FT_EFF_extrapolation__1down);
   fChain->SetBranchAddress("weight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up", &weight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_weight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("weight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down", &weight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_weight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up", &weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down", &weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("weight_EL_EFF_Trigger_TotalCorrUncertainty__1up", &weight_EL_EFF_Trigger_TotalCorrUncertainty__1up, &b_weight_EL_EFF_Trigger_TotalCorrUncertainty__1up);
   fChain->SetBranchAddress("weight_EL_EFF_Trigger_TotalCorrUncertainty__1down", &weight_EL_EFF_Trigger_TotalCorrUncertainty__1down, &b_weight_EL_EFF_Trigger_TotalCorrUncertainty__1down);
   fChain->SetBranchAddress("weight_EL_EFF_TriggerEff_TotalCorrUncertainty__1up", &weight_EL_EFF_TriggerEff_TotalCorrUncertainty__1up, &b_weight_EL_EFF_TriggerEff_TotalCorrUncertainty__1up);
   fChain->SetBranchAddress("weight_EL_EFF_TriggerEff_TotalCorrUncertainty__1down", &weight_EL_EFF_TriggerEff_TotalCorrUncertainty__1down, &b_weight_EL_EFF_TriggerEff_TotalCorrUncertainty__1down);
   fChain->SetBranchAddress("weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up", &weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down", &weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("weight_MUON_EFF_TrigStatUncertainty__1up", &weight_MUON_EFF_TrigStatUncertainty__1up, &b_weight_MUON_EFF_TrigStatUncertainty__1up);
   fChain->SetBranchAddress("weight_MUON_EFF_TrigStatUncertainty__1down", &weight_MUON_EFF_TrigStatUncertainty__1down, &b_weight_MUON_EFF_TrigStatUncertainty__1down);
   fChain->SetBranchAddress("weight_MUON_EFF_TrigSystUncertainty__1up", &weight_MUON_EFF_TrigSystUncertainty__1up, &b_weight_MUON_EFF_TrigSystUncertainty__1up);
   fChain->SetBranchAddress("weight_MUON_EFF_TrigSystUncertainty__1down", &weight_MUON_EFF_TrigSystUncertainty__1down, &b_weight_MUON_EFF_TrigSystUncertainty__1down);
   fChain->SetBranchAddress("weight_FT_EFF_Eigen_B_0__1up", &weight_FT_EFF_Eigen_B_0__1up, &b_weight_FT_EFF_Eigen_B_0__1up);
   fChain->SetBranchAddress("weight_FT_EFF_Eigen_B_0__1down", &weight_FT_EFF_Eigen_B_0__1down, &b_weight_FT_EFF_Eigen_B_0__1down);
   fChain->SetBranchAddress("weight_FT_EFF_Eigen_B_1__1up", &weight_FT_EFF_Eigen_B_1__1up, &b_weight_FT_EFF_Eigen_B_1__1up);
   fChain->SetBranchAddress("weight_FT_EFF_Eigen_B_1__1down", &weight_FT_EFF_Eigen_B_1__1down, &b_weight_FT_EFF_Eigen_B_1__1down);
   fChain->SetBranchAddress("weight_FT_EFF_Eigen_B_2__1up", &weight_FT_EFF_Eigen_B_2__1up, &b_weight_FT_EFF_Eigen_B_2__1up);
   fChain->SetBranchAddress("weight_FT_EFF_Eigen_B_2__1down", &weight_FT_EFF_Eigen_B_2__1down, &b_weight_FT_EFF_Eigen_B_2__1down);
   fChain->SetBranchAddress("weight_FT_EFF_Eigen_B_3__1up", &weight_FT_EFF_Eigen_B_3__1up, &b_weight_FT_EFF_Eigen_B_3__1up);
   fChain->SetBranchAddress("weight_FT_EFF_Eigen_B_3__1down", &weight_FT_EFF_Eigen_B_3__1down, &b_weight_FT_EFF_Eigen_B_3__1down);
   fChain->SetBranchAddress("weight_FT_EFF_Eigen_B_4__1up", &weight_FT_EFF_Eigen_B_4__1up, &b_weight_FT_EFF_Eigen_B_4__1up);
   fChain->SetBranchAddress("weight_FT_EFF_Eigen_B_4__1down", &weight_FT_EFF_Eigen_B_4__1down, &b_weight_FT_EFF_Eigen_B_4__1down);
   fChain->SetBranchAddress("weight_FT_EFF_Eigen_B_5__1up", &weight_FT_EFF_Eigen_B_5__1up, &b_weight_FT_EFF_Eigen_B_5__1up);
   fChain->SetBranchAddress("weight_FT_EFF_Eigen_B_5__1down", &weight_FT_EFF_Eigen_B_5__1down, &b_weight_FT_EFF_Eigen_B_5__1down);
   fChain->SetBranchAddress("weight_FT_EFF_Eigen_B_6__1up", &weight_FT_EFF_Eigen_B_6__1up, &b_weight_FT_EFF_Eigen_B_6__1up);
   fChain->SetBranchAddress("weight_FT_EFF_Eigen_B_6__1down", &weight_FT_EFF_Eigen_B_6__1down, &b_weight_FT_EFF_Eigen_B_6__1down);
   fChain->SetBranchAddress("weight_FT_EFF_Eigen_B_7__1up", &weight_FT_EFF_Eigen_B_7__1up, &b_weight_FT_EFF_Eigen_B_7__1up);
   fChain->SetBranchAddress("weight_FT_EFF_Eigen_B_7__1down", &weight_FT_EFF_Eigen_B_7__1down, &b_weight_FT_EFF_Eigen_B_7__1down);
   fChain->SetBranchAddress("weight_FT_EFF_Eigen_B_8__1up", &weight_FT_EFF_Eigen_B_8__1up, &b_weight_FT_EFF_Eigen_B_8__1up);
   fChain->SetBranchAddress("weight_FT_EFF_Eigen_B_8__1down", &weight_FT_EFF_Eigen_B_8__1down, &b_weight_FT_EFF_Eigen_B_8__1down);
   Notify();
}

Bool_t LQTree_v26::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void LQTree_v26::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t LQTree_v26::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef LQTree_v26_cxx

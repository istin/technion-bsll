/* author   :   Serhat Istin
<mailto:istin@cern.ch>
*/

#ifndef SKELETON_H
#define SKELETON_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <TTree.h>
#include <NTAnalysis/EventData.h>
#include <TParameter.h>
#include <TH1F.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/optional/optional.hpp>

#include <memory>

class Skeleton : public EL::AnaAlgorithm
{
    
    
public:
  // this is a standard algorithm constructor
  Skeleton (const std::string& name, ISvcLocator* pSvcLocator);
  virtual ~Skeleton();
  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;
  virtual StatusCode fileExecute() override;
  virtual StatusCode beginInputFile() override;

  
  //!!! You have to implement these in your derived algorithm !!!
  virtual StatusCode BookHistograms()=0;  
  virtual StatusCode doAnalysis()=0;
  //virtual double GetDenominator()=0;

  virtual StatusCode ConfigureFromFile( );
  
  std::string GetValueAsString(const std::string& name) const {return m_AnalysisProperties.at(name);}//unpack some stuff from the property map
  float GetValueAsDouble(const std::string& name)const { return std::stof(m_AnalysisProperties.at(name));}
  bool hasObjectSystematic() const;
  bool hasSFSystematic() const;

  StatusCode CheckFileConfiguration();
  
  StatusCode AllocateAnalysisTree();
  StatusCode SetupDataSourceAttributes();
  StatusCode InitializeDataSource();

  StatusCode IncrementDenominator();

  StatusCode AllocateAndAssignMetaData();

  StatusCode IterateData();


protected:
  // Configuration, and any other types of variables go here.
  //float m_cutValue;
  //TTree *m_myTree;
  //TH1 *m_myHist;

    TParameter<double>* m_denom;
    std::string m_config;//name of the Configuration file(Mandate)
    std::string m_datasource;//We can run on various types of data like minituples LqTress etc... the one  in the Configuration is used

    NTAna::EventData* m_data;//!
    float m_emunit;//sometimes ntuples come with energy-momentum in MeV instead of GeV. This will be he conversion factor as in the Configuration
    std::vector<std::string> m_allowedSysts;//list of all available systematics in the Configuration
    std::string m_sys;//systematic set by the client
    std::unordered_map<std::string,std::string> m_AnalysisProperties;//these are typically cut values as set in the kinemtacis section and such...(Rather than DataConfig)
};

#endif

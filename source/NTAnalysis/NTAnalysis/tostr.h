/* author   :   Serhat Istin
<mailto:istin@cern.ch>
*/

#ifndef TOSTR_H
#define TOSTR_H
//herhangi bişeyi std::stringe çevirmek için
#include <string>
#include <sstream>

template <typename T> std::string tostr(const T& t) {
   std::ostringstream os; os<<t; return os.str();
 }
#endif
//sene olmus 2019 buna gerek yok lakin nostalji olsun diye dursun
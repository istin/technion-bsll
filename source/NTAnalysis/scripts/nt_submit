#!/usr/bin/env bash


#global configurables


#An utility script to discover and retry failed nt analysis jobs
#Serhat Istin

if [ -z "$NTANADIR" ];then
    echo "${FUNCNAME[0]} : Analysis Framework not setup!"
    exit
fi

SYSFILE=$NTANADIR/data/'systematics.txt'

SCRIPTS_DIR=""

CURRDIR=$(readlink -e .)


function check () {

    if [[ ! -f $1 ]] && [[ ! -d $1 ]] && [[ ! -L $1 ]];then
        echo "Unable to locate : "$1
        exit
    fi    
}

CheckSetupAndMakeSubmissionDirectory(){

    if [ $# -ne 2 ];then
        echo "Commandline Error ! : Usage: "
        echo "${FUNCNAME[0]} : <algorithm> <channel>"
        exit
    fi

    algorithm=$1
    channel=$2
    if [ "$algorithm" != "BSLLAlg" ] && [ "$algorithm" != "GenericAlg" ];then
        echo "${FUNCNAME[0]} : Wrong algorithm is set : "$algorithm
        exit
    fi 

    if [ "$channel" != "MM" ] && [ "$channel" != "EE" ];then
        echo "${FUNCNAME[0]} : Wrong channel set !"
        exit
    fi
    #Check if analysisBase is setup
    if [ -z "$$AnalysisBase_VERSION" ];then
        echo "${FUNCNAME[0]} : AnalysisBase not setup !"
        exit
    fi    
    check_if_batch_clean=$(qstat | grep $USER | wc -l)

    if [ $check_if_batch_clean -ne 0 ];then
        echo "${FUNCNAME[0]} : *** There are still some jobs running. ***"  
        exit 
    fi


    ana_setup=$NTANABUILD/setupFramework.sh

    if [ ! -f $ana_setup ];then
        echo "${FUNCNAME[0]} : Setup File : " $ana_setup " does not exist!"
        exit
    fi

    check $SYSFILE
    check $ana_setup

    destination=$(readlink -e .)/submitOut_${algorithm}_${channel}

    

    if [ -d "$destination" ];then
	    echo "${FUNCNAME[0]} : Destination : $destination already exists ! You must backup manually"
        exit
    fi

    mkdir $destination

    SCRIPTS_DIR=$destination/submission_scripts
    mkdir $SCRIPTS_DIR
    resultsdir=$destination/backgrounds
    mkdir $resultsdir

}


CreateAndLaunchJobs(){

    if [ $# -ne 1 ];then
        echo "*** Error in ${FUNCNAME[0]} :"
        echo "A proper input sample directory must be suplied"
        exit
    fi


    input_sample=$(readlink -e $1)
    check $input_sample
    check $SCRIPTS_DIR
    cd $SCRIPTS_DIR

    let ctr=0

    for sys in $(cat $SYSFILE);do
        script=node_${sys}_${channel}.sh
        echo "#!/bin/bash">$script
        echo "shopt -s expand_aliases">>$script
        # note that ATLAS_LOCAL_ROOT_BASE (unlike the aliases) is passed the shell from where you are submitting.
        #echo "alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh' ">>$script
        echo "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase">>$script
        echo "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh">>$script
        echo "setupATLAS">>$script
        echo "asetup AnalysisBase,${AnalysisBase_VERSION},here">>$script
        echo "lsetup python">>$script
        echo "source ${ana_setup}">>$script
        random_tag=$RANDOM
        outdir=out_${algorithm}_${sys}_${channel}
        echo "mkdir ${outdir}">>$script
        echo "cd ${outdir}">>$script
        name=${script%.*}
        echo "nt_runner -A ${algorithm} -I ${input_sample} -R out_${sys} -C $channel -e ${sys} -m" >> $script
        echo "Normalize -i ." >> $script
        echo "shopt -s extglob">>$script
        echo "rm -rf !(*.root)">>$script  
        echo "cd ..">>$script
        echo "if [ -d ${resultsdir}/${outdir} ];then rm -rf ${resultsdir}/${outdir};fi" >>$script
        echo " mv ${outdir} ${resultsdir}/${outdir}" >>$script
        #echo "rm -rf ${outdir}">>$script
        chmod +x $script
    done



    for  executable in $(ls *.sh | grep node) ;do
        name=$(basename $executable .sh)
        qsub -q P -N $name -o $SCRIPTS_DIR -e $SCRIPTS_DIR $executable
    done

    cd $CURRDIR
}



echo "Running Job Submission $0 ..."

display_usage() {
  echo
  echo "Usage: $0"
  echo
  echo " -h, --help       Display usage instructions"
  echo " -A, --Algorithm  Algorithm to run on       "
  echo " -C, --Channel                              "
  echo " -I, --Input     Sample(s)                  "
  echo
  exit
}



argument="$1"

if [[ -z $argument ]] ; then
  echo "Expected arguments to be present"
  display_usage
  exit
fi
alg=""
ch=""
ins=""

OPTIONS=A:C:I:h
LONGOPTS=Algorithm:,Channel:,Input:,help
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # e.g. return value is 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi
eval set -- "$PARSED"

# now enjoy the options in order and nicely split until we see --
while true; do
    case "$1" in
        -A|--Algorithm)
            alg="$2"
            shift 2
            ;;
        -I|--Input)
            ins="$2"
            shift 2
            ;;
        -C|--Channel)
            ch="$2"
            shift 2
            ;;
        -h|--help)
            display_usage
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Fatal error in arguments"
            exit 3
            ;;
    esac
done

if [ "$alg" == "" ];then
    echo "Algorithm not set"
    exit
fi
if [ "$ch" == "" ];then
    echo "Channel not set"
    exit
fi
if [ "$ins" == "" ];then
    echo "input sample not set"
    exit
fi


CheckSetupAndMakeSubmissionDirectory $alg $ch
CreateAndLaunchJobs $ins
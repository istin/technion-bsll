#!/usr/bin/env python
# author   :   Serhat Istin
#mailto:istin@cern.ch


import os

def AddOverFlowBins(h_histo):
  #add content of the overflow bin to the last one
  i_overflow=h_histo.GetNbinsX()+1
  i_lastbin=h_histo.GetNbinsX()
  overflowcontent=h_histo.GetBinContent(i_overflow)
  underflowcontent=h_histo.GetBinContent(0)
  lastbincontent=h_histo.GetBinContent(i_lastbin)
  firstbincontent=h_histo.GetBinContent(1)
  #add overflow and underflows to the last and first bins
  h_histo.SetBinContent(i_lastbin,lastbincontent+overflowcontent)
  h_histo.SetBinContent(1,firstbincontent+underflowcontent)

def ProcessHist(h_histo):
  if h_histo is None:
    print("None Histogram!")
    os.sys.exit()
  h_histo.Sumw2()
  AddOverFlowBins(h_histo)

  return
 # if h_histo.GetName().rfind("h_ptZ_")!=-1:
 #   h_histo.Rebin(2)

    
